import React from 'react';
import logo from './logo.svg';
import './App.css';
import 'holderjs';
import Home from './components/Home';
import Services from './components/Services';
import Success from './components/Success';
import Checkout from './components/Checkout';
import Login from './components/Login';
import Signup from './components/Signup';
import Profile from './components/Profile';
import Contact from './components/Contact';
import Admin_subscriptions from './components/admin/Admin_subscriptions';
import Forgot from './components/Forgot';
import ResetPassword from './components/ResetPassword';
import Admin_users from './components/admin/Admin_users';
import Logout from './components/Logout';
import Dashboard from './components/Dashboard';
import Verify from './components/Verify';
import ScrollToTop from './components/ScrollToTop';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Header from './components/common/Header';
import Footer from './components/common/Footer';
import AdminAppointments from './components/admin/Admin_appointments';
import 'font-awesome/css/font-awesome.min.css';
import AddBlog from './components/admin/Add_blog';
import Blogs from './components/Blogs';
import BlogDetails from './components/BlogDetails';
import Edit_blog from './components/admin/Edit_blog';
import Manage_blogs from './components/admin/Manage_blogs';

class App extends React.Component {
  state = {
    logged_in: this.getCookie('session_app') ? true : false,
    user_type: this.getCookie('user_type') ? this.getCookie('user_type') : ''
  };

  componentWillMount() {
    console.log(this.props.history)    
  }

  getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }

  onUpdatedSession(type, user_type) {
    if(type == 'logged_in') {
      this.setState({ logged_in: true, user_type: user_type });
    } else {
      this.setState({ logged_in: false, user_type: user_type });
    }
  }

  render() {
    return (
      <Router>
        <ScrollToTop>
          <Header logged_in={this.state.logged_in} user_type={this.state.user_type}/>
            <Route exact path="/" component={Home} />
            <Route exact path="/services" component={Services} />
            <Route exact path="/checkout" component={Checkout} />
            <Route exact path="/success" component={Success} />
            <Route exact path="/login" render={(props) => <Login {...props} onUpdatedSession={this.onUpdatedSession.bind(this)} />} />
            <Route exact path="/signup" component={Signup} />
            <Route exact path="/forgot" component={Forgot} />
            <Route exact path="/verify-email/:token" component={Verify} />
            <Route exact path="/reset-password/:token" component={ResetPassword} />
            <Route exact path="/logout" render={(props) => <Logout {...props} onUpdatedSession={this.onUpdatedSession.bind(this)} />} />
            <Route exact path="/subscriptions" component={Dashboard} />
            <Route exact path="/profile" component={Profile} />
            <Route exact path="/contact" component={Contact} />
            <Route exact path="/admin/users" component={Admin_users} />
            <Route exact path="/admin/subscriptions" component={Admin_subscriptions} />
            <Route exact path="/admin/appointments" component={AdminAppointments}/>
            <Route exact path="/admin/blogs/add" component={AddBlog} />
            <Route exact path="/admin/blogs/manage" component={Manage_blogs} />
            <Route exact path="/blogs" component={Blogs} />
            <Route exact path="/blogs/:id" component={BlogDetails} />
            <Route exact path="/admin/blogs/edit/:id" component={Edit_blog} />
          <Footer />
        </ScrollToTop>
      </Router>
    );
  }
}

export default App;