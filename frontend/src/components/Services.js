import React from 'react';
import './Services.css';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/FormControl';
import Carousel from 'react-bootstrap/Carousel';
import Jumbotron from 'react-bootstrap/Jumbotron';
import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Accordion from 'react-bootstrap/Accordion';
import InputGroup from 'react-bootstrap/InputGroup';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Tooltip from 'react-bootstrap/Tooltip';
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';
import Spinner from 'react-bootstrap/Spinner';
import config from '../config';
import $ from 'jquery';

class Services extends React.Component {
	state = {
		'selected_prices': [],
		'plans': [],
		'users': 1,
		'loading': true,
		'plans_list': []
	}

	renderLoading() {
		if(this.state.loading) {
			return (<Row className="text-align-center"><Col><Spinner animation="border" /></Col></Row>);
		}
	}

	onItemClick(item, event,) {
		let selected = this.state.selected_prices;
		if(selected.indexOf(item.id) == -1) {
			selected.push(item.id);
			if(item.plan_relations && item.plan_relations.length) {
				for(var i=0; i< item.plan_relations.length; i++) {
					if(this.state.selected_prices.indexOf(item.plan_relations[i].required_plan.id) == -1) {
						selected.push(item.plan_relations[i].required_plan.id);
					}
				}
			}
		} else {
			let indexItem = selected.indexOf(item.id);
			selected.splice(indexItem, 1);
		}
		this.setState({
			'selected_prices': selected
		});

		let selected_plans = [];

		for(let i=0; i< this.state.plans_list.length; i++) {
			if(selected.indexOf(this.state.plans_list[i].id) != -1) {
				selected_plans.push(this.state.plans_list[i]);
			}
		}
		localStorage.setItem('selected_plans', JSON.stringify(selected_plans));
	}

	handleChange(event) {
		localStorage.setItem('users', event.target.value);
		this.setState({
			'users': event.target.value
		});
	}

	componentDidMount() {
		fetch(config.api_url+'api/plans', {
			method: 'get'
		})
		.then( response => response.json())
		.then( (response) => {
			let plans = [];
			if(response.data) {
				response.data.forEach( (data) => {
					if(!plans[data.categories_id]) {
						plans[data.categories_id] = [];
					}
					plans[data.categories_id].push(data);
				});
				this.setState({
					plans: plans,
					loading: false,
					plans_list: response.data
				});
				let selected_plans = JSON.parse(localStorage.getItem('selected_plans'));
				let selected_plans_id = [];
				for(let l=0; l< selected_plans.length; l++) {
					selected_plans_id.push(selected_plans[l].id);
				}
				this.setState({
					selected_prices: selected_plans_id
				});
			}
		})
		.catch( (error) => {
			console.log(error);
		});
	}

	handleCheckout(value, event) {
		if(value == 'anually') {
			const data = {
				apps: this.state.selected_prices.length,
				users: this.state.users,
				type: 'anually'
			};
			localStorage.setItem('cart', JSON.stringify(data));
		} else {
			const data = {
				apps: this.state.selected_prices.length,
				users: this.state.users,
				type: 'monthly'
			};
			localStorage.setItem('cart', JSON.stringify(data));
		}
		this.props.history.push('/checkout'); 
	}

	renderInfoIcon(item) {
		if(this.state.selected_prices && this.state.selected_prices.indexOf(item.id) != -1) {
			return (<i style={{color: '#27a664'}} class="fas fa-link"></i>);
		} else {
			return (<strong className="info-icon" style={{fontStyle: 'italic'}}>i</strong>);
		}
	}

	renderInfo(item) {
		if(item.plan_relations && item.plan_relations.length) {
			let requiredPlans = [];
			item.plan_relations.forEach( (plan_relation) => {
				requiredPlans.push(plan_relation.required_plan.name);
			});
			return (
				<OverlayTrigger overlay={<Tooltip id="tooltip-disabled">Requires {requiredPlans.join(',')}</Tooltip>}>
				  <span className="pull-right info-pricing">
				    <span>
						{this.renderInfoIcon(item)}
					</span>
				  </span>
				</OverlayTrigger>
			);
		}
	}

	renderPlans(plan_id) {
		if(this.state.plans[plan_id]) {
			var rows = [];
			this.state.plans[plan_id].map( item => {
      			rows.push(
      				<Col onClick={this.onItemClick.bind(this, item)} key={item.id} md="4" className={"product-outer-column"+(this.state.selected_prices.indexOf(item.id) != -1 ? ' selected-price' : '')}>
		      			<Row className="product-column">
		      				<Col md="3" xs="3" className="padding-left-0">
		      					<img src={item.image} />
		      				</Col>
		      				<Col md="9" xs="9"  className="padding-left-0 padding-right-0">
		      					<Row>
		      						<Col><strong className="plan-name-strong">{item.name}</strong> 
		      							{ this.state.selected_prices.indexOf(item.id) == -1 ? <div className="pull-right circle-select"></div> : <i style={{color: '#ffffff', fontSize: '10px'}} className="fa fa-check fa-icon pull-right"></i>}
		      						</Col>
		      					</Row>
		      					<Row>
		      						<Col>
		      							{Number(item.price+((item.price * item.fee)/100)).toFixed(2)} USD / month
		      							{this.renderInfo(item)}
		      						</Col>
		      					</Row>
		      				</Col>
		      			</Row>
		      		</Col>
      			)
      		})
      		return rows;
		}
	}

	renderTotal() {
		let totalAnnualPrice = 0;
		let totalMonthlyPrice = 0;
		let discountAnnual = config.discount;
		let perUserCost = config.user_price;
		let totalAnnualWithUserPrice = 0;

		let annualPriceUser = perUserCost - ((perUserCost * discountAnnual)/100);
		for(let k=0; k< this.state.plans_list.length; k++) {
			if(this.state.selected_prices.indexOf(this.state.plans_list[k].id) != -1) {
				let currentObject = this.state.plans_list[k];
				totalMonthlyPrice = totalMonthlyPrice + (currentObject.price + ((currentObject.fee * currentObject.price)/100));
			}
		}
		let totalAnnualPriceApps = totalMonthlyPrice - ((totalMonthlyPrice * discountAnnual)/100);


		return (
			<Tabs defaultActiveKey="anually" id="prices-tab">
			  <Tab eventKey="anually" title="Anually" className="price-tab-content">
			    <Row className="price-total-row">
			    	<Col>
			    		<span className="pull-left">{this.state.users} <font className="price-label-font">Users</font></span>
			    		<span className="pull-right price-span">${annualPriceUser} USD</span>
			    	</Col>
			    </Row>

			    <Row className="price-total-row">
			    	<Col>
			    		<span className="pull-left">{this.state.selected_prices.length} <font className="price-label-font">Apps</font></span>
			    		<span className="pull-right price-span">${Number(totalAnnualPriceApps).toFixed(2)} USD</span>
			    	</Col>
			    </Row>         
			    
			    <Row className="total-pricing-month">
			    	<Col>
			    		<span className="pull-left"><font className="price-label-font">Total / Month</font></span>
			    		<span className="pull-right price-span">${Number(annualPriceUser+totalAnnualPriceApps).toFixed(2)}</span>
			    	</Col>
			    </Row> 
			    <Row className="spacing-top-botton-10">
			    	<Col className="billed-yearly">
			    		Billed annually at ${Number((annualPriceUser+totalAnnualPriceApps)*12).toFixed(2)} / year
			    	</Col>
			    </Row>
			    <Row>
			    	<Col  md={12} xs={6} className="spacing-top-botton-10">
				    	<Button variant="primary" onClick={this.handleCheckout.bind(this, 'anually')} size="md" block>
						    Checkout
						</Button>
					</Col>
				</Row>
			  </Tab>
			  <Tab eventKey="monthly" title="Monthly" className="price-tab-content">
			  	<Row className="price-total-row">
			    	<Col>
			    		<span className="pull-left">{this.state.users} <font className="price-label-font">Users</font></span>
			    		<span className="pull-right price-span">${Number(perUserCost).toFixed(2)} USD</span>
			    	</Col>
			    </Row>

			    <Row className="price-total-row">
			    	<Col>
			    		<span className="pull-left">{this.state.selected_prices.length} <font className="price-label-font">Apps</font></span>
			    		<span className="pull-right price-span">${Number(totalMonthlyPrice).toFixed(2)} USD</span>
			    	</Col>
			    </Row>         
			    
			    <Row className="total-pricing-month">
			    	<Col>
			    		<span className="pull-left"><font className="price-label-font">Total / Month</font></span>
			    		<span className="pull-right price-span">${Number(perUserCost+totalMonthlyPrice)} USD</span>
			    	</Col>
			    </Row>
			    <Row className="spacing-top-botton-10">
			    	<Col className="billed-yearly">
			    		Billed monthly at ${Number(perUserCost+totalMonthlyPrice).toFixed(2)} / month
			    	</Col>
			    </Row>
			    <Row className="spacing-top-botton-10">
			    	<Col md={12} xs={6} className="spacing-top-botton-10">
				    	<Button variant="primary"  onClick={this.handleCheckout.bind(this, 'monthly')} size="md" block>
						    Checkout
						</Button>
					</Col>
				</Row>
			  </Tab>
			</Tabs>
		);
	}

	render() {
		return (
			<div className="background-silver">
		      <Jumbotron className="text-align-center appmosphere-pricing">
		      	<h3>Appmosphere Pricing</h3>
		      	<h5>Even pricing is easier with Appmosphere</h5>
		      </Jumbotron>
		      <Container className="pricing-container">
		      	<Row>
		      		<Col md={9} xs={12}>
		      			<Row>
				      		<Col>
				      			<h3>Choose the number of <strong>Users</strong></h3>
				      		</Col>
				      	</Row>
				      	<Row>
				      		<Col>
				      			<Row>
				      				<Col>
				      					<InputGroup className="mb-3">
										    <FormControl
										      placeholder="1"
										      defaultValue="1"
										      aria-describedby="basic-addon2"
										      type="number"
										      onChange={this.handleChange.bind(this)}
										    />
										    <InputGroup.Append>
										      <InputGroup.Text id="basic-addon2">Users</InputGroup.Text>
										    </InputGroup.Append>
										</InputGroup>
				      				</Col>
				      				<Col className="line-height-35">$24 USD/user/month</Col>
				      			</Row>						  
							</Col>
							<Col/>
				      	</Row>
				      	<Row>
				      		<Col>
				      			<h3>Choose your <strong>Apps</strong></h3>
				      		</Col>
				      	</Row>
				      	<Row className="select-price-row">
				      		{this.renderLoading()}
				      		{this.renderPlans(1)}
				      	</Row>
				      	<Row>
				      		<Col>
				      			<h3>Extra <strong>Integrations</strong></h3>
				      		</Col>
				      	</Row>
				      	<Row className="select-price-row">
				      		{this.renderLoading()}
				      		{this.renderPlans(2)}
				      	</Row>
				      	<Row>
				      		<video class="intro-video" controls>
							  <source src={"videos/Editing.mp4"} type="video/mp4"/>
							  <source src={"videos/Editing.mov"} type="video/mov"/>
							  Your browser does not support the video.
							</video>
				      	</Row>
				      	<Row>
				      		<Col>
				      			<h3>Any <strong>Questions</strong></h3>
				      			<p>If the answer to your question is not on this page, please contact our account managers.</p>
				      		</Col>
				      	</Row>
				      	<Row className="accordin-row">
				      		<Accordion>
							  <Card>
							    <Accordion.Toggle as={Card.Header} eventKey="0">
							      What do you get in your subscription?
							    </Accordion.Toggle>
							    <Accordion.Collapse eventKey="0">
							      <Card.Body>
							      	<p>
										You get access to our scalable cloud infrastructure including hosting, incremental daily backups on two continents, email integration, top notch security, 24/7 monitoring and a control center to manage your Odoo environment. Your Odoo instance is upgraded on demand to benefit from new features at your convenience.
									    You get access to our support by email or live chat in application. Our support teams are available from Monday to Friday, 24/5, in English and French.
									</p>
							      </Card.Body>
							    </Accordion.Collapse>
							  </Card>
							  <Card>
							    <Accordion.Toggle as={Card.Header} eventKey="1">
							      What do you get in your subscription?
							    </Accordion.Toggle>
							    <Accordion.Collapse eventKey="1">
							      <Card.Body>
							      	<p>
										You get access to our scalable cloud infrastructure including hosting, incremental daily backups on two continents, email integration, top notch security, 24/7 monitoring and a control center to manage your Odoo environment. Your Odoo instance is upgraded on demand to benefit from new features at your convenience.
									    You get access to our support by email or live chat in application. Our support teams are available from Monday to Friday, 24/5, in English and French.
									</p>
							      </Card.Body>
							    </Accordion.Collapse>
							  </Card>
							</Accordion>
				      	</Row>
		      		</Col>
		      		<Col md={3} xs={12} className="price-tab-right">
		      			{this.renderTotal()}
		      		</Col>
		      	</Row>
		      </Container>
		    </div>
		);
	}
}

export default Services;