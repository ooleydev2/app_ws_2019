import React from 'react';
import './Verify.css';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/FormControl';
import Carousel from 'react-bootstrap/Carousel';
import Jumbotron from 'react-bootstrap/Jumbotron';
import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Spinner from 'react-bootstrap/Spinner';
import Alert from 'react-bootstrap/Alert';
import Accordion from 'react-bootstrap/Accordion';
import Badge from 'react-bootstrap/Badge';
import Modal from 'react-bootstrap/Modal';
import config from '../config';
import { NavLink } from 'react-router-dom';
import validators from './common/reset_password_validators';

class ResetPassword extends React.Component {
	state = {
		loading: false, //Verifying token exists
		verified: false, //Verified token exists
		invalid_token:  false, //Token is invalid
		processing: false, //Resetting password
		success: false, //Reset password
		failed: false,
		user_info: {
			password: ''
		}
	};

	token = '';

	componentDidMount() {
		this.token = this.props.match.params.token;
		this.setState({
			loading: true
		});

	    fetch(config.api_url + 'api/users/valid_token', {
	    	method: 'POST',
	    	headers: {
	            'Content-Type': 'application/json'
	        },
			body: JSON.stringify({ token: this.token })
	    })
	    .then( (response) => {
	    	response.json()
	    	.then( (success) => {
	    		if(response.status == 200) {
					this.setState({
						loading: false, //Verifying token exists
						verified: true, //Verified token exists
						invalid_token: false, //Token is invalid
						processing: false, //Resetting password
						success: false, //Reset password
						failed: false
					});
	    		} else {
					this.setState({
						loading: false, //Verifying token exists
						verified: false, //Verified token exists
						invalid_token: true, //Token is invalid
						processing: false, //Resetting password
						success: false, //Reset password
						failed: false
					});	
	    		}
	    	})
	    	.catch( (error) => {
	    		this.setState({
	    			loading: false, //Verifying token exists
					verified: false, //Verified token exists
					invalid_token:  false, //Token is invalid
					processing: false, //Resetting password
					success: false, //Reset password
					failed: 'Error while processing your request. Please try again later!'
	    		});	
	    	});
	    })
	    .catch( (error) => {
	    	this.setState({
    			loading: false, //Verifying token exists
				verified: false, //Verified token exists
				invalid_token:  false, //Token is invalid
				processing: false, //Resetting password
				success: false, //Reset password
				failed: 'Error while processing your request. Please try again later!'
    		});
	    });
	}

	renderMessages() {
		if(this.state.loading) {
			return (
				<Row className="text-align-center">
					<Col md={12} xs={12}>
						<Spinner
					      as="span"
					      animation="border"
					      size="md"
					      role="status"
					      aria-hidden="true"
					    /> Verifying
					</Col>
				</Row>
			);
		}

		if(this.state.success) {
			return (
				<Alert variant={'success'}>
				    Successfully reset your account please <NavLink exact to="/login">login here</NavLink> to continue!
				</Alert>
			);
		}

		if(this.state.failed) {
			return (
				<Alert variant={'danger'}>
				    Something went wrong while processing your request. Please try again later!
				</Alert>
			);
		}

		if(this.state.invalid_token) {
			return (
				<Alert variant={'danger'}>
				   Invalid request either the token has been already used or does not exist!
				</Alert>
			);
		}
	}

	validators = validators;
	renderLoading() {
		if(this.state.loading) {
			return (
					<Row className="text-align-center">
						<Col md={12} xs={12}>
							<Spinner
						      as="span"
						      animation="border"
						      size="md"
						      role="status"
						      aria-hidden="true"
						    /> Verifying
						</Col>
					</Row>
			);
		}
	}

	renderFailed() {
		if(this.state.failed) {
			return (<Alert variant={'danger'}>
			   {this.state.failed}
			</Alert>);
		}
	}

	renderSuccess() {
		if(this.state.success) {
			return (<Alert variant={'success'}>
			   Successfully verified your account please <NavLink exact to="/login">login here</NavLink> to continue!
			</Alert>);
		}
	}

	handleChange(name, event) {
		let fieldName = name;
		let fieldValue = event.target.value;
		this.updateValidators(fieldName, fieldValue);
		let values = this.state.user_info;
		values[name] = event.target.value;
		this.setState({
			user_info: values
		});
	}

	isFormValid() {
	    let status = true;
	    Object.keys(this.validators).forEach((field) => {
	      this.updateValidators(field, this.state.user_info[field]);
	      if (!this.validators[field].valid) {
	        status = false;
	      }
	    });
	    this.setState({ form_submitted: true });
	    return status;
	}

	updateValidators(fieldName, value) {
	    this.validators[fieldName].errors = [];
	    this.validators[fieldName].state = value;
	    this.validators[fieldName].valid = true;
	    this.validators[fieldName].rules.forEach((rule) => {
	    	if(!this.validators[fieldName].errors.length) {
	    		if (rule.test instanceof RegExp) {
			        if (!rule.test.test(value)) {
			          this.validators[fieldName].errors.push(rule.message);
			          this.validators[fieldName].valid = false;
			        }
			    } else if (typeof rule.test === 'function') {
			        if (!rule.test(value)) {
			          this.validators[fieldName].errors.push(rule.message);
			          this.validators[fieldName].valid = false;
			        }
			    }
	    	}	
	    });
	}

	renderSubmitButton() {
		if(this.state.processing) {
			return (
				<Button disabled variant="primary" className="margin-center" type="submit">
					<Spinner
				      as="span"
				      animation="border"
				      size="sm"
				      role="status"
				      aria-hidden="true"
				    /> Update
			    </Button>
			);
		} else {
			return (
				<Button variant="primary" className="margin-center" type="submit">
					Update
			    </Button>
			);
		}
	}

	displayValidationErrors(fieldName) {
	    const validator = this.validators[fieldName];
	    const result = '';
	    if (validator && !validator.valid) {
	      const errors = validator.errors.map((info, index) => {
	        return <span className="error" key={index}>* {info}</span>;
	      });
	      return (
	        <Row>
	        	<Col md={12} sm={12} className="text text-danger">
	        		{errors}
	        	</Col>
	        </Row>
	      );
	    }
	    return result;
	}

	handleSubmit(e) {
		if(this.isFormValid()) {
			fetch(config.api_url + 'api/users/update_password', {
		    	method: 'POST',
		    	headers: {
		            'Content-Type': 'application/json'
		        },
				body: JSON.stringify({ token: this.token, password: this.state.user_info.password })
		    })
		    .then( (response) => {
		    	response.json()
		    	.then( (success) => {
		   			this.setState({
		    			loading: false, //Verifying token exists
						verified: false, //Verified token exists
						invalid_token:  false, //Token is invalid
						processing: false, //Resetting password
						success: true, //Reset password
						failed: false
		    		}); 
		    		document.getElementsByTagName('form')[0].reset();		
		    	})
		    	.catch( (error) => {
		    		this.setState({
		    			loading: false, //Verifying token exists
						verified: false, //Verified token exists
						invalid_token:  false, //Token is invalid
						processing: false, //Resetting password
						success: true, //Reset password
						failed: 'Something went wrong while processing your request. Please try again later!'
					});
		    	});
		    })
		    .catch( (error) => {
		    	this.setState({
	    			loading: false, //Verifying token exists
					verified: false, //Verified token exists
					invalid_token:  false, //Token is invalid
					processing: false, //Resetting password
					success: true, //Reset password
					failed: 'Something went wrong while processing your request. Please try again later!'
				});
		    });
		}
		e.preventDefault();
		e.stopPropagation();
	}

	renderInputFields() {
		if(this.state.verified) {
			return (
				<div>
					<Form.Group controlId="formGridAddress1">
					    <Form.Label>New Password</Form.Label>
					    <Form.Control type="password" required  onChange={this.handleChange.bind(this, 'password')} name="address" placeholder="Enter Password" />
						{this.displayValidationErrors('password')}
					</Form.Group>

					<Row className="checkout-button">
						<Col md={12} xs={12}>
							{this.renderSubmitButton()}
						</Col>
						<Col md={12} xs={12}>
							<NavLink exact to="/forgot" className="pull-left">Forgot Password?</NavLink>
							<NavLink exact to="/signup" className="pull-right">Signup here</NavLink>
						</Col>
					</Row>
				</div>
			);
		}
	}

	render() {
		return (
			<div className="container container-verify">
				<Form noValidate onSubmit={e => this.handleSubmit(e)}>
					<Row className="top-class">
						<Col md={4} className="form-wrapper">
							<Row>
								<Col>
									{this.renderMessages()}
								</Col>
								{this.renderInputFields()}
							</Row>
						</Col>
					</Row>
				</Form>
			</div>
		);
	}
}

export default ResetPassword;