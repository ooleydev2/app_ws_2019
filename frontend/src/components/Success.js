import React from 'react';
import './Success.css';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/FormControl';
import Carousel from 'react-bootstrap/Carousel';
import Jumbotron from 'react-bootstrap/Jumbotron';
import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Accordion from 'react-bootstrap/Accordion';
import Alert from 'react-bootstrap/Alert';

class Success extends React.Component {
	render() {
		return (
			<div>
				<Jumbotron className="jumbotron-message">
		      	<Alert variant={'success'}>
				    <i style={{color: 'green'}} className="fa fa-check"></i> Successfully created subscription!
				</Alert>
		      </Jumbotron>
		    </div>
		);
	}
}

export default Success;