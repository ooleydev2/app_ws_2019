import React from 'react';
import './Login.css';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/FormControl';
import Carousel from 'react-bootstrap/Carousel';
import Jumbotron from 'react-bootstrap/Jumbotron';
import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Accordion from 'react-bootstrap/Accordion';
import Badge from 'react-bootstrap/Badge';
import Modal from 'react-bootstrap/Modal';
import config from '../config';
import { NavLink } from 'react-router-dom';
import validators from './common/login_validators';
import Alert from 'react-bootstrap/Alert';
import Spinner from 'react-bootstrap/Spinner';

class Login extends React.Component {
	state = {
		success: false,
		processing: false,
		failed: false,
		user_info: {
			email: '',
			password: ''
		}
	};
	validators = validators;

	renderMessages() {
		if(this.state.success) {
			return (
				<Alert variant={'success'}>
				    <Spinner
				      as="span"
				      animation="border"
				      size="sm"
				      role="status"
				      aria-hidden="true"
				    /> Successfully logged in. Please wait while we redirect you to your dashboard!
				</Alert>
			);
		}

		if(this.state.failed) {
			return (
				<Alert variant={'danger'}>
				   {this.state.failed}
				</Alert>
			);
		}
	}

	handleSubmit(e) {
		const that = this;
		if(this.isFormValid()) {
			this.setState({
				processing: true
			});
			const formData = {
				'email': this.state.user_info.email,
				'password': this.state.user_info.password
			}
			fetch(config.api_url+'api/users/login', {
				method: 'POST',
  				headers: {
		            'Content-Type': 'application/json'
		        },
  				body: JSON.stringify(formData)
			})
			.then( (response) => {
				response.json()
				.then( (success) => {
					if(response.status == 200) {
						that.setState({
							success: true,
							processing: false,
							failed: false
						});
						let timestamp = (new Date()).getTime() + (4 * 60 * 60 * 1000);
						document.cookie = "session_app=" + success.token + "; expires=" + new Date(timestamp) + "; path=/ ";
						document.cookie = "user_type=" + success.user_type + "; expires=" + new Date(timestamp) + "; path=/ ";
						that.props.onUpdatedSession('logged_in', success.user_type);
						setTimeout(function () {
							if (success.user_type == 'user') {
								that.props.history.push('/subscriptions');
							} else {
								that.props.history.push('/admin/users');
							}
						}, 2000);
					} else {
						that.setState({
							success: false,
							processing: false,
							failed: success.description ? success.description : 'Something went wrong while logging you in. Please try again later!' 
						});
					}
				})
				.catch( (error) => {
					that.setState({
						success: false,
						processing: false,
						failed: 'Something went wrong while logging you in. Please try again later!'
					});
				});
			})
			.catch( (error) => {
				that.setState({
					success: false,
					processing: false,
					failed: 'Something went wrong while logging you in. Please try again later!'
				});
			});
		}
		e.preventDefault();
		e.stopPropagation();
	}

	handleChange(name, event) {
		let fieldName = name;
		let fieldValue = event.target.value;
		this.updateValidators(fieldName, fieldValue);
		let values = this.state.user_info;
		values[name] = event.target.value;
		this.setState({
			user_info: values
		});
	}

	isFormValid() {
	    let status = true;
	    Object.keys(this.validators).forEach((field) => {
	      this.updateValidators(field, this.state.user_info[field]);
	      if (!this.validators[field].valid) {
	        status = false;
	      }
	    });
	    this.setState({ form_submitted: true });
	    return status;
	}

	updateValidators(fieldName, value) {
	    this.validators[fieldName].errors = [];
	    this.validators[fieldName].state = value;
	    this.validators[fieldName].valid = true;
	    this.validators[fieldName].rules.forEach((rule) => {
	    	if(!this.validators[fieldName].errors.length) {
	    		if (rule.test instanceof RegExp) {
			        if (!rule.test.test(value)) {
			          this.validators[fieldName].errors.push(rule.message);
			          this.validators[fieldName].valid = false;
			        }
			    } else if (typeof rule.test === 'function') {
			        if (!rule.test(value)) {
			          this.validators[fieldName].errors.push(rule.message);
			          this.validators[fieldName].valid = false;
			        }
			    }
	    	}	
	    });
	}

	renderSubmitButton() {
		if(this.state.processing) {
			return (
				<Button disabled variant="primary" className="margin-center" type="submit">
					<Spinner
				      as="span"
				      animation="border"
				      size="sm"
				      role="status"
				      aria-hidden="true"
				    /> Login
			    </Button>
			);
		} else {
			return (
				<Button variant="primary" className="margin-center" type="submit">
					Login
			    </Button>
			);
		}
	}

	displayValidationErrors(fieldName) {
	    const validator = this.validators[fieldName];
	    const result = '';
	    if (validator && !validator.valid) {
	      const errors = validator.errors.map((info, index) => {
	        return <span className="error" key={index}>* {info}</span>;
	      });
	      return (
	        <Row>
	        	<Col md={12} sm={12} className="text text-danger">
	        		{errors}
	        	</Col>
	        </Row>
	      );
	    }
	    return result;
	}
	
	render() {
		return (
			<div>
				<Form noValidate onSubmit={e => this.handleSubmit(e)}>
					<Row className="top-class">
						<Col md={4} className="form-wrapper">
							<Row>
								<Col>
									{this.renderMessages()}
								</Col>
							</Row>
							<Form.Row>
							    <Form.Group as={Col} controlId="formGridEmail">
							      <Form.Label>Email</Form.Label>
							      <Form.Control type="email" name="email" onChange={this.handleChange.bind(this, 'email')} required placeholder="example@gmail.com" />
							      {this.displayValidationErrors('email')}
							    </Form.Group>
							</Form.Row>

							<Form.Group controlId="formGridAddress1">
							    <Form.Label>Password</Form.Label>
							    <Form.Control type="password" required  onChange={this.handleChange.bind(this, 'password')} name="address" placeholder="Enter Password" />
								{this.displayValidationErrors('password')}
							</Form.Group>

							<Row className="checkout-button">
								<Col md={12} xs={12}>
									{this.renderSubmitButton()}
								</Col>
								<Col md={12} xs={12}>
									<NavLink exact to="/forgot" className="pull-left">Forgot Password?</NavLink>
									<NavLink exact to="/signup" className="pull-right">Signup here</NavLink>
								</Col>
							</Row>
						
						</Col>
					</Row>
				</Form>
			</div>
		);
	}
}

export default Login;