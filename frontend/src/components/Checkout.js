import React from 'react';
import './Checkout.css';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/FormControl';
import Carousel from 'react-bootstrap/Carousel';
import Jumbotron from 'react-bootstrap/Jumbotron';
import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Accordion from 'react-bootstrap/Accordion';
import InputGroup from 'react-bootstrap/InputGroup';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Tooltip from 'react-bootstrap/Tooltip';
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';
import Spinner from 'react-bootstrap/Spinner';
import {CardNumberElement, injectStripe, StripeProvider, Elements, CardExpiryElement, CardCVCElement} from 'react-stripe-elements';
import config from '../config';
import Alert from 'react-bootstrap/Alert';

class _CardForm extends React.Component {
  state = {
  	countries: [],
  	states: [],
  	cart: localStorage.getItem('selected_plans') ? JSON.parse(localStorage.getItem('selected_plans')) : '',
  	users: localStorage.getItem('users'),
  	loading: false,
  	total: 0,
  	users: 1,
	user_data: {},
	error: false  
  }

    getCookie(cname) {
	  var name = cname + "=";
	  var decodedCookie = decodeURIComponent(document.cookie);
	  var ca = decodedCookie.split(';');
	  for(var i = 0; i <ca.length; i++) {
	    var c = ca[i];
	    while (c.charAt(0) == ' ') {
	      c = c.substring(1);
	    }
	    if (c.indexOf(name) == 0) {
	      return c.substring(name.length, c.length);
	    }
	  }
	  return "";
	}

  calculatePrice() {
  	const that = this;
  	let total_price = 0;
  	let discount = config.discount;
  	let price_per_user = config.user_price;
  	let duration = JSON.parse(localStorage.getItem('cart')).type;
  	let totalMonthlyPrice = 0;
  	for(let k=0; k<this.state.cart.length; k++) {
  		totalMonthlyPrice = totalMonthlyPrice + (this.state.cart[k].price + ((this.state.cart[k].price * this.state.cart[k].fee)/100));
  	}
  	let price_per_user_annual = price_per_user - ((price_per_user * discount)/100);
  	let pricePlansMontly = totalMonthlyPrice - ((totalMonthlyPrice * discount)/100);

  	if(duration == 'anually') {
  		total_price = total_price + (price_per_user_annual * this.state.users * 12) + (pricePlansMontly * 12);
  	} else {
  		total_price = total_price + (price_per_user) + (totalMonthlyPrice);
  	}
  	
  	this.setState({ total: total_price, duration: duration });
  }

  componentDidMount() {
  	const that = this;
  	this.calculatePrice();
  	fetch(config.api_url+'api/location')
  	.then(response => {
		response.json()
		.then( (success) => {
			if(response.status == 200) {
				that.setState({
					countries: success.data
				});
			} else {
				that.setState({
					error: 'Error while fetching Countries. Please try again later!'
				});		
			}
		})
		.catch(error => {
			that.setState({
				error: 'Error while fetching Countries. Please try again later!'
			});
		});
  	})
  	.catch(error => {
		that.setState({
			error: 'Error while fetching Countries. Please try again later!'
		});
  	});

  	if(this.getCookie('session_app'))
	  	fetch(config.api_url+'api/users/details', {
	  		method: 'GET',
	  		headers: {
	  			'Content-Type': 'application/json',
	  			'Authorization': 'Bearer '+this.getCookie('session_app')
	  		}
	  	})
	  	.then( (response) => {
	  		if(response.status == 200) {
	  			response.json()
	  			.then( (success) => {
	  				this.setState({ 'user_data': success.data });
	  				if(success.data.country) {
	  					fetch(config.api_url+'api/location/'+success.data.country)
					  	.then(response => response.json())
					  	.then(response => {
					  		that.setState({
					  			states: response.data
					  		});
					  	})
					  	.catch(error => {
					  		console.log("Error");
					  		console.log(error);
					  	});
	  				}
	  			});
	  		}
	  	})
	  	.catch( (error) => {

	  	});
  }

  handleCountryChange(event) {
  	if(event.target.value) {
  		fetch(config.api_url+'api/location/'+event.target.value)
	  	.then(response => response.json())
	  	.then(response => {
	  		this.setState({
	  			states: response.data
	  		});
	  	})
	  	.catch(error => {
	  		console.log("Error");
	  		console.log(error);
	  	});
  	} else {
  		this.setState({
  			states: []
  		});
  	}
  }

  renderPrice() {
  	if(this.state.duration == 'anually') {
  		return (
  			<span>
  				{this.state.cart.length} Apps selected with {this.state.users} Users <br/>
  				<strong>Total : </strong> {Number(this.state.total).toFixed(2)} USD / Year
  			</span>
  		)
  	} else {
  		return (
  			<span>
  				{this.state.cart.length} Apps selected with {this.state.users} Users <br/>
  				<strong>Total : </strong> {Number(this.state.total).toFixed(2)} USD / Month
  			</span>
  		)
  	}
  }

  renderStatesOption() {
	if(this.state.states.length) {
		return (
			<Form.Group as={Col} controlId="formGridState">
			  <Form.Label>State</Form.Label>
			  <Form.Control value={this.state.user_data.state} onChange={(e) => this.updateValue('state', e.target.value)} name="state" as="select">
			    <option>Choose...</option>
			    {
			    	this.state.states.map( state => {
			    		return (
			    			<option value={state.id} key={state.id}>{state.name}</option>
			    		);
			    	})
			    }
			  </Form.Control>
			</Form.Group>
		);
	} else {
		return (
			<Form.Group as={Col}  name="state" controlId="formGridState">
			  <Form.Label>State</Form.Label>
			  <Form.Control as="select">
			    <option>Choose...</option>
			  </Form.Control>
			</Form.Group>
		);
	}
  }

  renderLoader() {
  	if(this.state.loading) {
  		return (
  			<Spinner
		      as="span"
		      animation="border"
		      size="sm"
		      role="status"
		      aria-hidden="true"
		    />
  		);
  	}
  }

  handleSubmit(event) {
  	const that = this;
  	const formData = {
		email: event.target.email.value,
		address: event.target.address.value,
		address_1: event.target.address_1.value,
		zipcode: event.target.zipcode.value,
		country: event.target.country.value,
		state: event.target.state.value,
		city: event.target.city.value,
		company_name: event.target.company_name.value,
		tin: event.target.tin.value
	};
	that.setState({
		loading: true,
		error: false
	});
  	this.props.stripe.createToken({type: 'card', name: 'Jenny Rosen'})
  	.then( success => {
  		if(!success.error) {	
			formData['users'] = this.state.users;
		  	formData['total'] = this.state.total;
			formData['discount'] = 0;
			formData['stripe_id'] = success.token.id;
			formData['interval'] = this.state.cart.type == 'anually' ? 'year' : 'month';
  			formData['plan_items'] = JSON.parse(localStorage.getItem('selected_plans'));
  			fetch(config.api_url+'api/location/subscription', {
  				method: 'POST',
  				headers: {
		            'Content-Type': 'application/json',
		            // 'Content-Type': 'application/x-www-form-urlencoded',
		        },
  				body: JSON.stringify(formData)
  			})
  			.then( response => {
				response.json()
				.then( (success) => {
					if(response.status == 200) {
						localStorage.setItem('selected_plans', '');
						that.props.history.push('/success');
					} else {
						that.setState({
							loading: false,
							error: 'Error while creating subscription. Please try again later!'
						});
					}
				})
				.catch( error => {
					that.setState({
						loading: false,
						error: 'Error while creating subscription. Please try again later!'
					});
				});
  			})
  			.catch( error => {
				that.setState({
					loading: false,
					error: 'Error while creating subscription. Please try again later!'
				});
  			});
  		} else {
			that.setState({
				loading: false,
				error: success.error.message ? success.error.message : 'Error while creating subscription. Please try again later!'
			});
  		}
  	})
  	.catch( error => {
		that.setState({
			loading: false,
			error: 'Error while creating subscription. Please try again later!'
		});
  	});
  	
  	event.preventDefault();
    event.stopPropagation();
  }

  componentDidUpdate() {
  	if(!this.state.cart.length) {
  		this.props.history.push('/services');
  	}
  }

  removeItem(index, event) {
  	const that = this;
  	let plans = JSON.parse(localStorage.getItem('selected_plans'));
  	plans.splice(index, 1);
  	localStorage.setItem('selected_plans', JSON.stringify(plans));
  	this.setState({
  		cart: plans
  	});
  	setTimeout(function() {
  		that.calculatePrice();
  	}, 500);
  }

    renderCart() {
		if(this.state.cart) {
			var rows = [];
			this.state.cart.map( (item, index) => {
	  			rows.push(
	  				<Col key={item.id} className="selected-price checkout-items">
		      			<Row className="product-column">
		      				<Col md="2" xs="2" className="padding-left-0">
		      					<img src={item.image} />
		      				</Col>
		      				<Col md="9" xs="9"  className="padding-left-0 padding-right-0">
		      					<Row className="padding-left-15">
		      						<Col>
		      							<strong>{item.name}</strong>
		      							<span className="pull-right info-pricing">
									        <a onClick={this.removeItem.bind(this, index)}>Remove</a>
									    </span>
		      						</Col>
		      					</Row>
		      					<Row className="padding-left-15">
		      						<Col>
		      							{Number(item.price+((item.price * item.fee)/100)).toFixed(2)} USD / month
		      						</Col>
		      					</Row>
		      				</Col>
		      			</Row>
		      		</Col>
	  			)
	  		})
	  		return rows;
		}
    }

    updateValue(field, value) {
    	let data = this.state.user_data;
    	data[field] = value;
    	this.setState({ user_data: data });
    }

	renderMessages() {
		if(this.state.error) {
			return (
				<Alert variant={'danger'}>
					{this.state.error}
				</Alert>
			);
		}
	}

  render() {
    return (
	    <Form onSubmit={e => this.handleSubmit(e)}>
	      	<Row>
		      	<Col md={6} xs={12}>
		      		{this.renderPrice()}
		      	</Col>
		    </Row>
			<Row>
				<Col md={6} xs={12}>
				    <Jumbotron className="pricing-address">
				      	<h5>Shipping <strong>Address</strong></h5>
				    </Jumbotron>
					<Form.Row>
					    <Form.Group as={Col} controlId="formGridEmail">
					      <Form.Label>Email</Form.Label>
					      <Form.Control value={this.state.user_data.email} onChange={(e) => this.updateValue('email', e.target.value)} type="email" name="email" required placeholder="example@gmail.com" />
					    </Form.Group>
					</Form.Row>

					<Form.Group controlId="formGridAddress1">
					    <Form.Label>Address</Form.Label>
					    <Form.Control required value={this.state.user_data.address} onChange={(e) => this.updateValue('address', e.target.value)} name="address" placeholder="1234 Main St" />
					</Form.Group>

					<Form.Group controlId="formGridAddress2">
					    <Form.Label>Address 2 (optional)</Form.Label>
					    <Form.Control required value={this.state.user_data.address_1} onChange={(e) => this.updateValue('address_1', e.target.value)} name="address_1" placeholder="Apartment, studio, or floor" />
					</Form.Group>

					<Form.Row>
					    <Form.Group as={Col} controlId="formGridCity">
					      <Form.Label>City</Form.Label>
					      <Form.Control value={this.state.user_data.city} onChange={(e) => this.updateValue('address', e.target.city)} name="city" required/>
					    </Form.Group>

					    <Form.Group as={Col} controlId="formGridZip">
					      <Form.Label>Zip</Form.Label>
					      <Form.Control value={this.state.user_data.zipcode} onChange={(e) => this.updateValue('zipcode', e.target.value)} name="zipcode" required/>
					    </Form.Group>
					</Form.Row>

					<Form.Row>
					    <Form.Group as={Col} controlId="formGridCity">
					      <Form.Label>Company Name</Form.Label>
					      <Form.Control value={this.state.user_data.company_name} onChange={(e) => this.updateValue('company_name', e.target.value)} name="company_name" required/>
					    </Form.Group>

					    <Form.Group as={Col} controlId="formGridZip">
					      <Form.Label>TIN/VAN</Form.Label>
					      <Form.Control value={this.state.user_data.tin} onChange={(e) => this.updateValue('tin', e.target.value)} name="tin" required/>
					    </Form.Group>
					</Form.Row>

					<Form.Row>
					  	<Form.Group as={Col} controlId="formGridState">
						  <Form.Label>Country</Form.Label>
						  <Form.Control value={this.state.user_data.country} onChange={(e) => this.updateValue('country', e.target.value)} required as="select" name="country" onChange={this.handleCountryChange.bind(this)}>
						    <option>Choose...</option>
						    {
						    	this.state.countries.map( (country, index) => {
						    		return (
						    			<option value={country.id} key={index}>{country.name}</option>
						    		);
						    	})
						    }
						  </Form.Control>
						</Form.Group>
						{this.renderStatesOption()}
					  </Form.Row>

					  <Jumbotron className="pricing-address">
				      	<h5>Payment <strong>Info</strong></h5>
				      </Jumbotron>

				      <Form.Row>
					    <Form.Group as={Col} controlId="formGridEmail">
					      <Form.Label>Card Number</Form.Label>
					      <CardNumberElement className="form-control" style={{base: {fontSize: '18px'}}}/>
					    </Form.Group>
					    <Form.Group as={Col} controlId="formGridEmail">
					      <Form.Label>Valid Through</Form.Label>
					      <CardExpiryElement className="form-control" style={{base: {fontSize: '18px'}}}/>
					    </Form.Group>
					</Form.Row>

					<Form.Row>
					    <Form.Group as={Col} controlId="formGridEmail">
					      <Form.Label>CVC</Form.Label>
					      <CardCVCElement className="form-control" style={{base: {fontSize: '18px'}}}/>
					    </Form.Group>
					</Form.Row>
					{this.renderMessages()}
					<Button variant="primary" type="submit">
					    Checkout
					    {this.renderLoader()}
					</Button>
				</Col>
				<Col>
					{this.renderCart()}
				</Col>
			</Row>
		</Form>
    )
  }
}
const CardForm = injectStripe(_CardForm)

class Checkout extends React.Component {
	state = {
		'selected_prices': [],
		'plans': [1,2,3,4,5,6,7,8,9],
		'users': 1
	}

	onItemClick(item, event,) {
		let selected = this.state.selected_prices;
		if(selected.indexOf(item) == -1) {
			selected.push(item);
		} else {
			let indexItem = selected.indexOf(item);
			selected.splice(indexItem, 1);
		}
		this.setState({
			'selected_prices': selected
		});
		console.log(this.state);
	}

	handleChange(event) {
		this.setState({
			'users': event.target.value
		});
	}

	render() {
		return (
			<div className="container container-checkout">
				<StripeProvider apiKey="pk_test_kmL967TxrEmkU7LcZAbMo6Vd00xkbfX5Sg">
					<Elements>
		    			<CardForm history={this.props.history}/>
		    		</Elements>
		    	</StripeProvider>
		    </div>
		);
	}
}

export default Checkout;