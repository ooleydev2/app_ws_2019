import React from 'react';
import './Home.css';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/FormControl';
import Carousel from 'react-bootstrap/Carousel';
import Jumbotron from 'react-bootstrap/Jumbotron';
import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Accordion from 'react-bootstrap/Accordion';
import Badge from 'react-bootstrap/Badge';
import Modal from 'react-bootstrap/Modal';
import config from '../config';
import Comparison from './common/Comparison';

class Home extends React.Component {
	state = {
		show: false,
		submitted: false,
		email: '',
		email_subscribed: false
	};

	handleClose() {
		this.setState({ show: false });
	}

	changeEmail(value) {
		this.setState({ email: value });

	}

	componentDidMount() {
		if(!this.getCookie('signed_subscrition')) {
			this.setState({ show: true });
		}
	}

	getCookie(name) {
	    var nameEQ = name + "=";
	    var ca = document.cookie.split(';');
	    for(var i=0;i < ca.length;i++) {
	        var c = ca[i];
	        while (c.charAt(0)==' ') c = c.substring(1,c.length);
	        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	    }
	    return null;
	}

	renderSuccess() {
		if(this.state.email_subscribed) {
			return (<span class='text text-success'>Successfully signed up for Email Newsletter!</span>);
		}
	}

	handleSubmit() {
		const that = this;
		const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		that.setState({ submitted: true });

		if(that.state.email.trim() && re.test(that.state.email)) {
			fetch(config.api_url+'api/newsletter', {
  				method: 'POST',
  				headers: {
		            'Content-Type': 'application/json'
		        },
  				body: JSON.stringify({ email: that.state.email })
  			})
  			.then( success => {
  				 that.setState({ email_subscribed: true });
  				 setTimeout(function() {
  				 	that.setState({ email_subscribed: false });
  				 	that.setState({ show: false });

				  	var expires = "";
			        var date = new Date();
			        date.setTime(date.getTime() + (30*24*60*60*1000));
			        expires = "; expires=" + date.toUTCString();
				
				    document.cookie = "signed_subscrition=yes"  + expires + "; path=/";
  				 }, 2000);
  			})
  			.catch( error => {
  				console.log(error);
  				alert("Error 1");
  			});
		}
	}

	renderEmailError() {
		const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

		if(this.state.submitted && !this.state.email.trim()) {
      		return (<span class='text text-danger'>Please enter email</span>);
      	} else if(this.state.submitted && !re.test(this.state.email)) {
      		return (<span class='text text-danger'>Please enter valid email</span>);
      	}
	}

	render() {
		return (
			<div>
		      <Carousel interval={5000}>
		        <Carousel.Item>
		          <Row className="row-carousel-what-we-do">
		          	<Col md={6} className="solutions-image-container" xs={12}>
		          		<img src={"images/software.jpg"} />
		          	</Col>
		          	<Col md={6} xs={12}>
		          		<Row>
		          			<Col><h2 className="font-normal">What we <strong>do?</strong></h2></Col>
		          		</Row>
		          		<Row>
		          			<Col className="what-we-do-paragraph">
		          				<p>Appmosphere is your trusted, objective, and “on-demand” technology consultant. Here’s a snapshot of what our team can provide to you:</p>
		          				<ul>
	          						<li>IT Infrastructure Designs/Feedback</li>
	          						<li>Assistance with screening/interviewing software developers/programmers for your projects/teams</li>
	          						<li>Custom Software Design, Development and Deployment</li>
	          						<li>Technical Writing/Documentation•“Train the Trainer” Services</li>
		          				</ul>
		          			</Col>
		          		</Row>
		          	</Col>
		          </Row>
		        </Carousel.Item>
		        <Carousel.Item>
		          <Row className="row-carousel-what-we-do">
		          	<Col md={6} className="solutions-image-container" xs={12}>
		          		<img src={"images/odoo_partner.png"} />
		          	</Col>
		          	<Col md={6} xs={12}>
		          		<Row>
		          			<Col><h2 className="font-normal">Odoo official <strong>partner</strong></h2></Col>
		          		</Row>
		          		<Row>
		          			<Col className="what-we-do-paragraph">
		          				<p>Appmosphere is an official partner and odoo developer.  If your not aware, odoo is a one stop shop Enterprise Relationship Management platform.   We can seamlessly migrate you away from disparate other platforms (SAP, Salesforce, etc) to one efficient system that can easily scale to your business.   We can consult with you team on how to make the most of the odoo platform, make custom modifications and generally get the most bang for your buck.  Many times our consultancy pays for itself within the first two months of migrating to odoo!  Schedule a free consultation today!</p>
		          			</Col>
		          		</Row>
		          	</Col>
		          </Row>
		        </Carousel.Item>
		        <Carousel.Item>
		          <Row className="row-carousel-what-we-do">
		          	<Col md={6} className="solutions-image-container" xs={12}>
		          		<img className="erm-image" src={"images/erm.jpg"} />
		          	</Col>
		          	<Col md={6} xs={12}>
		          		<Row>
		          			<Col><h2 className="font-normal">Enterprise Relationship <strong>Management</strong></h2></Col>
		          		</Row>
		          		<Row>
		          			<Col className="what-we-do-paragraph">
		          				<p>Does your company need any of the following?  If so, we can build “ala carte” a cost effective solution for your company that can easily scale to the size of your business.</p>	
		          				<ul>
		          					<li>Point of Sale, e-commerce, automated marketing</li>
		          					<li>Help Desk/IT</li>
		          					<li>Customer Management System (e-mail marketing)</li>
		          					<li>Invoicing</li>
		          					<li>HR/Payroll/Timesheets</li>
		          					<li>Inventory Management</li>
		          					<li>Manufacturing/Supply Chain Management•Documentation</li>
		          				</ul>
		          			</Col>
		          		</Row>
		          	</Col>
		          </Row>
		        </Carousel.Item>
		        <Carousel.Item>
		          <Row className="row-carousel-what-we-do">
		          	<Col md={6} className="solutions-image-container" xs={12}>
		          		<img className="erm-image" src={"images/code_review.png"} />
		          	</Col>
		          	<Col md={6} xs={12}>
		          		<Row>
		          			<Col><h2 className="font-normal">Code <strong>Review</strong></h2></Col>
		          		</Row>
		          		<Row>
		          			<Col className="what-we-do-paragraph">
		          				<p>For as little as $1000, you can have an objective 3rd party review of your code.  This can help protect your investment, and also provide your programming team with actionable feedback to correct code before it’s too late.   Or, maybe you, yourself are a programmer and are looking for some ideas or feedback on your code.   Call today for a free consultation and estimate for your code review.</p>
		          			</Col>
		          		</Row>
		          	</Col>
		          </Row>
		        </Carousel.Item>
		        <Carousel.Item>
		          <Row className="row-carousel-what-we-do">
		          	<Col md={6} className="solutions-image-container" xs={12}>
		          		<img className="erm-image" src={"images/ethical_hacking.png"} />
		          	</Col>
		          	<Col md={6} xs={12}>
		          		<Row>
		          			<Col><h2 className="font-normal">Ethical <strong>Hacking</strong></h2></Col>
		          		</Row>
		          		<Row>
		          			<Col className="what-we-do-paragraph">
		          				<p>Test the security of your system by working with Appmosphere to provide “ethical hacking”.  Identify and proactively protect your systems.  If you don’t do it……..someone else may (and put your business data at risk).</p>
		          			</Col>
		          		</Row>
		          	</Col>
		          </Row>
		        </Carousel.Item>
		      </Carousel>

		      <Comparison />

		      <Jumbotron className="intro-section">
		        <h2 className="text-align-center heading-jumbo">Services</h2>
		        
		        <Container>
		          <Row>
		            <Col>
		              <Card className="align-center" style={{ width: '18rem' }}>
		                <Card.Body className="services-card-body">
		                  <Card.Text>
		                    <Row>
		                  		<Col md={3}><img className="services-image" src={"images/web.png"}/></Col>
		                  		<Col md={9}>
		                  			<Row><Col>Services</Col></Row>
		                  			<Row>
		                  				<Col>
		                  					<Badge variant="primary" className="services-badge">Enterprise Solutions</Badge>
		                  					<Badge variant="primary" className="services-badge">Web Solutions</Badge>
		                  					<Badge variant="primary" className="services-badge">E Commerce</Badge>
		                  					<Badge variant="primary" className="services-badge">Contracted Developer Management</Badge>
		                  					<Badge variant="primary" className="services-badge">Code Reviews</Badge>
		                  					<Badge variant="primary" className="services-badge">Developer Technical Interviews and Placement</Badge>
		                  				</Col>
		                  			</Row>
		                  		</Col>	
		                  	</Row>
		                  </Card.Text>
		                </Card.Body>
		              </Card>
		            </Col>
		            <Col>
		              <Card className="align-center" style={{ width: '18rem' }}>
		                <Card.Body className="services-card-body">
		                  <Card.Text>
		                    <Row>
		                  		<Col md={3}><img className="services-image" src={"images/web.png"}/></Col>
		                  		<Col md={9}>
		                  			<Row><Col>Web . eCommerce . Security, Access, Mobile and Analytics</Col></Row>
		                  			<Row>
		                  				<Col>
		                  					<Badge variant="primary" className="services-badge">iOS/iPhone App Development</Badge>
		                  					<Badge variant="primary" className="services-badge">Android App Development</Badge>
		                  					<Badge variant="primary" className="services-badge">JavaScript  - React and Angular Frameworks</Badge>
		                  					<Badge variant="primary" className="services-badge">ERP</Badge>
		                  					<Badge variant="primary" className="services-badge">ODOO</Badge>
		                  					<Badge variant="primary" className="services-badge">Cysco</Badge>
		                  					<Badge variant="primary" className="services-badge">OpenPath</Badge>
		                  					<Badge variant="primary" className="services-badge">Magento</Badge>
		                  					<Badge variant="primary" className="services-badge">WordPress</Badge>
		                  					<Badge variant="primary" className="services-badge">Python</Badge>
		                  				</Col>
		                  			</Row>
		                  		</Col>	
		                  	</Row>
		                  </Card.Text>
		                </Card.Body>
		              </Card>
		            </Col>
		            <Col>
		              <Card className="align-center" style={{ width: '18rem' }}>
		                <Card.Body className="services-card-body">
		                  <Card.Text>
		                    <Row>
		                  		<Col md={3}><img className="services-image" src={"images/web.png"}/></Col>
		                  		<Col md={9}>
		                  			<Row><Col>DevOps</Col></Row>
		                  			<Row>
		                  				<Col>
		                  					<Badge variant="primary" className="services-badge">Jenkins</Badge>
		                  					<Badge variant="primary" className="services-badge">Docker</Badge>
		                  					<Badge variant="primary" className="services-badge">AWS</Badge>
		                  					<Badge variant="primary" className="services-badge">Atlassian Management</Badge>
		                  				</Col>
		                  			</Row>
		                  		</Col>	
		                  	</Row>
		                  </Card.Text>
		                </Card.Body>
		              </Card>
		            </Col>
		          </Row>
		          <div class="separator"></div>
		          <Row>
		            <Col md={4} xs={12}>
		              <Card className="align-center" style={{ width: '18rem' }}>
		                <Card.Body className="services-card-body">
		                  <Card.Text>
		                    <Row>
		                  		<Col md={3}><img className="services-image" src={"images/web.png"}/></Col>
		                  		<Col md={9}>
		                  			<Row><Col>Technology Consultancy</Col></Row>
		                  			<Row>
		                  				<Col>
		                  					<Badge variant="primary" className="services-badge">System Design/Technical Documentation</Badge>
											<Badge variant="primary" className="services-badge">Code Reviews/Project Management/Dev Ops</Badge>
											<Badge variant="primary" className="services-badge">ERM Consultation</Badge>
											<Badge variant="primary" className="services-badge">Point of Sale/E-Commerce</Badge>
											<Badge variant="primary" className="services-badge">Development Operations/Project Management</Badge>
											<Badge variant="primary" className="services-badge">Programmer/Development Team Selection</Badge>
											<Badge variant="primary" className="services-badge">UI/UX Testing</Badge>
											<Badge variant="primary" className="services-badge">Business Analytics</Badge>
											<Badge variant="primary" className="services-badge">iOT (Smart Home/Business Design and Implementation)</Badge>
		                  				</Col>
		                  			</Row>
		                  		</Col>	
		                  	</Row>
		                  </Card.Text>
		                </Card.Body>
		              </Card>
		            </Col>
		            <Col md={4} xs={12}>
		              <Card className="align-center" style={{ width: '18rem' }}>
		                <Card.Body className="services-card-body">
		                  <Card.Text>
		                    <Row>
		                  		<Col md={3}><img className="services-image" src={"images/web.png"}/></Col>
		                  		<Col md={9}>
		                  			<Row><Col>Custom Software Development</Col></Row>
		                  			<Row>
		                  				<Col>
		                  					<Badge variant="primary" className="services-badge">Mobile Apps (iOS, Android, etc)</Badge>
											<Badge variant="primary" className="services-badge">Web Design/Web Apps</Badge>
											<Badge variant="primary" className="services-badge">Customizations on odoo, Shopify, or Facebook Apps</Badge>
		                  				</Col>
		                  			</Row>
		                  		</Col>	
		                  	</Row>
		                  </Card.Text>
		                </Card.Body>
		              </Card>
		            </Col>
		          </Row>
		        </Container>
		      </Jumbotron>
		      <Jumbotron className="showcase-jumbo">
		        <Row>
		          <Col>
		            <img className="showcase-image" src={"images/showcase-homepage.png"}/>
		          </Col>
		          <Col>
		            <h2>Our values & Culture</h2>
		            <Accordion defaultActiveKey="0">
		              
		              <Card>
		                <Card.Header>
		                  <Accordion.Toggle as={Button} variant="link" eventKey="0">
		                    Relationships
		                  </Accordion.Toggle>
		                </Card.Header>
		                <Accordion.Collapse eventKey="0">
		                  <Card.Body>
		                  	<p>At Appmosphere, we enter relationships with our clients in the spirit of partnership.  We seek to understand your business, and it’s challenges, and bring forth recommendations that scale to your business and needs.</p>
		                  	<p>We also value building the future of technology by sponsoring educational programs for those interested in growing their technical skills.  Ask us about our “boot camps” on cruise ships:  Developers at Sea!</p>  
		                  </Card.Body>
		                </Accordion.Collapse>
		              </Card>

		              <Card>
		                <Card.Header>
		                  <Accordion.Toggle as={Button} variant="link" eventKey="1">
		                    Knowledge
		                  </Accordion.Toggle>
		                </Card.Header>
		                <Accordion.Collapse eventKey="1">
		                  <Card.Body>
		                  	<p>Our consultants have over 30 years of IT experience.  That history gives our team a unique perspective and tool kit to bring to your business issues.  In addition, our team invests about 25% of our time in continuing education to make sure we can bring the “cutting edge” of technology and thought leadership to help solve our client’s business challenges.</p>	
		                  </Card.Body>
		                </Accordion.Collapse>
		              </Card>

		              <Card>
		                <Card.Header>
		                  <Accordion.Toggle as={Button} variant="link" eventKey="2">
		                    Honesty
		                  </Accordion.Toggle>
		                </Card.Header>
		                <Accordion.Collapse eventKey="2">
		                  <Card.Body>
		                  	<p>Our clients have developed trusting relationships with our consultants.  Our consultants will bring forward the best solution, even if it’s recommending a solution that does not directly benefit Appmosphere.  Our actions speak for themselves!</p>
		                  </Card.Body>
		                </Accordion.Collapse>
		              </Card>

		              <Card>
		                <Card.Header>
		                  <Accordion.Toggle as={Button} variant="link" eventKey="3">
		                    Transparency
		                  </Accordion.Toggle>
		                </Card.Header>
		                <Accordion.Collapse eventKey="3">
		                  <Card.Body>
		                  	<p>Often times technological solutions fall into a “black box” of “proprietary” information.  At Appmosphere we believe these solutions are bought and paid for by our clients.  Anything Appmosphere develops on behalf of our clients is shared and owned by the clients.  They can (and do) get “under the hood” of what we are working on all the time.  Nothing to hide here!</p>  
		                  </Card.Body>
		                </Accordion.Collapse>
		              </Card>

		            </Accordion>
		          </Col>
		        </Row>
		      </Jumbotron>
		      <Jumbotron className="jumbotron-bg">
		        <div className="col-red-bg">
		            <p className="jumbotron-bg-text">
		              We are a proud odoo service provider.
		            </p>
		        </div>
		      </Jumbotron>
		      <Jumbotron nextIcon="<span></span>" prevIcon="<span></span>" className="jumbotron-review">
		          <Row>
		            <Col md={{ size: 10, offset: 1 }}>
		              <h4>What people are saying</h4>
		              <Carousel interval={5000}>
				        <Carousel.Item>
				          <Row>
				          	<Col md={12} xs={12}>
				          		<Row>
				          			<Col className="what-we-do-paragraph">
				          				<p>Appmosphere is having in-depth expertise for web and mobile app technology. We are known as global sourcing partner for providing right solution in short span of time with unmatched design quality and technology innovation. In addition, we provide mobile app technology solution for Android, iOS which is 98% of the mobile market.</p>
				          				<p>The worldwide presence and global project execution model make Appmosphere a preferred partner for international companies seeking a web technology solution targeted for the UK and US.</p>
				          			</Col>
				          		</Row>
				          		<Row>
				          			<Col>
				          				<Card style={{ width: '18rem' }}>
							                <Card.Body>
							                  <Card.Text>
							                    <Row>
							                  		<Col md={3}><img className="services-image" src={"images/web.png"}/></Col>
							                  		<Col md={9}>
							                  			<Row><Col>Services</Col></Row>
							                  			<Row><Col>CEO ABC. Company</Col></Row>
							                  		</Col>	
							                  	</Row>
							                  </Card.Text>
							                </Card.Body>
							              </Card>
				          			</Col>
				          		</Row>
				          	</Col>
				          </Row>
				        </Carousel.Item>
				        <Carousel.Item>
				          <Row>
				          	<Col md={12} xs={12}>
				          		<Row>
				          			<Col className="what-we-do-paragraph">
				          				<p>Appmosphere is having in-depth expertise for web and mobile app technology. We are known as global sourcing partner for providing right solution in short span of time with unmatched design quality and technology innovation. In addition, we provide mobile app technology solution for Android, iOS which is 98% of the mobile market.</p>
				          				<p>The worldwide presence and global project execution model make Appmosphere a preferred partner for international companies seeking a web technology solution targeted for the UK and US.</p>
				          			</Col>
				          		</Row>
				          		<Row>
				          			<Col>
				          				<Card style={{ width: '18rem' }}>
							                <Card.Body>
							                  <Card.Text>
							                    <Row>
							                  		<Col md={3}><img className="services-image" src={"images/web.png"}/></Col>
							                  		<Col md={9}>
							                  			<Row><Col>Services</Col></Row>
							                  			<Row><Col>CEO ABC. Company</Col></Row>
							                  		</Col>	
							                  	</Row>
							                  </Card.Text>
							                </Card.Body>
							              </Card>
				          			</Col>
				          		</Row>
				          	</Col>
				          </Row>
				        </Carousel.Item>
				      </Carousel>
		            </Col>
		          </Row>
		      </Jumbotron>
		      	<Modal show={this.state.show} onHide={this.handleClose.bind(this)}>
		          <Modal.Header closeButton>
		            <Modal.Title>Subscribe to our <b>Newsletter</b></Modal.Title>
		          </Modal.Header>
		          <Modal.Body>
		          	<Form>
					  <Form.Row>
					    <Form.Group as={Col} controlId="formGridEmail">
					      <Form.Label>Email</Form.Label>
					      <Form.Control type="email" onChange={ e => this.changeEmail(e.target.value) } name="email" required placeholder="example@gmail.com" />
					      {
					      	this.renderEmailError()
					      }
					    </Form.Group>
					  </Form.Row>
					</Form>
		          </Modal.Body>
		          <Modal.Footer>
		            <Button variant="secondary" onClick={this.handleClose.bind(this)}>
		              Cancel
		            </Button>
		            <Button variant="primary" onClick={this.handleSubmit.bind(this)}>
		              Signup
		            </Button>
		          </Modal.Footer>
		        </Modal>
		    </div>
		);
	}
}

export default Home;