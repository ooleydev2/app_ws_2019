import React from 'react';
import './Contact.css';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/FormControl';
import Carousel from 'react-bootstrap/Carousel';
import Jumbotron from 'react-bootstrap/Jumbotron';
import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Alert from 'react-bootstrap/Alert';
import Modal from 'react-bootstrap/Modal';
import Accordion from 'react-bootstrap/Accordion';
import InputGroup from 'react-bootstrap/InputGroup';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Tooltip from 'react-bootstrap/Tooltip';
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';
import Spinner from 'react-bootstrap/Spinner';
import ProgressBar from 'react-bootstrap/ProgressBar';
import {CardNumberElement, injectStripe, StripeProvider, Elements, CardExpiryElement, CardCVCElement} from 'react-stripe-elements';
import config from '../config';
import Calendar from 'react-calendar';
import moment from 'moment';
import Information from './questionarie/Information';
import Experience from './questionarie/Experience';
import Consult from './questionarie/Consult';
import SolutionsInterested from './questionarie/SolutionsInterested';
import validators from './common/AppointmentValidators';

class Contact extends React.Component {
	state = {
		user_data: {
			name: '',
			email: '',
			message: ''
		},
		appointment_data: {
			primary_name: '',
			primary_email: '',
			primary_phone: '',
			business_name: '',
			business_address: '',
			business_website: '',
			experience: '',
			consult_checkbox: [],
			solutions_interested: [],
			free_appointment: '',
			custom_application: '',
			contact_person: ''
		},
		loading: false,
		date: new Date(),
		current_date: new Date(),
		show: false,
		consult_options: [
			'Invoicing/Bookkeeping/Sales',
			'Point of Sale System', 
			'E-Commerce',
			'Invoicing',
			'Proposals/Quotes',
			'Accounting/Bookkeeping',
			'Marketing',
			'Customer Management System (CRM)',
			'Website',
			'E-Mail Marketing',
			'Events',
			'Appointments/Calendars',
			'Marketing Automation',
			'Project/Supply Chain Management',
			'Inventory Management',
			'Manufacturing Management',
			'Purchasing/Procurement',
			'Project Management',
			'Subscriptions',
			'Document Management',
			'Signatures/Routing',
			'PLM',
			'HR/Employee Management',
			'Timecards',
			'Expenses',
			'Recruitment',
			'Leaves',
			'Help Desk'
		],
		current_state: 0
	};
	validators = validators;
	valid_states = [['primary_name', 'primary_email', 'primary_phone', 'business_name', 'business_address', 'business_website'], ['experience'], ['consult_checkbox'], ['solutions_interested'], ['free_appointment'], ['custom_application'], ['contact_person']];

	updateValidators(fieldName, value) {
	    this.validators[fieldName].errors = [];
	    this.validators[fieldName].state = value;
	    this.validators[fieldName].valid = true;
	    this.validators[fieldName].rules.forEach((rule) => {
	    	if(!this.validators[fieldName].errors.length) {
	    		if (rule.test instanceof RegExp) {
			        if (!rule.test.test(value)) {
			          this.validators[fieldName].errors.push(rule.message);
			          this.validators[fieldName].valid = false;
			        }
			    } else if (typeof rule.test === 'function') {
			        if (!rule.test(value)) {
			          this.validators[fieldName].errors.push(rule.message);
			          this.validators[fieldName].valid = false;
			        }
			    }
	    	}	
	    });
	}
	getPercentage() {
		return Number((this.state.current_state / 6) * 100).toFixed(0); 
	}

	getProgressBar() {
		return (<ProgressBar now={this.getPercentage()} label={this.getPercentage()+'%'} />);
	}

	goToNextState() {
		const that = this;
		let valid = true;

		this.valid_states[this.state.current_state].forEach( (item) => {
			that.updateValidators(item, this.state.appointment_data[item]);
			if(!this.validators[item].valid) {
				valid = false;
			}
		});
		if(!valid) {
			this.setState({
				current_state: this.state.current_state
			});
			return;
		}

		this.setState({
			current_state: (this.state.current_state + 1)
		});
	}

	goToPrevState() {
		this.setState({
			current_state: (this.state.current_state - 1)
		});
		console.log(this.state);
	}

	updateValue(name, value) {
	  	let user_data = this.state.user_data;
	  	user_data[name] = value;
	  	this.setState({
	  		user_data: user_data
	  	});
	}

	renderNextButton() {
		if(this.state.current_state == 6) {
			if(this.state.appointment_loading) {
				return (
					<Button disabled className="pull-right" variant="primary" onClick={this.handleAppointmentSubmit.bind(this)}>
					<Spinner
				      as="span"
				      animation="border"
				      size="sm"
				      role="status"
				      aria-hidden="true"
				    />
					Submit</Button>
				);	
			} else {
				return (
					<Button className="pull-right" variant="primary" onClick={this.handleAppointmentSubmit.bind(this)}>Submit</Button>
				);
			}
		} else {
			return (
				<Button className="pull-right" variant="primary" onClick={this.goToNextState.bind(this)}>Next</Button>
			);
		}
	}

	renderPrevButton() {
		if(this.state.appointment_loading) {
			return (
				<Button disabled className="pull-left" variant="primary" onClick={this.goToPrevState.bind(this)}>Previous</Button>
			);
		} else {
			return (
				<Button className="pull-left" variant="primary" onClick={this.goToPrevState.bind(this)}>Previous</Button>
			);
		}
	}

	renderQuestionarieSection() {
		if(this.state.current_state == 0) {
			return (
				<Information appointment_data={this.state.appointment_data} displayValidationErrors={this.displayValidationErrors.bind(this)} valueChanged={this.updateAppointmentValue.bind(this)}/>
			);
		}

		if(this.state.current_state == 1) {
			return (
				<Experience data={this.state.appointment_data} displayValidationErrors={this.displayValidationErrors.bind(this)} valueChanged={this.updateAppointmentValue.bind(this)}/>
			);
		}

		if(this.state.current_state == 2) {
			return (
				<Consult data={this.state.appointment_data.consult_checkbox} displayValidationErrors={this.displayValidationErrors.bind(this)} consult_options={this.state.consult_options} valueChanged={this.updateAppointmentValue.bind(this)}/>
			);
		}
		
		if(this.state.current_state == 3) {
			return (
				<SolutionsInterested data={this.state.appointment_data.solutions_interested} displayValidationErrors={this.displayValidationErrors.bind(this)} consult_options={this.state.consult_options} valueChanged={this.updateAppointmentValue.bind(this)}/>
			);
		}
			
		if(this.state.current_state == 4) {
			return (
				<Row>
					<Col>
						<Form.Row>
							<Col>
								<strong>Are you interested in a free consultation appointment with a member of our team?</strong>
							</Col>
						</Form.Row>

						<Form.Row>
						    <Form.Group as={Col} controlId="formGridEmail">
						        <Row>
						        	<Col>
						        		<Form.Control value="0" checked={this.state.appointment_data.free_appointment == '0'} onChange={(e) => this.updateAppointmentValue('free_appointment', e.target.value)} name="free_appointment" className="radio-options" type="radio" id="first_free_consultation"/><label for="first_free_consultation" className="radio-label">Yes, that would be helpful!</label>
						        	</Col>
						        </Row>
								<Row>
									<Col>
										<Form.Control  value="1" checked={this.state.appointment_data.free_appointment == '1'} onChange={(e) => this.updateAppointmentValue('free_appointment', e.target.value)} name="free_appointment" className="radio-options" type="radio" id="second_free_consultation"/><label for="second_free_consultation" className="radio-label">No, no need to chitty-chat.  Let's just get started!</label>
									</Col>
								</Row>
						    </Form.Group>
						</Form.Row>
						{this.displayValidationErrors('free_appointment')}
					</Col>
				</Row>
			);
		}			

		if(this.state.current_state == 5) {
			return (
				<Row>
					<Col>
						<Form.Row>
							<Col>
								<strong>Are you interested in custom applications for your business?  (either stand-alone apps, or custom modifications to existing Odoo apps)?</strong>
							</Col>
						</Form.Row>

						<Form.Row>
						    <Form.Group as={Col} controlId="formGridEmail">
						        <Row>
						        	<Col>
						        		<Form.Control checked={this.state.appointment_data.custom_application == '0'} value="0" onChange={(e) => this.updateAppointmentValue('custom_application', e.target.value)} name="custom_application" className="radio-options" type="radio" id="first_custom_"/><label for="first_custom_" className="radio-label">Yes, definitely...let's talk!</label>
						        	</Col>
						        </Row>
								<Row>
									<Col>
										<Form.Control checked={this.state.appointment_data.custom_application == '1'} value="1" onChange={(e) => this.updateAppointmentValue('custom_application', e.target.value)} name="custom_application"  className="radio-options" type="radio" id="second_custom_"/><label for="second_custom_" className="radio-label">I'm not sure yet....let's discuss</label>
									</Col>
								</Row>
								<Row>
									<Col>
										<Form.Control value="2" checked={this.state.appointment_data.custom_application == '2'} onChange={(e) => this.updateAppointmentValue('custom_application', e.target.value)} name="custom_application"  className="radio-options" type="radio" id="third_custom_"/><label for="third_custom_" className="radio-label">No, just sticking to what's available</label>
									</Col>
								</Row>
						    </Form.Group>
						</Form.Row>
					</Col>
					<Col xs={12} md={12}>
						{this.displayValidationErrors('custom_application')}
					</Col>
				</Row>
				
			);
		}		
		
		if(this.state.current_state == 6) {
			return (
				<Row>
					<Col>
						<Form.Row>
							<Col>
								<strong>Do you have a point-person within your organization that will be the main Point of Contact for managing the ERM/Odoo solutions?</strong>
							</Col>
						</Form.Row>

						<Form.Row>
						    <Form.Group as={Col} controlId="formGridEmail">
						        <Row>
						        	<Col>
						        		<Form.Control value="0" checked={this.state.appointment_data.contact_person == '0'} onChange={(e) => this.updateAppointmentValue('contact_person', e.target.value)} name="contact_person" className="radio-options" type="radio" id="first_point"/><label for="first_point" className="radio-label">Yes</label>
						        	</Col>
						        </Row>
								<Row>
									<Col>
										<Form.Control  value="1" checked={this.state.appointment_data.contact_person == '1'} onChange={(e) => this.updateAppointmentValue('contact_person', e.target.value)} name="contact_person" className="radio-options" type="radio" id="second_point"/><label for="second_point" className="radio-label">Not sure yet</label>
									</Col>
								</Row>
								<Row>
									<Col>
										<Form.Control  value="2" checked={this.state.appointment_data.contact_person == '2'} onChange={(e) => this.updateAppointmentValue('contact_person', e.target.value)} name="contact_person" className="radio-options" type="radio" id="third_point"/><label for="third_point" className="radio-label">No, there are many people involved</label>
									</Col>
								</Row>
						    </Form.Group>
						</Form.Row>
					</Col>
				</Row>
			);
		}
	}

	displayValidationErrors(fieldName) {
	    const validator = this.validators[fieldName];
	    const result = '';
	    if (validator && !validator.valid) {
	      const errors = validator.errors.map((info, index) => {
	        return <span className="error" key={index}>* {info}</span>;
	      });
	      return (
	        <Row>
	        	<Col md={12} sm={12} className="text text-danger">
	        		{errors}
	        	</Col>
	        </Row>
	      );
	    }
	    return result;
	}

	updateAppointmentValue(name, value) {
		const that = this;
		let appointment_data = this.state.appointment_data;
		if(name == 'consult_checkbox') {
			let options = appointment_data.consult_checkbox;
			if(options.indexOf(value) != -1) {
				let findIndex = options.indexOf(value);
				options.splice(findIndex, 1);
				appointment_data['consult_checkbox'] = options;
			} else {
				appointment_data['consult_checkbox'].push(value);
			}
			that.updateValidators('consult_checkbox', appointment_data['consult_checkbox']);
		} else if(name == 'solutions_interested') {
			let options = appointment_data.solutions_interested;
			if(options.indexOf(value) != -1) {
				let findIndex = options.indexOf(value);
				options.splice(findIndex, 1);
				appointment_data['solutions_interested'] = options;
			} else {
				appointment_data['solutions_interested'].push(value);
			}
			that.updateValidators('solutions_interested', appointment_data['solutions_interested']);
		} else {
			appointment_data[name] = value;
			that.updateValidators(name, value);
		}

	  	this.setState({
	  		appointment_data: appointment_data
	  	});
	}

	handleAppointmentSubmit(e) {
		const that = this;
		let valid = true;
		this.valid_states[6].forEach( (item) => {
			that.updateValidators(item, this.state.appointment_data[item]);
			if(!this.validators[item].valid) {
				valid = false;
			}
		});
		if(!valid) {
			return;
		} else {
			this.setState({
				appointment_loading: true
			});
			let data = this.state.appointment_data;
			data['date'] = moment(this.state.current_date).format('YYYY-MM-DD');
			try {
				fetch(config.api_url+'api/newsletter/appointment', {
				method: 'POST',
				headers: {
		            'Content-Type': 'application/json'
		        },
				body: JSON.stringify(data)
			})
			.then( (response) => {
				response.json()
				.then( (success) => {
					if(response.status == 200) {
						that.setState({ appointment_success: true });
						setTimeout(function() {
							that.setState({
								appointment_data: {
									primary_name: '',
									primary_email: '',
									primary_phone: '',
									business_name: '',
									business_address: '',
									business_website: '',
									experience: '',
									consult_checkbox: [],
									solutions_interested: [],
									free_appointment: '',
									custom_application: '',
									contact_person: ''
								},
								current_state: 0,
								show: false,
								appointment_loading: false,
								appointment_success: false
							});
						}, 3000);
					} else {
						that.setState({ appointment_loading:false, appointment_success: false, appointment_error: 'Something went wrong while sending Message. Please try again later!'});
					}
				})
				.catch( (error) => {
					that.setState({ appointment_loading:false, appointment_success: false, appointment_error: 'Something went wrong while sending Message. Please try again later!'});
				});
			});		
			} catch(error) {
				console.log(error);
			}
		}
	}

	handleSubmit(e) {
		const that = this;
		this.setState({
			loading: true
		});
		try {
			fetch(config.api_url+'api/newsletter/contact_us', {
			method: 'POST',
			headers: {
	            'Content-Type': 'application/json'
	        },
			body: JSON.stringify(this.state.user_data)
		})
		.then( (response) => {
			response.json()
			.then( (success) => {
				if(response.status == 200) {
					that.setState({ success: true, loading: false });
					that.setState({
						user_data: {
							name: '',
							email: '',
							message: ''
						}
					});
				} else {
					that.setState({ loading:false, success: false, error: 'Something went wrong while sending Message. Please try again later!'});
				}
			})
			.catch( (error) => {
				that.setState({ loading:false, success: false, error: 'Something went wrong while sending Message. Please try again later!'});
			});
		});		
		} catch(error) {
			console.log(error);
		}
		

		e.preventDefault();
		e.stopPropagation();
	}

	renderMessagesAppointment() {
		if(this.state.appointment_success) {
			return (
				<Alert variant={'success'}>
				    Successfully scheduled appointment. We will get back to you soon!
				</Alert>
			);
		}

		if(this.state.appointment_error) {
			return (
				<Alert variant={'danger'}>
				   {this.state.failed_appointment}
				</Alert>
			);
		}
	}

	renderMessages() {
		if(this.state.success) {
			return (
				<Alert variant={'success'}>
				    Successfully sent Message. We will get back to you soon!
				</Alert>
			);
		}

		if(this.state.failed) {
			return (
				<Alert variant={'danger'}>
				   {this.state.failed}
				</Alert>
			);
		}
	}

	onChange(name, value) {
		let user_data = this.state.user_data;
		user_data[name] = value;
		this.setState({ user_data: user_data });
	}


	onChangeDate(date) {
		this.setState({ current_date: moment(date).format("YYYY-MM-DD HH:mm:ss").toString(), show: true });
	}

	handleClosePopup() {
		this.setState({ show: false });
	}

 	render() {
 		return (
 			<div className="container form-container">
 				<Modal show={this.state.show} size="lg">
		          <Modal.Header closeButton  onClick={this.handleClosePopup.bind(this)}>
		            <Modal.Title>Book an appointment for  {moment(this.state.current_date).format('LL')}</Modal.Title>
		          </Modal.Header>
		          <Modal.Body>
		          	<Form.Row>
						<Col>
							<p>Thanks for your interest in Odoo!  Appmoshere is here to help get you started, or to build on what you already have.  We have just a few questions to help us understand your business situation.</p>
						</Col>
					</Form.Row>
		          	<Form name="appointment" onSubmit={e => this.handleAppointmentSubmit(e)}>
						{this.getProgressBar()}
						{this.renderMessagesAppointment()}
						{this.renderQuestionarieSection()}
						<Row className="display-block">
							{this.renderPrevButton()}
							{this.renderNextButton()}
						</Row>
					</Form>
		          </Modal.Body>
		        </Modal>
	 			<Form onSubmit={e => this.handleSubmit(e)}>
	 				{this.renderMessages()}
					<Row>
						<Col>
							<Form.Row>
							    <Form.Group as={Col} controlId="formGridEmail">
							      <Form.Label>Name</Form.Label>
							      <Form.Control value={this.state.user_data.name} onChange={(e) => this.updateValue('name', e.target.value)} type="text" name="name" required placeholder="John Doe" />
							    </Form.Group>
							</Form.Row>

							<Form.Group controlId="formGridAddress1">
							    <Form.Label>Email</Form.Label>
							    <Form.Control required value={this.state.user_data.email} onChange={(e) => this.updateValue('email', e.target.value)} name="email" placeholder="example@gmail.com" type="email" />
							</Form.Group>

							<Form.Group controlId="formGridAddress2">
							    <Form.Label>Message</Form.Label>
							    <Form.Control required value={this.state.user_data.message} onChange={(e) => this.updateValue('message', e.target.value)} as="textarea" rows="3" />
							</Form.Group>

							{this.renderSubmitButtomLoader()}
						</Col>
					</Row>
				</Form>
				<Row>
					<Col md={12} className="text-align-center">
						<h3>or Schedule an Appointment?</h3>
					</Col>
				</Row>
				<Row>
					<Col md={12} className="text-align-center">
						<Calendar onChange={(e) => this.onChangeDate(e)} value={this.state.date}/>
					</Col>
					<Col md={12} className="text-align-center">
						<i>* Click on a date to book an appointment</i>
					</Col>
				</Row>
			</div>
 		);
 		
 	}

 	submitAppointment() {
 		document.appointment.submit();
 	}

	renderSubmitButtonLoaderAppointment() {
	  	if(this.state.appointment_loading) {
	  		return (
	  			<Button className="close-right" disabled variant="primary" type="submit">
		    		<Spinner
			      as="span"
			      animation="border"
			      size="sm"
			      role="status"
			      aria-hidden="true"
			    /> Book Now
				</Button>
	  		);
	  	} else {
	  		return (
	  			<Button className="close-right" variant="primary" type="submit">
		    		Book Now
				</Button>
	  		);
	  	}
	} 	

 	renderSubmitButtomLoader() {
	  	if(this.state.loading) {
	  		return (
	  			<Button disabled variant="primary" type="submit">
		    		<Spinner
			      as="span"
			      animation="border"
			      size="sm"
			      role="status"
			      aria-hidden="true"
			    /> Send
				</Button>
	  		);
	  	} else {
	  		return (
	  			<Button variant="primary" type="submit">
		    		Send
				</Button>
	  		);
	  	}
	  }
}

export default Contact;

