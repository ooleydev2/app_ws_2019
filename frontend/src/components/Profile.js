import React from 'react';
import './Profile.css';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/FormControl';
import Carousel from 'react-bootstrap/Carousel';
import Jumbotron from 'react-bootstrap/Jumbotron';
import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Alert from 'react-bootstrap/Alert';
import Accordion from 'react-bootstrap/Accordion';
import InputGroup from 'react-bootstrap/InputGroup';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Tooltip from 'react-bootstrap/Tooltip';
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';
import Spinner from 'react-bootstrap/Spinner';
import {CardNumberElement, injectStripe, StripeProvider, Elements, CardExpiryElement, CardCVCElement} from 'react-stripe-elements';
import config from '../config';

class _CardForm extends React.Component {
  state = {
  	countries: [],
  	states: [],
  	cart: localStorage.getItem('selected_plans') ? JSON.parse(localStorage.getItem('selected_plans')) : '',
  	users: localStorage.getItem('users'),
  	loading: false,
  	total: 0,
  	users: 1,
  	user_data: {
  		email: '',
  		address: '',
  		address_1: '',
  		city: '',
		zipcode: '',
		company_name: '',
		tin: '',
		country: '',
		state: ''
  	},
  	updating: false,
  	success: false
  }

    getCookie(cname) {
	  var name = cname + "=";
	  var decodedCookie = decodeURIComponent(document.cookie);
	  var ca = decodedCookie.split(';');
	  for(var i = 0; i <ca.length; i++) {
	    var c = ca[i];
	    while (c.charAt(0) == ' ') {
	      c = c.substring(1);
	    }
	    if (c.indexOf(name) == 0) {
	      return c.substring(name.length, c.length);
	    }
	  }
	  return "";
	}

  componentDidMount() {
  	const that = this;
  	fetch(config.api_url+'api/location')
  	.then(response => response.json())
  	.then(response => {
  		console.log(response);
  		this.setState({
  			countries: response.data
  		});
  	})
  	.catch(error => {

  	});

  	if(this.getCookie('session_app'))
	  	fetch(config.api_url+'api/users/details', {
	  		method: 'GET',
	  		headers: {
	  			'Content-Type': 'application/json',
	  			'Authorization': 'Bearer '+this.getCookie('session_app')
	  		}
	  	})
	  	.then( (response) => {
	  		if(response.status == 200) {
	  			response.json()
	  			.then( (success) => {
	  				this.setState({ 'user_data': success.data });
	  				if(success.data.country) {
	  					fetch(config.api_url+'api/location/'+success.data.country)
					  	.then(response => response.json())
					  	.then(response => {
					  		that.setState({
					  			states: response.data
					  		});
					  	})
					  	.catch(error => {
					  		console.log("Error");
					  		console.log(error);
					  	});
	  				}
	  			});
	  		}
	  	})
	  	.catch( (error) => {

	  	});
  }

  handleCountryChange(event) {
  	if(event.target.value) {
  		fetch(config.api_url+'api/location/'+event.target.value)
	  	.then(response => response.json())
	  	.then(response => {
	  		this.setState({
	  			states: response.data
	  		});
	  	})
	  	.catch(error => {
	  		console.log("Error");
	  		console.log(error);
	  	});
  	} else {
  		this.setState({
  			states: []
  		});
  	}
  }

  updateValue(name, value) {
  	let user_data = this.state.user_data;
  	user_data[name] = value;
  	this.setState({
  		user_data: user_data
  	});
  }

  renderMessages() {
		if(this.state.success) {
			return (
				<Alert variant={'success'}>
				    Successfully updated your profile!
				</Alert>
			);
		}

		if(this.state.failed) {
			return (
				<Alert variant={'danger'}>
				   {this.state.failed}
				</Alert>
			);
		}
	}

  renderStatesOption() {
	if(this.state.states.length) {
		return (
			<Form.Group as={Col} controlId="formGridState">
			  <Form.Label>State</Form.Label>
			  <Form.Control value={this.state.user_data.state} onChange={(e) => this.updateValue('state', e.target.value)} name="state" as="select">
			    <option>Choose...</option>
			    {
			    	this.state.states.map( state => {
			    		return (
			    			<option value={state.id} key={state.id}>{state.name}</option>
			    		);
			    	})
			    }
			  </Form.Control>
			</Form.Group>
		);
	} else {
		return (
			<Form.Group as={Col}  name="state" controlId="formGridState">
			  <Form.Label>State</Form.Label>
			  <Form.Control as="select">
			    <option>Choose...</option>
			  </Form.Control>
			</Form.Group>
		);
	}
  }

  renderLoader() {
  	if(this.state.loading) {
  		return (
  			<Spinner
		      as="span"
		      animation="border"
		      size="sm"
		      role="status"
		      aria-hidden="true"
		    />
  		);
  	}
  }

  handleSubmit(event) {
  	const that = this;
  	this.setState({ updating: true })
  	fetch(config.api_url+'api/users/update_details', {
  		method: 'POST',
  		headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '+this.getCookie('session_app')
        },
		body: JSON.stringify(this.state.user_data)
  	})
  	.then( (response) => {
  		response.json()
  		.then( (success) => {
  			if(response.status == 200) {
				that.setState({ success: true, updating: false });
  			} else {
				that.setState({ error: success.description ? success.description : 'Something went wrong while processing your order. Please try again later!' });
  			}
  		})
  		.catch( (error) => {
  			that.setState({ updating: false, error: 'Something went wrong while processing your order. Please try again later!' });
  		});
  	})
  	.catch( (error) => {
  		that.setState({ updating: false, error: 'Something went wrong while processing your order. Please try again later!' });
  	});
  	event.preventDefault();
  	event.stopPropagation();
  }

  render() {
    return (
    	<div className="container container-profile">
	    <Form onSubmit={e => this.handleSubmit(e)}>
	    	{this.renderMessages()}
			<Row>
				<Col>
					<Form.Row>
					    <Form.Group as={Col} controlId="formGridEmail">
					      <Form.Label>Email</Form.Label>
					      <Form.Control disabled value={this.state.user_data.email} onChange={(e) => this.updateValue('email', e.target.value)} type="email" name="email" required placeholder="example@gmail.com" />
					    </Form.Group>
					</Form.Row>

					<Form.Group controlId="formGridAddress1">
					    <Form.Label>Address</Form.Label>
					    <Form.Control required value={this.state.user_data.address} onChange={(e) => this.updateValue('address', e.target.value)} name="address" placeholder="1234 Main St" />
					</Form.Group>

					<Form.Group controlId="formGridAddress2">
					    <Form.Label>Address 2 (optional)</Form.Label>
					    <Form.Control required value={this.state.user_data.address_1} onChange={(e) => this.updateValue('address_1', e.target.value)} name="address_1" placeholder="Apartment, studio, or floor" />
					</Form.Group>

					<Form.Row>
					    <Form.Group as={Col} controlId="formGridCity">
					      <Form.Label>City</Form.Label>
					      <Form.Control value={this.state.user_data.city} onChange={(e) => this.updateValue('address', e.target.city)} name="city" required/>
					    </Form.Group>

					    <Form.Group as={Col} controlId="formGridZip">
					      <Form.Label>Zip</Form.Label>
					      <Form.Control value={this.state.user_data.zipcode} onChange={(e) => this.updateValue('zipcode', e.target.value)} name="zipcode" required/>
					    </Form.Group>
					</Form.Row>

					<Form.Row>
					    <Form.Group as={Col} controlId="formGridCity">
					      <Form.Label>Company Name</Form.Label>
					      <Form.Control value={this.state.user_data.company_name} onChange={(e) => this.updateValue('company_name', e.target.value)} name="company_name" required/>
					    </Form.Group>

					    <Form.Group as={Col} controlId="formGridZip">
					      <Form.Label>TIN/VAN</Form.Label>
					      <Form.Control value={this.state.user_data.tin} onChange={(e) => this.updateValue('tin', e.target.value)} name="tin" required/>
					    </Form.Group>
					</Form.Row>

					<Form.Row>
					  	<Form.Group as={Col} controlId="formGridState">
						  <Form.Label>Country</Form.Label>
						  <Form.Control value={this.state.user_data.country} onChange={(e) => this.updateValue('country', e.target.value)} required as="select" name="country" onChange={this.handleCountryChange.bind(this)}>
						    <option>Choose...</option>
						    {
						    	this.state.countries.map( country => {
						    		return (
						    			<option value={country.id} key={country.id}>{country.name}</option>
						    		);
						    	})
						    }
						  </Form.Control>
						</Form.Group>
						{this.renderStatesOption()}
					  </Form.Row>

					  {this.renderSubmitButtomLoader()}
				</Col>
			</Row>
		</Form>
		</div>
    )
  }

  renderSubmitButtomLoader() {
  	if(this.state.updating) {
  		return (
  			<Button disabled variant="primary" type="submit">
	    		<Spinner
		      as="span"
		      animation="border"
		      size="sm"
		      role="status"
		      aria-hidden="true"
		    /> Update
			</Button>
  		);
  	} else {
  		return (
  			<Button variant="primary" type="submit">
	    		Update
			</Button>
  		);
  	}
  }
  
}

class Checkout extends React.Component {
	state = {
		'selected_prices': [],
		'plans': [1,2,3,4,5,6,7,8,9],
		'users': 1
	}

	onItemClick(item, event,) {
		let selected = this.state.selected_prices;
		if(selected.indexOf(item) == -1) {
			selected.push(item);
		} else {
			let indexItem = selected.indexOf(item);
			selected.splice(indexItem, 1);
		}
		this.setState({
			'selected_prices': selected
		});
		console.log(this.state);
	}

	handleChange(event) {
		this.setState({
			'users': event.target.value
		});
	}

	render() {
		return (
			<div>
		    	<_CardForm history={this.props.history}/>
		    </div>
		);
	}
}

export default Checkout;