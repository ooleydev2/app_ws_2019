import React from 'react';
import { Route, withRouter } from 'react-router-dom';
import $ from 'jquery';
const userUrls = ['/subscriptions', '/profile'];
const adminUrls = ['/admin/users', '/admin/subscriptions', '/admin/appointments'];

class ScrollToTop extends React.Component {
  getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }

  componentDidMount() {
    if((adminUrls.indexOf(this.props.location.pathname) != -1 || userUrls.indexOf(this.props.location.pathname) != -1) && !this.getCookie('session_app')) {
      this.props.history.push('/');     
    } 

    if(this.getCookie('user_type') == 'user' && adminUrls.indexOf(this.props.location.pathname) != -1) {
      this.props.history.push('/subscriptions');
    }
  }

  componentDidUpdate(prevProps) {

    if (this.props.location !== prevProps.location) {
      window.scrollTo(0, 0);
      $('.footer').css('display', 'block');
      console.log(this.props.location);
    }
  }

  render() {
    return this.props.children
  }
}

export default withRouter(ScrollToTop)