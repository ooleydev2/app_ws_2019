import React from 'react';
import Jumbotron from 'react-bootstrap/Jumbotron';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/FormControl';

class SolutionsInterested extends React.Component {
	render() {
		return (
			<Row>
				<Col>
					<Form.Row>
						<Col>
							<strong>Now, please indicate which Odoo solutions you are interested in for your business.</strong>
						</Col>
					</Form.Row>

					<Form.Row>
							{this.props.consult_options.map( (option, index) => {
								return (
								<Col key={index}  md={4} xs={12}>
									<input onChange={(e) => this.props.valueChanged('consult', e.target.value)} name="interested_checkbox" value={''+index} type="checkbox" id={'consult'+index}/>
									<label for={'consult'+index}>{option}</label>
								</Col>);
							})}
					</Form.Row>								
				</Col>
			</Row>
		);
	}
}

export default SolutionsInterested;