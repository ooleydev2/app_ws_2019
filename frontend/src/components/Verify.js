import React from 'react';
import './Verify.css';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/FormControl';
import Carousel from 'react-bootstrap/Carousel';
import Jumbotron from 'react-bootstrap/Jumbotron';
import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Spinner from 'react-bootstrap/Spinner';
import Alert from 'react-bootstrap/Alert';
import Accordion from 'react-bootstrap/Accordion';
import Badge from 'react-bootstrap/Badge';
import Modal from 'react-bootstrap/Modal';
import config from '../config';
import { NavLink } from 'react-router-dom';
import validators from './common/signup_validators';

class Verify extends React.Component {
	state = {
		loading: false,
		success: false,
		failed: false
	};
	validators = validators;

	componentDidMount () {
	    const token = this.props.match.params.token;

	    fetch(config.api_url + 'api/users/verify', {
	    	method: 'POST',
	    	headers: {
	            'Content-Type': 'application/json'
	        },
			body: JSON.stringify({ token: token })
	    })
	    .then((response) => {
	        response.json()
	        .then( (success) => {
	        	if(response.status == 200) {
					this.setState({
						loading: false,
						success: true,
						failed: false
					});
	        	} else {
					this.setState({
						loading: false,
						success: false,
						failed: success.description ? success.description : 'Something went wrong while verifying your account. Please try again later!'
					});
	        	}
	        })
	        .catch( (error) => {
	        	this.setState({
        			loading: false,
					success: false,
					failed: 'Something went wrong while verifying your account. Please try again later!'
        		});
	        });
	    })
	}
	
	renderLoading() {
		if(this.state.loading) {
			return (
					<Row className="text-align-center">
						<Col md={12} xs={12}>
							<Spinner
						      as="span"
						      animation="border"
						      size="md"
						      role="status"
						      aria-hidden="true"
						    /> Verifying
						</Col>
					</Row>
			);
		}
	}

	renderFailed() {
		if(this.state.failed) {
			return (<Alert variant={'danger'}>
			   {this.state.failed}
			</Alert>);
		}
	}

	renderSuccess() {
		if(this.state.success) {
			return (<Alert variant={'success'}>
			   Successfully verified your account please <NavLink exact to="/login">login here</NavLink> to continue!
			</Alert>);
		}
	}

	render() {
		return (
			<div className="container container-verify">
				{this.renderLoading()}
				{this.renderFailed()}
				{this.renderSuccess()}
			</div>
		);
	}
}

export default Verify;