import React from 'react';
import Jumbotron from 'react-bootstrap/Jumbotron';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/FormControl';

class Experience extends React.Component {
	render() {
		return (
			<Row>
				<Col>
					<Form.Row>
						<Col>
							<strong>Please check the box that best describes your experience levels with Odoo ERM solutions.</strong>
						</Col>
					</Form.Row>

					<Form.Row>
					    <Form.Group as={Col} controlId="formGridEmail">
					        <Row>
					        	<Col>
					        		<Form.Control checked={this.props.data.experience == '0'} value="0" name="experience" onChange={(e) => this.props.valueChanged('experience', e.target.value)} className="radio-options" type="radio" id="first_exp"/><label for="first_exp" className="radio-label">I am a current (or former) subscriber to one or more Odoo software applications</label>
					        	</Col>
					        </Row>
							<Row>
								<Col>
									<Form.Control checked={this.props.data.experience == '1'} value="1" name="experience" onChange={(e) => this.props.valueChanged('experience', e.target.value)} className="radio-options" type="radio" id="second_exp"/><label for="second_exp" className="radio-label">I am not a current or former subscriber, but I am familiar with the software application options</label>
								</Col>
							</Row>
							<Row>
								<Col>
									<Form.Control checked={this.props.data.experience == '2'}  value="2" name="experience" onChange={(e) => this.props.valueChanged('experience', e.target.value)} className="radio-options" type="radio" id="third_exp"/><label for="third_exp" className="radio-label">I am not a current or former subscriber, and I'm not very familiar with software application options</label>
								</Col>
							</Row>
					    </Form.Group>
					</Form.Row>
					{this.props.displayValidationErrors('experience')}
				</Col>
			</Row>
		);
	}
}

export default Experience;