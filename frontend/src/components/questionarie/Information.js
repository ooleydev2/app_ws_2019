import React from 'react';
import Jumbotron from 'react-bootstrap/Jumbotron';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/FormControl';

class Information extends React.Component {
	render() {
		return (
			<Row>
				<Col>
					<Form.Row>
						<Col>
							<strong>First, tell us a little bit about your business!</strong>
						</Col>
					</Form.Row>

					<Form.Row>
					    <Form.Group as={Col} controlId="formGridEmail">
					      <Form.Label>Name of the primary contact</Form.Label>
					      <Form.Control value={this.props.appointment_data.primary_name} onChange={(e) => this.props.valueChanged('primary_name', e.target.value)} type="text" name="primary_name" required placeholder="John Doe" />
					      {this.props.displayValidationErrors('primary_name')}
					    </Form.Group>

					    <Form.Group as={Col} controlId="formGridAddress1">
						    <Form.Label>Primary contact email</Form.Label>
						    <Form.Control required value={this.props.appointment_data.primary_email} onChange={(e) => this.props.valueChanged('primary_email', e.target.value)} name="primary_email" placeholder="example@gmail.com" type="email" />
							{this.props.displayValidationErrors('primary_email')}
						</Form.Group>
					</Form.Row>

					<Form.Row>
					    <Form.Group as={Col} controlId="formGridAddress1">
						    <Form.Label>Primary contact phone number</Form.Label>
						    <Form.Control required value={this.props.appointment_data.primary_phone} onChange={(e) => this.props.valueChanged('primary_phone', e.target.value)} name="primary_phone" placeholder="+1xxxxxxxxxx" type="name" />
							{this.props.displayValidationErrors('primary_phone')}
						</Form.Group>

					    <Form.Group as={Col} controlId="formGridAddress1">
						    <Form.Label>Name of Business</Form.Label>
						    <Form.Control required value={this.props.appointment_data.business_name} onChange={(e) => this.props.valueChanged('business_name', e.target.value)} name="business_name" placeholder="Enter business name" type="name" />
							{this.props.displayValidationErrors('business_name')}
						</Form.Group>
					</Form.Row>
					
					<Form.Group controlId="formGridAddress1">
					    <Form.Label>Address</Form.Label>
					    <Form.Control required value={this.props.appointment_data.business_address} onChange={(e) => this.props.valueChanged('business_address', e.target.value)} name="business_address" placeholder="Enter business Adress" type="name" />
						{this.props.displayValidationErrors('business_address')}
					</Form.Group>

					<Form.Group controlId="formGridAddress1">
					    <Form.Label>Website</Form.Label>
					    <Form.Control required value={this.props.appointment_data.business_website} onChange={(e) => this.props.valueChanged('business_website', e.target.value)} name="business_website" placeholder="Enter business website" type="name" />
						{this.props.displayValidationErrors('business_website')}
					</Form.Group>
				</Col>
			</Row>
		);
	}
}

export default Information;