import React from 'react';
import Jumbotron from 'react-bootstrap/Jumbotron';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/FormControl';

class Consult extends React.Component {
	render() {
		return (
			<Row>
				<Col>
					<Form.Row>
						<Col>
							<strong>Odoo offers a way of integrating all of your business systems for data, resource, and financial efficiency.  Please select all the systems that may be applicable to your business.  (This doesn't commit you to an Odoo solution, but will help us personalize recommendations for you during a consultation)</strong>
						</Col>
					</Form.Row>

					<Form.Row>
							{this.props.consult_options.map( (option, index) => {
								return (
								<Col key={index}  md={4} xs={12}>
									<input checked={this.props.data.indexOf(String(index)) != -1} onChange={(e) => this.props.valueChanged('consult_checkbox', e.target.value)} name="consult_checkbox" value={''+index} type="checkbox" id={'consult_checkbox'+index}/>
									<label for={'consult_checkbox'+index}>{option}</label>
								</Col>);
							})}
					</Form.Row>								
					{this.props.displayValidationErrors('consult_checkbox')}
				</Col>
			</Row>
		);
	}
}

export default Consult;