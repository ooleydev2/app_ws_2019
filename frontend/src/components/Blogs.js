import CKEditor from 'ckeditor4-react';
import React from 'react';
import { Form, Row, Col, Breadcrumb, Button, Alert, Spinner, Pagination, Card } from 'react-bootstrap';
import config from '../config';
import './Blogs.css';
import BlogCard from './common/BlogCard';

class Blogs extends React.Component {
	state = {
		form: {
			title: null,
			body: "<p>Hello from CKEditor 4!</p>",
			file: null
		},
		saving_blog: false,
		saved_blog: false,
		error_blog: false,

		loading_blogs: true,
		blogs: [],
		loading_error: false,
		total_records: 0,
		limit: 12,
		current_page: 1
	}

	componentDidMount() {
		this.setState({ loading_blogs: true });
		fetch(config.api_url+'api/blogs?start=0&length=12&responseType=json')
		.then( (response) => {
			response.json()
			.then( (result) => {
				if(response.status == 200) {
					this.setState({ loading_blogs: false, blogs: result.data, total_records: result.total });
				} else {
					this.setState({ loading_blogs: false, loading_error: result.description ? result.description : 'Error while fetching blogs. Please try again later!' });
				}
			});
		})
		.catch( (error) => {
			this.setState({ loading_blogs: false, loading_error: error.message ? error.message : 'Error while fetching blogs. Please try again later!' });
		});
	}

	loadRecords(value, event) {
		this.setState({ loading_blogs: true });
		let nextOffset = this.state.limit * value;
		fetch(config.api_url+'api/blogs?start='+nextOffset+'&length=12&responseType=json')
		.then( (response) => {
			response.json()
			.then( (result) => {
				if(response.status == 200) {
					this.setState({ current_page: (value+1), loading_blogs: false, blogs: result.data, total_records: result.total });
				} else {
					this.setState({ loading_blogs: false, loading_error: result.description ? result.description : 'Error while fetching blogs. Please try again later!' });
				}
			});
		})
		.catch( (error) => {
			this.setState({ loading_blogs: false, loading_error: error.message ? error.message : 'Error while fetching blogs. Please try again later!' });
		});
	}

	renderPagination() {
		if(this.state.total_records > 0) {
			let totalPages = Math.ceil(this.state.total_records / this.state.limit);
			console.log("Total", this.state.total_records);
			console.log("Total", this.state.limit);
			let pages = [];
			for(let i=0; i<totalPages; i++) {
				pages.push(<Pagination.Item onClick={this.loadRecords.bind(this, i)} key={i} active={(i+1) == this.state.current_page}>{i+1}</Pagination.Item>);
			}

			return (
				<Pagination>
				  <Pagination.First title="Go to first page" onClick={ (e) => this.loadRecords(0) }/>
				  {pages}
				  <Pagination.Last title="Go to last page" onClick={ (e) => this.loadRecords(totalPages - 1) }/>
				</Pagination>
			);
		}
	}

	showLoader() {
		if(this.state.loading_blogs) {
			return (
				<Spinner animation="border" role="status">
				  <span className="sr-only">Loading...</span>
				</Spinner>
			);
		}
	}

	renderError() {
		if(this.state.loading_error) {
			return (<Alert variant="danger">{this.state.loading_error}</Alert>);
		}
	}

	render() {
		return (
			<div className="container container-add-blog">
				<Breadcrumb>
				  <Breadcrumb.Item>Customer Stories</Breadcrumb.Item>
				</Breadcrumb>
				{this.renderPagination()}
				{this.renderError()}
				<Row className="blog-cards">
					{
						this.state.blogs.map( (blog) => {
							return (
								<Col key={blog.id} md={4}>
									<BlogCard blog={blog}/>
									<div className="clear-bottom-30"></div>
								</Col>
							);
						})
					}
				</Row>
				{this.showLoader()}
	        </div>
		);
	}
}

export default Blogs;