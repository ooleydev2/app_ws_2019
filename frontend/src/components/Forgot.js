import React from 'react';
import './Login.css';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/FormControl';
import Carousel from 'react-bootstrap/Carousel';
import Jumbotron from 'react-bootstrap/Jumbotron';
import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Accordion from 'react-bootstrap/Accordion';
import Spinner from 'react-bootstrap/Spinner';
import Badge from 'react-bootstrap/Badge';
import Modal from 'react-bootstrap/Modal';
import config from '../config';
import { NavLink } from 'react-router-dom';
import validators from './common/forgot_validators';
import Alert from 'react-bootstrap/Alert';

class Forgot extends React.Component {
	state = {
		show: false,
		submitted: false,
		email: '',
		email_subscribed: false,
		user_info: {
			email: ''
		},
		processing: false,
		success: false,
		failed: false
	};
	validators = validators;

	renderMessages() {
		if(this.state.success) {
			return (
				<Alert variant={'success'}>
				    Successfully sent email. Please check your email for more instructions!
				</Alert>
			);
		}

		if(this.state.failed) {
			return (
				<Alert variant={'danger'}>
				   {this.state.failed}
				</Alert>
			);
		}
	}

	handleSubmit(e) {
		if(this.isFormValid()) {
			this.setState({ processing: true });
			const formData = {
				'email': this.state.user_info.email
			};
			fetch(config.api_url+'api/users/forgot', {
				method: 'POST',
  				headers: {
		            'Content-Type': 'application/json'
		        },
  				body: JSON.stringify(formData)
			})
			.then( (response) => {
				response.json()
				.then( (success) => {
					if(response.status == 200) {
						this.setState({
							success: true,
							processing: false,
							failed: false
						});
					} else {
						this.setState({
							success: true,
							processing: false,
							failed: success.description ? success.description : 'Something went wrong while processing your request. Please try again later!'
						});
					}
				})
				.catch( (error) => {
					this.setState({
						success: true,
						processing: false,
						failed: 'Something went wrong while processing your request. Please try again later!'
					});
				});
			})
			.catch( (error) => {
				this.setState({
					success: true,
					processing: false,
					failed: 'Something went wrong while processing your request. Please try again later!'
				});
			});
		} else {
			console.log("Invalid form");
		}
		e.preventDefault();
		e.stopPropagation();
	}

	handleChange(name, event) {
		let fieldName = name;
		let fieldValue = event.target.value;
		this.updateValidators(fieldName, fieldValue);
		let values = this.state.user_info;
		values[name] = event.target.value;
		this.setState({
			user_info: values
		});
	}

	renderSubmitButton() {
		if(this.state.processing) {
			return (
				<Button disabled variant="primary" className="margin-center" type="submit">
			    	<Spinner
				      as="span"
				      animation="border"
				      size="sm"
				      role="status"
				      aria-hidden="true"
				    />	Reset Password
				</Button>
			);
		} else {
			return (
				<Button variant="primary" className="margin-center" type="submit">
			    	Reset Password
				</Button>
			);
		}
	}

	isFormValid() {
	    let status = true;
	    Object.keys(this.validators).forEach((field) => {
	      this.updateValidators(field, this.state.user_info[field]);
	      if (!this.validators[field].valid) {
	        status = false;
	      }
	    });
	    this.setState({ form_submitted: true });
	    return status;
	}

	updateValidators(fieldName, value) {
	    this.validators[fieldName].errors = [];
	    this.validators[fieldName].state = value;
	    this.validators[fieldName].valid = true;
	    this.validators[fieldName].rules.forEach((rule) => {
	    	if(!this.validators[fieldName].errors.length) {
	    		if (rule.test instanceof RegExp) {
			        if (!rule.test.test(value)) {
			          this.validators[fieldName].errors.push(rule.message);
			          this.validators[fieldName].valid = false;
			        }
			    } else if (typeof rule.test === 'function') {
			        if (!rule.test(value)) {
			          this.validators[fieldName].errors.push(rule.message);
			          this.validators[fieldName].valid = false;
			        }
			    }
	    	}	
	    });
	}

	displayValidationErrors(fieldName) {
	    const validator = this.validators[fieldName];
	    const result = '';
	    if (validator && !validator.valid) {
	      const errors = validator.errors.map((info, index) => {
	        return <span className="error" key={index}>* {info}</span>;
	      });
	      return (
	        <Row>
	        	<Col md={12} sm={12} className="text text-danger">
	        		{errors}
	        	</Col>
	        </Row>
	      );
	    }
	    return result;
	}
	
	render() {
		return (
			<div>
				<Form noValidate onSubmit={e => this.handleSubmit(e)}>
					<Row className="top-class">
						<Col md={4} className="form-wrapper">
							<Row>
								<Col>
									{this.renderMessages()}
								</Col>
							</Row>
							<Form.Row>
							    <Form.Group as={Col} controlId="formGridEmail">
							      <Form.Label>Email</Form.Label>
							      <Form.Control type="email" name="email" onChange={this.handleChange.bind(this, 'email')} required placeholder="example@gmail.com" />
							      {this.displayValidationErrors('email')}
							    </Form.Group>
							</Form.Row>

							<Row className="checkout-button">
								<Col md={12} xs={12}>
									{this.renderSubmitButton()}	
								</Col>
								<Col md={12} xs={12}>
									<NavLink exact to="/login" className="pull-left">Login here</NavLink>
									<NavLink exact to="/signup" className="pull-right">Signup here</NavLink>
								</Col>
							</Row>
						
						</Col>
					</Row>
				</Form>
			</div>
		);
	}
}

export default Forgot;