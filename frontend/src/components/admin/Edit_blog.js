import CKEditor from 'ckeditor4-react';
import React from 'react';
import { Form, Row, Col, Breadcrumb, Button, Alert, Spinner } from 'react-bootstrap';
import config from '../../config';
import { NavLink } from 'react-router-dom';

class EditBlog extends React.Component {
	state = {
		data: null,
		saving_blog: false,
		saved_blog: false,
		error_blog: false,
		thumbnail_changed: false
	}

	componentDidMount() {
		let currentId = this.props.match.params.id;
		fetch(config.api_url+'api/blogs/'+currentId)
		.then( (response) => {
			response.json()
			.then( (success) => {
				this.setState({ data: success.data });
			})
		})
		.catch( (error) => {
			this.setState({ error_blog: error.message });
		});
	}

	renderMessages() {
		if(this.state.saved_blog) {
			return (<Alert variant={'success'}>Successfully updated blog post!</Alert>);
		}

		if(this.state.error_blog) {
			return (<Alert variant={'danger'}>{this.state.error_blog}</Alert>);
		}
	}

	valueChange(value, field) {
		let formValues = this.state.data;
		formValues[field] = value;
		if(field == 'thumbnail') {
			this.setState({ thumbnail_changed: true });
		}
		this.setState({
			data: formValues
		});
	}

	getCookie(cname) {
	  var name = cname + "=";
	  var decodedCookie = decodeURIComponent(document.cookie);
	  var ca = decodedCookie.split(';');
	  for(var i = 0; i <ca.length; i++) {
	    var c = ca[i];
	    while (c.charAt(0) == ' ') {
	      c = c.substring(1);
	    }
	    if (c.indexOf(name) == 0) {
	      return c.substring(name.length, c.length);
	    }
	  }
	  return "";
	}

	handleSubmit(e) {
			try {
				let currentId = this.props.match.params.id;
				let formData = new FormData();
				formData.append('title', this.state.data.title);
				formData.append('content', this.state.data.content);
				formData.append('short_text', this.state.data.short_text);
				if(this.state.thumbnail_changed) {
					formData.append('file', this.state.data.thumbnail, this.state.data.thumbnail);
				}
				
				this.setState({ saving_blog: true });
				fetch(config.api_url + 'api/blogs/edit_blog/'+currentId, {
					method: 'POST',
					body: formData,
					headers: {
			  			'Authorization': 'Bearer '+this.getCookie('session_app')
			  		}
				})
				.then( (response) => {
					response.json()
					.then( (success) => {
						if(response.status == 200) {
							this.setState({ saving_blog: false, saved_blog: true, error_blog: false });
							document.getElementById('success-alert').scrollIntoView();
						} else {
							this.setState({ saving_blog: false, saved_blog: false, error_blog: success.description ? success.description : 'Error while saving blog. Please try again later!' });
						}
					})
					.catch( (error) => {
						this.setState({ saving_blog: false, saved_blog: false, error_blog: error.message });
					});
				})
				.catch( (error) => {

				});
			} catch(error) {
				console.log(error);
			}
		e.preventDefault();
		e.stopPropagation();
	}

	renderSubmit() {
		if(this.state.saving_blog) {
			return (<Button disabled type="primary"><Spinner
		      as="span"
		      animation="border"
		      size="sm"
		      role="status"
		      aria-hidden="true"
		    /> Update</Button>);
		} else {
			return (<Button type="primary">Update</Button>);
		}
	}

	appendImage(file) {
		console.log(file);
		//document.getElementById("thumnail-image").src = (window.URL || window.webkitURL).createObjectURL(file);
		var reader = new FileReader();
    
	    reader.onload = function(e) {
	      document.getElementById("thumnail-image").src = e.target.result;
	    }
	    
	    reader.readAsDataURL(file);
	}

	render() {
		return (
			<div class="container container-add-blog">
				<Breadcrumb id="success-alert" >
				  <Breadcrumb.Item><NavLink to={"../../../admin/blogs/manage"}>Blogs</NavLink></Breadcrumb.Item>
				  <Breadcrumb.Item>Edit</Breadcrumb.Item>
				  <Breadcrumb.Item>{ this.state.data ? this.state.data.id : '-' }</Breadcrumb.Item>
				</Breadcrumb>
				<Form onSubmit={e => this.handleSubmit(e)}>
					{this.renderMessages()}
					<Form.Row>
					    <Form.Group as={Col} controlId="formGridCity">
					      <Form.Label>Blog Post Title</Form.Label>
					      <Form.Control value={this.state.data ? this.state.data.title : ''} onChange={(e) => this.valueChange(e.target.value, 'title')} placeholder="Enter blog title" name="blog_title" required/>
					    </Form.Group>
					</Form.Row>
					<Form.Row>
					    <Form.Group as={Col} controlId="formGridCity">
					      <Form.Label>Short Description</Form.Label>
					      <Form.Control value={this.state.data ? this.state.data.short_text : ''} onChange={(e) => this.valueChange(e.target.value, 'short_text')} placeholder="Enter short text" name="short_text" required/>
					    </Form.Group>
					</Form.Row>
					<Form.Row>
					    <Form.Group as={Col} controlId="formGridCity">
					      <Form.Label>Blog Post Body</Form.Label>
					      <CKEditor
					      		onChange={ (e) => this.valueChange(e.editor.getData(), 'content')}
					      		data={this.state.data ? this.state.data.content : ''}
				                config={{
				                	filebrowserUploadUrl: config.api_url+'api/images/upload'
				                }}
				            />
					    </Form.Group>
					</Form.Row>
					<Form.Row>
						<Col md={12} xs={12}>
							<img id="thumnail-image" style={{ maxHeight: '200px'}} src={config.api_url+(this.state.data ? this.state.data.thumbnail : '')} />
					    </Col>
					    <Form.Group as={Col} controlId="formGridCity">
					      <Form.Label>Thumbnail</Form.Label>
					      <Form.Control onChange={ (e) => { this.valueChange(e.target.files[0], 'thumbnail'); this.appendImage(e.target.files[0]); }} placeholder="Please select thumbnail" type="file" name="blog_title" />
					    </Form.Group>
					</Form.Row>
					<Form.Row>
						{this.renderSubmit()}
					</Form.Row>
				</Form>
	        </div>
		);
	}
}

export default EditBlog;