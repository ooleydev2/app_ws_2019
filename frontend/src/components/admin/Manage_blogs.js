import React from 'react';
import { MDBDataTable } from 'mdbreact';
import $ from 'jquery';
import './Admin_users.css';
import config from '../../config';
import Breadcrumb from 'react-bootstrap/Breadcrumb';
import Admin_user_detail from './Admin_user_detail';
import ReactDOM from 'react-dom';
import Nav from 'react-bootstrap/Nav';
import { BrowserRouter, NavLink } from 'react-router-dom';
import { Row, Col, Button } from 'react-bootstrap';

class ManageBlogs extends React.Component {
  componentDidMount() {
    const that = this;
    window.$('table').DataTable({
        "columnDefs": [ 
        {
            "targets": 5,
            "orderable": false,
            "createdCell": function (td, cellData, rowData, row, col) {
                ReactDOM.render(
                    <Nav.Link href={"/admin/blogs/edit/"+rowData[5]}>Edit</Nav.Link>,
                    td
                );
            }
        },
        {
            "targets": 2,
            "orderable": false,
            "createdCell": function (td, cellData, rowData, row, col) {
                ReactDOM.render(
                    <img style={{maxWidth: '150px'}} src={config.api_url+rowData[2]} />,
                    td
                );
            }
        }
        ],
        "processing": true,
        "serverSide": true,
        "ajax": {
            'url': config.api_url+"api/blogs",
            'type': 'GET',
            'beforeSend': function (request) {
                request.setRequestHeader("Authorization", "Bearer "+that.getCookie('session_app'));
            }
        }
    });
  }

  getCookie(cname) {
      var name = cname + "=";
      var decodedCookie = decodeURIComponent(document.cookie);
      var ca = decodedCookie.split(';');
      for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
          c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
          return c.substring(name.length, c.length);
        }
      }
      return "";
    }

  render() {
    return (
        <div className="container table-container">
            <Breadcrumb>
              <Breadcrumb.Item>Blogs</Breadcrumb.Item>
            </Breadcrumb>
            <Row>
              <Col>
                <NavLink to={"../../admin/blogs/add"}><Button className="pull-right">Add Blog Post</Button></NavLink>
              </Col>
            </Row>
            <table className={"users-table table"+(window.screen.width < 1000 ? ' table-responsive' : '')}>
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Short Text</th>
                        <th>Thumbnail</th>
                        <th>Created At</th>
                        <th>Posted By</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
      );
  }
}

export default ManageBlogs;