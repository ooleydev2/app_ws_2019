import React from 'react';
import Modal from 'react-bootstrap/Modal';
import Spinner from 'react-bootstrap/Spinner';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Alert from 'react-bootstrap/Alert';
import Button from 'react-bootstrap/Button';
import config from '../../config';
import moment from 'moment';

class Admin_booking_detail extends React.Component {
	getCookie(cname) {
	  var name = cname + "=";
	  var decodedCookie = decodeURIComponent(document.cookie);
	  var ca = decodedCookie.split(';');
	  for(var i = 0; i <ca.length; i++) {
	    var c = ca[i];
	    while (c.charAt(0) == ' ') {
	      c = c.substring(1);
	    }
	    if (c.indexOf(name) == 0) {
	      return c.substring(name.length, c.length);
	    }
	  }
	  return "";
	}
	consoleLog() {
		const that = this;
		this.setState({ show: true });
		fetch(config.api_url+'api/users/user_details/'+this.props.user, {
			headers: {
				'Authorization': 'Bearer '+this.getCookie('session_app')
			}
		})
		.then( response => {
			response.json()
			.then( (data) => {
				that.setState({ detail: data.data, loading: false });
				console.log(this.state.detail);
			});
		})
		.catch( (error) => {
			that.setState({ failed: true });
		});
	}

	renderCancelButton(order) {
		if(order.subscription_state == 'cancelled') {
			return (<Button disabled variant="danger">Cancelled</Button>);
		} else {
			return (<Button onClick={this.cancelSubscription.bind(this, order)} variant="danger">Cancel</Button>);
		}
	}

	cancelSubscription(order, event) {
		this.setState({
			order_to_cancel: order,
			show_confirmation_modal: true,
			error_cancelling: false
		});

	}

	renderSubscriptions() {
		if(this.state.detail && (!this.state.detail.orders || !this.state.detail.orders.length)) {
			return (<div>No subscriptions found!</div>);
		}

		let subscirptionsArray = [];

		if(this.state.detail && this.state.detail.orders && this.state.detail.orders.length) {
			this.state.detail.orders.forEach( (order, index) => {
				subscirptionsArray.push(
					<tr>
						<td>{index+1}</td>
						<td>{order.stripe_id}</td>
						<td>{moment(order.created_at).format('LLLL')}</td>
						<td>${order.total}</td>
						<td>{this.renderCancelButton(order)}</td>
					</tr>
				);
			});
			return (
				<table className="table table-responsive">
					<thead>
						<th>Sr. No.</th>
						<th>Stripe ID</th>
						<th>Created On</th>
						<th>Total</th>
						<th>Action</th>
					</thead>
					<tbody>
						{subscirptionsArray}
					</tbody>
				</table>
			);
		}
	}

	renderDetails() {
		if(this.state.detail) {
			return (
				<div>
					<Row>
						<Col xs={12} md={12}>
							<label class="font-weight-bold">Name:</label> {this.state.detail.name}
						</Col>
						<Col xs={12} md={12}>
							<label class="font-weight-bold">Email:</label> {this.state.detail.email}
						</Col>
						<Col xs={12} md={12}>
							<label class="font-weight-bold">Adress:</label> {this.state.detail.address}
						</Col>
						<Col xs={12} md={12}>
							<label class="font-weight-bold">City:</label> {this.state.detail.city}
						</Col>
						<Col xs={12} md={12}>
							<label class="font-weight-bold">State:</label> {this.state.detail.states.name ? this.state.detail.states.name : '-'}
						</Col>
						<Col xs={12} md={12}>
							<label class="font-weight-bold">Country:</label> {this.state.detail.states.name ? this.state.detail.countries.name : '-'}
						</Col>
						<Col xs={12} md={12}>
							<label class="font-weight-bold">Subscriptions:</label>
						</Col>
						<Col xs={12} md={12}>
							{this.renderSubscriptions()}	
						</Col>
					</Row>
				</div>
			)
		}
	}

	renderFailed() {
		if(this.state.failed) {
			return (<Alert variant="danger">Error while loading details. Plese try again later!</Alert>);
		}
	}

	state = {
		show: false,
		loading: true,
		failed: false,
		detail: false
	}

	onClose() {
		this.setState({ show: false });
	}

	renderLoading() {
		if(this.state.loading) {
			return (<Spinner
		      as="span"
		      animation="border"
		      size="sm"
		      role="status"
		      aria-hidden="true"
		    />);
		}


	}

	getAnswers(key, value, question) {
		if(key == 'solutions_interested' || key == 'consult_checkbox') {
			let answers = value.split(",");
			let answersArray = [];

			answers.forEach( (answer) => {
				answersArray.push(question.answers[parseInt(answer)]);
			});
			return answersArray.join(", ");
		} else if(question.answers) {
			return question.answers[parseInt(value)];
		} else {
			return value ? value : '-';
		}
	}

	onCloseConfirmation() {
		this.setState({
			show_confirmation_modal: false
		});
	}

	renderCancellingMessage() {
		if(this.state.error_cancelling) {
			return (
				<span>{this.state.error_cancelling}</span>
			);
		}
		else if(this.state.cancelling_subscription) {
			return (
				<span>
					<Spinner
				      as="span"
				      animation="border"
				      size="sm"
				      role="status"
				      aria-hidden="true"
				    /> Cancelling Subscription
				</span>
			);
		} else {
			return (
				<span>Are you sure you want to cancel this subscription #{this.state.current_subscription}?</span>
			);
		}
	}

	renderCancelButtonModal() {
		if(this.state.cancelling_subscription) {
			return (
				<Button disabled variant="danger">
	              Cancelling
	            </Button>
			)
		} else {
			if(this.state.subscription_cancelled) {
				return (
					<Button variant="danger" disabled>
		              Cancellled
		            </Button>
				)
			} else {
				return (
					<Button variant="danger" onClick={this.cancelSelectedSubscription.bind(this)}>
		              Confirm
		            </Button>
				)
			}
		}
	}

	cancelSelectedSubscription() {
		const that = this;
		this.setState({
			cancelling_subscription: true
		});

		fetch(config.api_url+'api/orders/cancel_subscription', {
			method: 'POST',
			headers: {
	            'Content-Type': 'application/json',
	        	'Authorization': 'Bearer '+this.getCookie('session_app')
	        },
			body: JSON.stringify({ subscription_id: this.state.order_to_cancel.stripe_id })
		})
		.then( (response) => {
			response.json()
			.then( (success) => {
				if(response.status == 200) {
					that.setState({
						cancelling_subscription: false,
						error_cancelling: false
					});
					let detail = this.state.detail;
					detail.orders.forEach((order, index) => {
						if (that.state.order_to_cancel.id == order.id) {
							detail.orders[index]['subscription_state'] = 'cancelled';
						}
					});
					this.setState({ detail: detail, show_confirmation_modal: false  });
				} else {
					that.setState({
						error_cancelling: success.description ? success.description : 'Something went wrong while cancelling subscription. Please try again later!',
						cancelling_subscription: false,
						subscription_cancelled: false
					});
				}
			})
			.catch( (error) => {
				that.setState({
					error_cancelling: 'Something went wrong while cancelling subscription. Please try again later!',
					cancelling_subscription: false,
					show: false,
					subscription_cancelled: false
				});
			});
		})
		.catch( (error) => {
			that.setState({
				error_cancelling: 'Something went wrong while cancelling subscription. Please try again later!',
				cancelling_subscription: false,
				show: false,
				subscription_cancelled: false
			});
		});
	}

	render() {
		let details = [];
		const that = this;
		
		return (
			<div>
				<div className="view-button" onClick={this.consoleLog.bind(this)}>View</div>
				 <Modal show={this.state.show} size="lg" onHide={this.onClose.bind(this)}>
		          <Modal.Header closeButton>
		            <Modal.Title>User <b>Details</b></Modal.Title>
		          </Modal.Header>
		          <Modal.Body>
		          		{this.renderLoading()}
		          		{this.renderDetails()}
		          </Modal.Body>
		        </Modal>
		        <Modal show={this.state.show_confirmation_modal}  onHide={this.onCloseConfirmation.bind(this)}>
		          <Modal.Header closeButton>
		            <Modal.Title>User <b>Details</b></Modal.Title>
		          </Modal.Header>
		          <Modal.Body>
		          		{this.renderCancellingMessage()}
		          </Modal.Body>
		          <Modal.Footer>
		            <Button variant="primary" onClick={this.onCloseConfirmation.bind(this)}>
		              Close
		            </Button>
		            {this.renderCancelButtonModal()}
		          </Modal.Footer>
		        </Modal>
			</div>
		);
	}
}

export default Admin_booking_detail;