import React from 'react';
import ReactDOM from 'react-dom';
import { MDBDataTable } from 'mdbreact';
import $ from 'jquery';
import './Admin_appointments.css';
import config from '../../config';
import Breadcrumb from 'react-bootstrap/Breadcrumb';
import Alert from 'react-bootstrap/Alert';
import Admin_booking_detail from './Admin_booking_detail';

class AdminAppointments extends React.Component {
  componentDidMount() {
  	const that = this;
  	window.$('table').DataTable({
  		"columnDefs": [
			{
                "createdCell": function (td, cellData, rowData, row, col) {
                	ReactDOM.render(
                		<Admin_booking_detail appointment={cellData}>
						</Admin_booking_detail>,
                		td
                	);
                },
                "targets": 5,
                "orderable": false
            }
		],
  		"processing": true,
        "serverSide": true,
        "ajax": {
        	'url': config.api_url+"api/newsletter/appointment_list",
		    'type': 'GET',
		    'beforeSend': function (request) {
		        request.setRequestHeader("Authorization", "Bearer "+that.getCookie('session_app'));
		    }
        }
  	});
  }

  getCookie(cname) {
	  var name = cname + "=";
	  var decodedCookie = decodeURIComponent(document.cookie);
	  var ca = decodedCookie.split(';');
	  for(var i = 0; i <ca.length; i++) {
	    var c = ca[i];
	    while (c.charAt(0) == ' ') {
	      c = c.substring(1);
	    }
	    if (c.indexOf(name) == 0) {
	      return c.substring(name.length, c.length);
	    }
	  }
	  return "";
	}

  render() {
  	return (
	  	<div className="container table-container">
	  		<Breadcrumb>
			  <Breadcrumb.Item>Appointments</Breadcrumb.Item>
			</Breadcrumb>
		    <table className={"users-table table"+(window.screen.width < 1000 ? ' table-responsive' : '')}>
		        <thead>
		            <tr>
		                <th>Primary Name</th>
		                <th>Primary Email</th>
		                <th>Created On</th>
		                <th>Address</th>
		                <th>Website</th>
		                <th>Action</th>
		            </tr>
		        </thead>
		        <tbody>
		        </tbody>
		    </table>
		</div>
	  );
  }
}

export default AdminAppointments;