import React from 'react';
import Modal from 'react-bootstrap/Modal';
import Spinner from 'react-bootstrap/Spinner';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Alert from 'react-bootstrap/Alert';
import config from '../../config';
const responses  = {
	"primary_email": {
		"question": "Primary Email"
	},
	"primary_name": {
		"question": "Primary Name"
	},
	"primary_phone": {
		"question": "Primary Phone"	
	},
	"business_website": {
		"question": "Business Website"	
	},
	"business_address": {
		"question": "Business Adress"	
	},
	"business_name": {
		"question": "Business Name"
	},
	"experience": {
		"question": "Please check the box that best describes your experience levels with Odoo ERM solutions.",
		"answers":[
			'I am a current (or former) subscriber to one or more Odoo software applications',
			'I am not a current or former subscriber, but I am familiar with the software application options',
			'I am not a current or former subscriber, and I\'m not very familiar with software application options'
		]
	},
	"consult_checkbox": {
		"question": "Odoo offers a way of integrating all of your business systems for data, resource, and financial efficiency. Please select all the systems that may be applicable to your business. (This doesn't commit you to an Odoo solution, but will help us personalize recommendations for you during a consultation)",
		"answers": [
			'Invoicing/Bookkeeping/Sales',
			'Point of Sale System', 
			'E-Commerce',
			'Invoicing',
			'Proposals/Quotes',
			'Accounting/Bookkeeping',
			'Marketing',
			'Customer Management System (CRM)',
			'Website',
			'E-Mail Marketing',
			'Events',
			'Appointments/Calendars',
			'Marketing Automation',
			'Project/Supply Chain Management',
			'Inventory Management',
			'Manufacturing Management',
			'Purchasing/Procurement',
			'Project Management',
			'Subscriptions',
			'Document Management',
			'Signatures/Routing',
			'PLM',
			'HR/Employee Management',
			'Timecards',
			'Expenses',
			'Recruitment',
			'Leaves',
			'Help Desk'
		]
	},
	'solutions_interested': {
		"question": "Now, please indicate which Odoo solutions you are interested in for your business.",
		"answers": [
			'Invoicing/Bookkeeping/Sales',
			'Point of Sale System', 
			'E-Commerce',
			'Invoicing',
			'Proposals/Quotes',
			'Accounting/Bookkeeping',
			'Marketing',
			'Customer Management System (CRM)',
			'Website',
			'E-Mail Marketing',
			'Events',
			'Appointments/Calendars',
			'Marketing Automation',
			'Project/Supply Chain Management',
			'Inventory Management',
			'Manufacturing Management',
			'Purchasing/Procurement',
			'Project Management',
			'Subscriptions',
			'Document Management',
			'Signatures/Routing',
			'PLM',
			'HR/Employee Management',
			'Timecards',
			'Expenses',
			'Recruitment',
			'Leaves',
			'Help Desk'
		] 
	},
	"free_appointment": {
		"question": "Are you interested in a free consultation appointment with a member of our team?",
		"answers": [
			'Yes, that would be helpful!',
			'No, no need to chitty-chat. Let\'s just get started!'
		]
	},
	"custom_application": {
		"question": "Are you interested in custom applications for your business? (either stand-alone apps, or custom modifications to existing Odoo apps)?",
		"answers": [
			'Yes, definitely...let\'s talk!',
			'I\'m not sure yet....let\'s discuss',
			'No, just sticking to what\'s available'
		]
	},
	'contact_person': {
		"question": 'Do you have a point-person within your organization that will be the main Point of Contact for managing the ERM/Odoo solutions?',
		'answers': [
			'Yes',
			'Not sure yet',
			'No, there are many people involved'
		]
	}
}

class Admin_booking_detail extends React.Component {
	getCookie(cname) {
	  var name = cname + "=";
	  var decodedCookie = decodeURIComponent(document.cookie);
	  var ca = decodedCookie.split(';');
	  for(var i = 0; i <ca.length; i++) {
	    var c = ca[i];
	    while (c.charAt(0) == ' ') {
	      c = c.substring(1);
	    }
	    if (c.indexOf(name) == 0) {
	      return c.substring(name.length, c.length);
	    }
	  }
	  return "";
	}
	consoleLog() {
		const that = this;
		this.setState({ show: true });
		fetch(config.api_url+'api/newsletter/appointment/'+this.props.appointment, {
			headers: {
				'Authorization': 'Bearer '+this.getCookie('session_app')
			}
		})
		.then( response => {
			response.json()
			.then( (data) => {
				that.setState({ detail: data.data, loading: false });
				console.log(this.state.detail);
			});
		})
		.catch( (error) => {
			that.setState({ failed: true });
		});
	}

	renderFailed() {
		if(this.state.failed) {
			return (<Alert variant="danger">Error while loading details. Plese try again later!</Alert>);
		}
	}

	state = {
		show: false,
		loading: true,
		failed: false,
		detail: false
	}

	onClose() {
		this.setState({ show: false });
	}

	renderLoading() {
		if(this.state.loading) {
			return (<Spinner
		      as="span"
		      animation="border"
		      size="sm"
		      role="status"
		      aria-hidden="true"
		    />);
		}


	}

	getAnswers(key, value, question) {
		if(key == 'solutions_interested' || key == 'consult_checkbox') {
			let answers = value.split(",");
			let answersArray = [];

			answers.forEach( (answer) => {
				answersArray.push(question.answers[parseInt(answer)]);
			});
			return answersArray.join(", ");
		} else if(question.answers) {
			return question.answers[parseInt(value)];
		} else {
			return value ? value : '-';
		}
	}

	render() {
		let details = [];
		const that = this;

		if(this.state.detail) {
			Object.keys(responses).forEach( (response, index) => {
				details.push(
					<Row key={index}>
						<Col xs={12} md={12}>
							<label className="label">{responses[response].question}</label>
						</Col>
						<Col xs={12} md={12}>
							<p>{that.getAnswers(response, this.state.detail[response], responses[response])}</p>
						</Col>
					</Row>
				);
			});	
		}
		
		return (
			<div>
				<div className="view-button" onClick={this.consoleLog.bind(this)}>View</div>
				 <Modal show={this.state.show} onHide={this.onClose.bind(this)}>
		          <Modal.Header closeButton>
		            <Modal.Title>Appointment <b>Details</b></Modal.Title>
		          </Modal.Header>
		          <Modal.Body>
		          		{this.renderLoading()}
		          		{details}
		          </Modal.Body>
		        </Modal>
			</div>
		);
	}
}

export default Admin_booking_detail;