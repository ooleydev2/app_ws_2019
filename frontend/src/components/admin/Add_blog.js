import CKEditor from 'ckeditor4-react';
import React from 'react';
import { Form, Row, Col, Breadcrumb, Button, Alert, Spinner } from 'react-bootstrap';
import config from '../../config';
import { NavLink } from 'react-router-dom';

class AddBlog extends React.Component {
	state = {
		form: {
			title: null,
			body: "<p>Hello from CKEditor 4!</p>",
			file: null,
			short_text: null
		},
		saving_blog: false,
		saved_blog: false,
		error_blog: false
	}

	renderMessages() {
		if(this.state.saved_blog) {
			return (<Alert variant={'success'}>Successfully added blog post!</Alert>);
		}

		if(this.state.error_blog) {
			return (<Alert variant={'danger'}>{this.state.error_blog}</Alert>);
		}
	}

	valueChange(value, field) {
		let formValues = this.state.form;
		formValues[field] = value;
		this.setState({
			form: formValues
		});
		console.log(this.state);
	}

	getCookie(cname) {
	  var name = cname + "=";
	  var decodedCookie = decodeURIComponent(document.cookie);
	  var ca = decodedCookie.split(';');
	  for(var i = 0; i <ca.length; i++) {
	    var c = ca[i];
	    while (c.charAt(0) == ' ') {
	      c = c.substring(1);
	    }
	    if (c.indexOf(name) == 0) {
	      return c.substring(name.length, c.length);
	    }
	  }
	  return "";
	}

	handleSubmit(e) {
		e.persist();
		let formData = new FormData();
		formData.append('title', this.state.form.title);
		formData.append('content', this.state.form.body);
		formData.append('short_text', this.state.form.short_text);
		formData.append('file', this.state.form.file, this.state.form.file.name);
		this.setState({ saving_blog: true });
		fetch(config.api_url + 'api/blogs/add_blog', {
			method: 'POST',
			body: formData,
			headers: {
	  			'Authorization': 'Bearer '+this.getCookie('session_app')
	  		}
		})
		.then( (response) => {
			response.json()
			.then( (success) => {
				if(response.status == 200) {
					this.setState({ saving_blog: false, saved_blog: true, error_blog: false });
					//console.log(e.target);
					e.target.reset();
					document.getElementById('success-alert').scrollIntoView();
				} else {
					this.setState({ saving_blog: false, saved_blog: false, error_blog: success.description ? success.description : 'Error while saving blog. Please try again later!' });
				}
			})
			.catch( (error) => {
				this.setState({ saving_blog: false, saved_blog: false, error_blog: error.message });
			});
		})
		.catch( (error) => {

		});
		e.preventDefault();
		e.stopPropagation();
	}

	renderSubmit() {
		if(this.state.saving_blog) {
			return (<Button disabled type="primary"><Spinner
		      as="span"
		      animation="border"
		      size="sm"
		      role="status"
		      aria-hidden="true"
		    /> Add Blog</Button>);
		} else {
			return (<Button type="primary">Add Blog</Button>);
		}
	}

	render() {
		return (
			<div class="container container-add-blog">
				<Breadcrumb  id="success-alert">
				  <Breadcrumb.Item><NavLink to={"../../../admin/blogs/manage"}>Blogs</NavLink></Breadcrumb.Item>
				  <Breadcrumb.Item>Add Blog</Breadcrumb.Item>
				</Breadcrumb>
				<Form onSubmit={e => this.handleSubmit(e)}>
					{this.renderMessages()}
					<Form.Row>
					    <Form.Group as={Col} controlId="formGridCity">
					      <Form.Label>Blog Post Title</Form.Label>
					      <Form.Control onChange={(e) => this.valueChange(e.target.value, 'title')} placeholder="Enter blog title" name="blog_title" required/>
					    </Form.Group>
					</Form.Row>
					<Form.Row>
					    <Form.Group as={Col} controlId="formGridCity">
					      <Form.Label>Short Description</Form.Label>
					      <Form.Control onChange={(e) => this.valueChange(e.target.value, 'short_text')} placeholder="Enter short text" name="short_text" required/>
					    </Form.Group>
					</Form.Row>
					<Form.Row>
					    <Form.Group as={Col} controlId="formGridCity">
					      <Form.Label>Blog Post Body</Form.Label>
					      <CKEditor
					      		onChange={ (e) => this.valueChange(e.editor.getData(), 'body')}
				                config={{
				                	filebrowserUploadUrl: config.api_url+'api/images/upload'
				                }}
				            />
					    </Form.Group>
					</Form.Row>
					<Form.Row>
					    <Form.Group as={Col} controlId="formGridCity">
					      <Form.Label>Thumbnail</Form.Label>
					      <Form.Control onChange={ (e) => this.valueChange(e.target.files[0], 'file')} placeholder="Please select thumbnail" type="file" name="blog_title" required/>
					    </Form.Group>
					</Form.Row>
					<Form.Row>
						{this.renderSubmit()}
					</Form.Row>
				</Form>
	        </div>
		);
	}
}

export default AddBlog;