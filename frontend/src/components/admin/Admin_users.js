import React from 'react';
import { MDBDataTable } from 'mdbreact';
import $ from 'jquery';
import './Admin_users.css';
import config from '../../config';
import Breadcrumb from 'react-bootstrap/Breadcrumb';
import Admin_user_detail from './Admin_user_detail';
import ReactDOM from 'react-dom';

class DatatablePage extends React.Component {
  componentDidMount() {
  	const that = this;
  	window.$('table').DataTable({
  		"columnDefs": [ {
		"targets": 6,
		"orderable": false,
		"createdCell": function (td, cellData, rowData, row, col) {
        	ReactDOM.render(
        		<Admin_user_detail user={rowData[7]}>
				</Admin_user_detail>,
        		td
        	);
        }
		}],
  		"processing": true,
        "serverSide": true,
        "ajax": {
        	'url': config.api_url+"api/users/users_list",
		    'type': 'GET',
		    'beforeSend': function (request) {
		        request.setRequestHeader("Authorization", "Bearer "+that.getCookie('session_app'));
		    }
        }
  	});
  }

  getCookie(cname) {
	  var name = cname + "=";
	  var decodedCookie = decodeURIComponent(document.cookie);
	  var ca = decodedCookie.split(';');
	  for(var i = 0; i <ca.length; i++) {
	    var c = ca[i];
	    while (c.charAt(0) == ' ') {
	      c = c.substring(1);
	    }
	    if (c.indexOf(name) == 0) {
	      return c.substring(name.length, c.length);
	    }
	  }
	  return "";
	}

  render() {
  	return (
	  	<div className="container table-container">
	  		<Breadcrumb>
			  <Breadcrumb.Item>Users</Breadcrumb.Item>
			</Breadcrumb>
		    <table className={"users-table table"+(window.screen.width < 1000 ? ' table-responsive' : '')}>
		        <thead>
		            <tr>
		                <th>Name</th>
		                <th>Email</th>
		                <th>Created On</th>
		                <th>Address</th>
		                <th>Country</th>
		                <th>State</th>
		                <th>Action</th>
		            </tr>
		        </thead>
		        <tbody>
		        </tbody>
		    </table>
		</div>
	  );
  }
}

export default DatatablePage;