import CKEditor from 'ckeditor4-react';
import React from 'react';
import { Form, Row, Col, Breadcrumb, Button, Alert, Spinner, Pagination, Card } from 'react-bootstrap';
import config from '../config';
import './Blogs.css';
import BlogCard from './common/BlogCard';
import moment from 'moment';

class Blogs extends React.Component {
	state = {
		form: {
			title: null,
			body: "<p>Hello from CKEditor 4!</p>",
			file: null
		},

		loading_detail: true,
		blog: null,
		loading_error: false
	}

	componentDidMount() {
		let currentId = this.props.match.params.id;
		fetch(config.api_url+'api/blogs/'+currentId)
		.then( (response) => {
			response.json()
			.then( (detail) => {
				if(response.status == 200) {
					this.setState({ blog: detail.data, loading_detail: false });
				} else {
					this.setState({ loading_error: detail.description ? detail.description : 'Error while loading details. Please try again later!', loading_detail: false });
				}
			});
		})
		.catch( (error) => {
			this.setState({ loading_error: error.description ? error.description : 'Error while loading details. Please try again later!', loading_detail: false });
		});
	}

	showLoader() {
		if(this.state.loading_blogs) {
			return (
				<Spinner animation="border" role="status">
				  <span className="sr-only">Loading...</span>
				</Spinner>
			);
		}
	}

	renderError() {
		if(this.state.loading_error) {
			return (<Alert variant="danger">{this.state.loading_error}</Alert>);
		}
	}

	createMarkup(text) { return {__html: this.state.blog.content}; };

	renderBlog() {
		if(this.state.blog) {
			return (
				<div>
					<Row className="blog-cards">
						<Col className="blog-image">
							<img class="banner-image-blog-detail" src={config.api_url+this.state.blog.thumbnail} />
							<div className="blog-title">
								<div className="blog-table">
									<div className="blog-column">
										{this.state.blog.title}
									</div>
								</div>
							</div>
						</Col>
					</Row>
					<Row>
						<Col md={12} xs={12}>
							<div class="blog-detail-container">
								<div class="pull-left">
									<img src={"../images/profile.jpg"} style={{height: '50px', borderRadius: '50%'}} />
								</div>
								<div style={{marginLeft: '10px'}} class="pull-right">
									<Row>
										<Col style={{fontWeight: 'bold'}}>{this.state.blog.user.name}</Col>
									</Row>
									<Row>
										<Col>{moment(this.state.blog.created_at).format('LLLL')}</Col>
									</Row>
								</div>
							</div>
						</Col>
					</Row>
					<Row className="blog-cards">
						<Col dangerouslySetInnerHTML={this.createMarkup()}>
						</Col>
					</Row>
				</div>
			);
		}
	}

	render() {
		return (
			<div className="container container-add-blog">
				
				{this.renderError()}
				{this.showLoader()}
				{this.renderBlog()}
	        </div>
		);
	}
}

export default Blogs;