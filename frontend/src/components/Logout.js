import React from 'react';
import './Login.css';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/FormControl';
import Carousel from 'react-bootstrap/Carousel';
import Jumbotron from 'react-bootstrap/Jumbotron';
import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Accordion from 'react-bootstrap/Accordion';
import Badge from 'react-bootstrap/Badge';
import Modal from 'react-bootstrap/Modal';
import config from '../config';
import { NavLink } from 'react-router-dom';
import validators from './common/login_validators';
import Alert from 'react-bootstrap/Alert';
import Spinner from 'react-bootstrap/Spinner';

class Logout extends React.Component {
	state = {
		success: false,
		processing: false,
		failed: false,
		user_info: {
			email: '',
			password: ''
		}
	};
	validators = validators;
	
	componentDidMount() {
		const that = this;
		document.cookie = "session_app=; path=/ ";
		this.props.onUpdatedSession('logged_out');
		that.props.history.push('/');
	}

	render() {
		return (
			<div>
				<Row className="text-align-center">
					<Col md={12} xs={12}>
						<Spinner
					      as="span"
					      animation="border"
					      size="md"
					      role="status"
					      aria-hidden="true"
					    /> Logout
					</Col>
				</Row>
			</div>
		);
	}
}

export default Logout;