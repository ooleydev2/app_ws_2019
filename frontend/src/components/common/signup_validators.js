const validators = {
  name: {
    rules: [
      {
        test: (value) => {
          if(!value || !value.trim()) {
            return false;
          }
          return true;
        },
        message: 'Please enter your name.'
      },
      {
        test: /^[A-Za-z0-9_ ]+$/,
        message: 'Name must contain only alphabets with numeric.'
      }
    ],
    valid: false,
    errors: [],
    state: ''
  },
  email: {
    rules: [
      {
        test: (value) => {
          if(!value || !value.trim()) {
            return false;
          }
          return true;
        },
        message: 'Please enter your email.'
      },
      {
        test: /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/,
        message: 'Please enter a valid email.'
      }
    ],
    valid: false,
    errors: [],
    state: '' 
  },
  password: {
    rules: [
      {
        test: (value) => {
          if(!value || !value.trim()) {
            return false;
          }
          return true;
        },
        message: 'Please enter your password.'
      },
      {
        test: /^[a-z0-9_]{7,20}$/,
        message: 'Password must contain only alphabets with numeric and must be between 7 and 20 characters in length.'
      }
    ],
    valid: false,
    errors: [],
    state: ''
  }
};

export default validators;