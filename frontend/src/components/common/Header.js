import React from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import NavDropdown from 'react-bootstrap/NavDropdown';
import { NavLink } from 'react-router-dom';
import $ from 'jquery';

class Header extends React.Component {
	state = {
		expanded: false
	};
	getCookie(cname) {
	  var name = cname + "=";
	  var decodedCookie = decodeURIComponent(document.cookie);
	  var ca = decodedCookie.split(';');
	  for(var i = 0; i <ca.length; i++) {
	    var c = ca[i];
	    while (c.charAt(0) == ' ') {
	      c = c.substring(1);
	    }
	    if (c.indexOf(name) == 0) {
	      return c.substring(name.length, c.length);
	    }
	  }
	  return "";
	}

	toggleNavbar() {

	}

	renderLogin() {
		if(this.props.logged_in && this.props.user_type == 'admin') {
			return (
				<NavDropdown expanded={this.state.expanded} title="Admin" id="basic-nav-dropdown">
					<Nav.Item>
						<Nav.Link href="/admin/users">Users</Nav.Link>
					</Nav.Item>
					<Nav.Item>
						<Nav.Link href="/admin/subscriptions">Subscriptions</Nav.Link>
					</Nav.Item>
					<Nav.Item>
						<Nav.Link href="/admin/appointments">Appointments</Nav.Link>
					</Nav.Item>
					<Nav.Item>
						<Nav.Link href="/admin/blogs/manage">Blog</Nav.Link>
					</Nav.Item>
					<Nav.Item>
						<Nav.Link href="/logout">Logout</Nav.Link>
					</Nav.Item>
			    </NavDropdown>
			); 
		} else if(this.props.logged_in) {
			return (
				<NavDropdown title="My Profile" id="basic-nav-dropdown">
					<Nav.Item>
						<Nav.Link onClick={this.toggle.bind(this)} href="/subscriptions">Subscriptions</Nav.Link>
					</Nav.Item>
					<Nav.Item>
						<Nav.Link onClick={this.toggle.bind(this)} href="/profile">Profile</Nav.Link>
					</Nav.Item>
					<Nav.Item>
						<Nav.Link onClick={this.toggle.bind(this)} href="/logout">Logout</Nav.Link>
					</Nav.Item>
			    </NavDropdown>
			); 
		} else {
			return (
				<NavLink onClick={this.toggle.bind(this)} className="nav-link" activeClassName="active" exact to="/login">Log In</NavLink>
			);
		}
	}

	toggle() {
      this.setState({ expanded: !this.state.expanded });
    }

	render() {
		return (
			<Navbar onToggle={this.toggle.bind(this)} expanded={this.state.expanded} fixed="top" bg="light" expand="lg">
		        <Navbar.Brand>Appmosphere</Navbar.Brand>
		        <Navbar.Toggle aria-controls="basic-navbar-nav" />
		        <Navbar.Collapse id="basic-navbar-nav">
		          <Nav className="justify-content-end" style={{ width: "80%" }}>
		            <NavLink onClick={this.toggle.bind(this)} className="nav-link" activeClassName="active" exact to="/">Home</NavLink>
		            <NavLink  className="nav-link" to="/blogs">Customer Stories</NavLink>
		            <NavLink onClick={this.toggle.bind(this)} className="nav-link" activeClassName="active" exact to="/services">Services</NavLink>
		            <NavLink onClick={this.toggle.bind(this)} className="nav-link" activeClassName="active" exact to="/contact">Contact Us</NavLink>
		       		{this.renderLogin()}     
		          </Nav>
		        </Navbar.Collapse>
		    </Navbar>
		);
	}
}

export default Header;