import CKEditor from 'ckeditor4-react';
import React from 'react';
import { Form, Row, Col, Breadcrumb, Button, Alert, Spinner, Pagination, Card } from 'react-bootstrap';
import config from '../../config';
import moment from 'moment';
import { NavLink } from 'react-router-dom';

class BlogCard extends React.Component {
	render() {
		return (
			<Card style={{ width: '18rem', minHeight: '390px' }}>
			  <Card.Img variant="top" src={config.api_url+this.props.blog.thumbnail} className="image-card"/>
			  <Card.Body>
			    <Card.Title>
			    	<NavLink to={"/blogs/"+this.props.blog.id}>{this.props.blog.title}</NavLink>
			    </Card.Title>
			    <Card.Text>
			      {this.props.blog.short_text}
			    </Card.Text>
			    <Card.Text>
			      {moment(this.props.blog.created_at).format('LLLL')}
			    </Card.Text>
			  </Card.Body>
			</Card>
		)
	}
}

export default BlogCard;