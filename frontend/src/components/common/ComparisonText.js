import React from 'react';
import { Row, Col } from 'react-bootstrap';

class ComparisonText extends React.Component {
	componentDidMount() {
		console.log(this.props);
	}
	render() {
		return (
			<div style={{ display: 'block', width: '100%' }}>
				{
					this.props.config ? this.props.config.parameters.map( (param) => {
						return (
							<div style={{ display: 'flex' }}>
								<Col md={3} xs={6} style={{ borderBottom: '2px solid #CCDDE0' }}>
									<span style={{ color: param.heading ? '#676767' : '', fontWeight: param.heading ? 'bold' : '', lineHeight: '40px', paddingLeft: '10px' }}>{param.text}</span>
								</Col>
								<Col md={3} xs={2} style={{ padding: '10px', textAlign: 'center', borderBottom: '2px solid #CCDDE0', backgroundColor: '#DEE7E9'}}>
									{
										this.renderValue(param.values[0], param.heading)
									}
								</Col>
								<Col md={3} xs={2} style={{ padding: '10px', textAlign: 'center', borderBottom: '2px solid #CCDDE0'}}>
									{
										this.renderValue(param.values[1], param.heading)
									}
								</Col>
								<Col md={3} xs={2} style={{ padding: '10px', textAlign: 'center', borderBottom: '2px solid #CCDDE0', backgroundColor: '#DEE7E9'}}>
									{
										this.renderValue(param.values[2], param.heading)
									}
								</Col>
							</div>
						)
					}) : ''
				}
			</div>
		);
	}

	renderValue(value, heading) {
		if(heading) {
			return (<span></span>);
		}

		if(typeof value == 'string') {
			return (<span>{value}</span>);
		}
		if(typeof value == 'number' && value == 1) {
			return (<i style={{color: '#27a664'}} class="fa fa-check"></i>);
		}
		if(typeof value == 'number' && value == 0) {
			return (<i style={{color: '#a94442'}} class="fa fa-times"></i>);
		}
	}
}

export default ComparisonText;