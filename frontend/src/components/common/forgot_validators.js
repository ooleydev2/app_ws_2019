const validators = {
  email: {
    rules: [
      {
        test: (value) => {
          if(!value || !value.trim()) {
            return false;
          }
          return true;
        },
        message: 'Please enter your email.'
      },
      {
        test: /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/,
        message: 'Please enter a valid email.'
      }
    ],
    valid: false,
    errors: [],
    state: '' 
  }
};

export default validators;