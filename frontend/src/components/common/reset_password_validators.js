const validators = {
  password: {
    rules: [
      {
        test: (value) => {
          if(!value || !value.trim()) {
            return false;
          }
          return true;
        },
        message: 'Please enter your password.'
      },
      {
        test: /^[a-z0-9_]{7,20}$/,
        message: 'Password must contain only alphabets with numeric and must be between 7 and 20 characters in length.'
      }
    ],
    valid: false,
    errors: [],
    state: ''
  }
};

export default validators;