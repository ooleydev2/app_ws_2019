import React from 'react';
import Jumbotron from 'react-bootstrap/Jumbotron';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import './Footer.css';

class Footer extends React.Component {
	render() {
		return (
			<Container className="container-fluid footer" fluid="true">
		        <Row>
		            <Col className="footer-column">
		              <h4>About Appmosphere</h4>
		              <p className="regular-font"> 
		              	Appmosphere stands as a one stop destination for all types of modern software development to customize your Enterprise Relationship Management software(ERM). Our solution specialized in delivering a CRM, ERM, POS, and Access solutions.
		              </p>
		              <p>
		                © 2009-2019 Company
		              </p>
		              <p>
		                Passionate People. Creative Thinking
		              </p>
		            </Col>
		            <Col className="footer-column"></Col>
		            <Col className="footer-column">
		              <h3><b>Let’s talk business. Contact us and we will discuss further.</b></h3>
		              <h4><b className="red-text">info@appmosphere.com</b></h4>
		              <p className="margin-bottom-0">8044 Lago Mist Way</p>
		              <p className="margin-bottom-0">Innovation, FL, 33545</p>
		              <p className="margin-bottom-0">Tel. 009 123 4567</p>
		            </Col>
		        </Row>
		    </Container>
		);
	}
}

export default Footer;