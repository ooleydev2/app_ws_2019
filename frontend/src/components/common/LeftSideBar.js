import React from 'react';
import './LeftSideBar.css';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/FormControl';
import Carousel from 'react-bootstrap/Carousel';
import Jumbotron from 'react-bootstrap/Jumbotron';
import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Accordion from 'react-bootstrap/Accordion';
import Badge from 'react-bootstrap/Badge';
import Modal from 'react-bootstrap/Modal';
import config from '../../config';
import { NavLink } from 'react-router-dom';

class LeftSideBar extends React.Component {
	state = {
		menuOpen: false
	};

	openNav() {
		if(!this.state.menuOpen) {
			document.getElementById("mySidenav").style.width = "250px";
			document.getElementById("main").style.marginLeft = "250px";	
		} else {
			document.getElementById("mySidenav").style.width = "0px";
			document.getElementById("main").style.marginLeft = "0px";
		}
		this.setState({
			menuOpen: !this.state.menuOpen
		});
	}

	render() {
		return (
			<div>
				<div id="mySidenav" className="sidenav">
				  <a className="closebtn">&times;</a>
				  <NavLink className="nav-link nav-left-sidebar" activeClassName="active" exact to="/dashboard">Dashboard</NavLink>
				  <NavLink className="nav-link nav-left-sidebar" activeClassName="active" exact to="/profile">My Profile</NavLink>
				</div>

				<div id="main">
				  <h2>Sidenav Push Example</h2>
				  <span className="open-span" onClick={this.openNav.bind(this)}>&#9776;</span>
				</div>
			</div>
		);
	}
}

export default LeftSideBar;