import './Comparison.css';
import React from 'react';
import { Row, Col } from 'react-bootstrap';
import ComparisonText from './ComparisonText';

const configuration = [
	{ 
		sector: 'BUSINESS SECTORS', 
		parameters: [
			{
				text: 'BUSINESS SECTORS',
				values: [1, 1, 1],
				heading: true
			},
			{
				text: 'DISTRIBUTION',
				values: [1, 1, 1]
			},
			{
				text: 'GOVERNMENT & NPO',
				values: [1, 0, 1]
			},
			{
				text: 'MANUFACTURING',
				values: [1, 1, 1]
			},
			{
				text: 'RETAIL',
				values: [1, 1, 1]
			},
			{
				text: 'SERVICES',
				values: [1, 1, 1]
			}
		] 
	},
	{ 
		sector: 'INDUSTRY', 
		parameters: [
			{
				text: 'INDUSTRY',
				values: [1, 1, 1],
				heading: true
			},
			{
				text: 'AEROSPACE & DEFENSE',
				values: [1, 1, 1]
			},
			{
				text: 'AUTOMOTIVE',
				values: [1, 1, 1]
			},
			{
				text: 'CHEMICAL',
				values: [1, 1, 1]
			},
			{
				text: 'CONSTRUCTION',
				values: [1, 1, 1]
			},
			{
				text: 'COSMETICS',
				values: [1, 1, 1]
			},
			{
				text: 'ELECTRONICS',
				values: [1, 1, 1]
			},
			{
				text: 'ENERGY/POWER/UTILITIES',
				values: [1, 0, 1]
			},
			{
				text: 'ENGINEERING',
				values: [1, 1, 1]
			},
			{
				text: 'FASHION',
				values: [1, 1, 1]
			},
			{
				text: 'FINANCIAL SERVICES',
				values: [1, 1, 1]
			},
			{
				text: 'LOCAL GOVERNMENT',
				values: [1, 1, 1]
			},
			{
				text: 'MEDIA',
				values: [1, 0, 1]
			},
			{
				text: 'MEDIA & HEALTHCARE',
				values: [1, 1, 1]
			},
			{
				text: 'METALWORKING',
				values: [1, 1, 1]
			},
			{
				text: 'OILFIELD',
				values: [1, 1, 1]
			},
			{
				text: 'PACKAGING',
				values: [1, 1, 1]
			},
			{
				text: 'PAINT & ADHESIVES',
				values: [1, 1, 1]
			},
			{
				text: 'PHARMACEUTICAL',
				values: [1, 1, 1]
			},
			{
				text: 'PLASTICS & RUBBER',
				values: [1, 1, 1]
			},
			{
				text: 'PRINT & DESIGN',
				values: [1, 1, 1]
			},
			{
				text: 'PRINT & DESIGN',
				values: [1, 1, 1]
			},
			{
				text: 'RENTAL',
				values: [1, 0, 1]
			},
			{
				text: 'TELECOMMUNICATIONS',
				values: [1, 1, 1]
			},
			{
				text: 'WOODWORKING',
				values: [1, 1, 1]
			},
			{
				text: 'CANNABIS',
				values: [0, 0, 0]
			},
			{
				text: 'NONPROFIT',
				values: [0, 0, 0]
			}
		] 
	},
	{ 
		sector: 'ERP SOFTWARE FEATURES', 
		parameters: [
			{
				text: 'ERP SOFTWARE FEATURES',
				values: [1, 1, 1],
				heading: true
			},
			{
				text: 'BILLING',
				values: [1, 1, 1]
			},
			{
				text: 'BUSINESS INTELLIGENCE/ANALYTICS',
				values: [0, 1, 1]
			},
			{
				text: 'COSTING',
				values: [1, 1, 1]
			},
			{
				text: 'CRM',
				values: [1, 1, 1]
			},
			{
				text: 'CUSTOMER SERVICE',
				values: [1, 1, 1]
			},
			{
				text: 'PRODUCT DESIGN',
				values: [0, 0, 0]
			},
			{
				text: 'FINANCIALS AND ACCOUNTING',
				values: [1, 1, 1]
			},
			{
				text: 'HR',
				values: [1, 0, 1]
			},
			{
				text: 'INVENTORY MANAGEMENT',
				values: [1, 1, 1]
			},
			{
				text: 'ORDER MANAGEMENT',
				values: [1, 1, 1]
			},
			{
				text: 'PLANNING & SCHEDULING',
				values: [1, 1, 1]
			},
			{
				text: 'PROJECT MANAGEMENT',
				values: [1, 1, 1]
			},
			{
				text: 'PURCHASING',
				values: [1, 1, 1]
			},
			{
				text: 'QUALITY CONTROL',
				values: [1, 0, 1]
			},
			{
				text: 'SALES',
				values: [1, 1, 1]
			},
			{
				text: 'SHIPPING & DISTRIBUTION',
				values: [1, 1, 1]
			},
			{
				text: 'SUPPLY CHAIN MANAGEMENT',
				values: [0, 1, 1]
			},
			{
				text: 'WAREHOUSE MANAGEMENT',
				values: [1, 1, 0]
			},
			{
				text: 'ASSET MANAGEMENT',
				values: [0, 1, 1]
			},
			{
				text: 'DOCUMENT MANAGEMENT',
				values: [0, 1, 0]
			}
		] 
	},
	{ 
		sector: 'CUSTOMER SUITABILITY', 
		parameters: [
			{
				text: 'CUSTOMER SUITABILITY',
				values: [1, 1, 1],
				heading: true
			},
			{
				text: 'ENTERPRISE (1000+ EMPLOYEES)',
				values: [1, 0, 0]
			},
			{
				text: 'MEDIUM SIZE (251-1000 EMPLOYEES)',
				values: [1, 1, 1]
			},
			{
				text: 'SMALL BUSINESS (1-250 EMPLOYEES)',
				values: [1, 1, 1]
			}
		] 
	},
	{ 
		sector: 'MOBILE CAPABILITIES', 
		parameters: [
			{
				text: 'MOBILE CAPABILITIES',
				values: [1, 1, 1],
				heading: true
			},
			{
				text: 'ANDROID APP',
				values: [1, 1, 1]
			},
			{
				text: 'IOS APP',
				values: [0, 1, 1]
			},
			{
				text: 'WEB APP',
				values: [0, 1, 1]
			}
		] 
	},
	{ 
		sector: 'SYSTEM HOSTING', 
		parameters: [
			{
				text: 'SYSTEM HOSTING',
				values: [1, 1, 1],
				heading: true
			},
			{
				text: 'CLOUD',
				values: [1, 1, 1]
			},
			{
				text: 'INSTALLED ON PREMISE',
				values: [0, 1, 0]
			}
		] 
	},
	{ 
		sector: 'REVIEWS', 
		parameters: [
			{
				text: 'SYSTEM HOSTING',
				values: [1, 1, 1],
				heading: true
			},
			{
				text: 'REVIEW SCORE',
				values: [null, null, null]
			}
		] 
	},
	{ 
		sector: 'OTHER INFORMATION', 
		parameters: [
			{
				text: 'SYSTEM HOSTING',
				values: [1, 1, 1],
				heading: true
			},
			{
				text: 'IMPLEMENTATION TIMEFRAME',
				values: [null, '2 to 8 weeks', null]
			},
			{
				text: 'PRICING',
				values: ['€20/User/Month + Apps', null, '$1,188']
			}
		] 
	}
];

class Comparison extends React.Component {
	render() {
		return (
			<div className="container container-comparision">
				<Row style={{marginTop: '80px'}}>
					<Col md={3} xs={6} style={{ borderBottom: '2px solid #CCDDE0' }}>
						<span style={{ lineHeight: '40px', paddingLeft: '10px', fontWeight: 'bold' }}>OVERVIEW</span>
					</Col>
					<Col md={3} xs={2} style={{ padding: '10px', textAlign: 'center', borderBottom: '2px solid #CCDDE0', backgroundColor: '#DEE7E9'}}>
						<img class="logo-image" src={"images/odoo.png"} />
					</Col>
					<Col md={3} xs={2}  style={{ padding: '10px', textAlign: 'center', borderBottom: '2px solid #CCDDE0'}}>
						<img style={{border: '1px solid #ccc'}} class="logo-image" src={"images/sap.png"} />
					</Col>
					<Col md={3} xs={2} style={{ padding: '10px', textAlign: 'center', borderBottom: '2px solid #CCDDE0', backgroundColor: '#DEE7E9'}}>
						<img class="logo-image" src={"images/netsuite.jpg"} />
					</Col>
					<Col md={3} xs={6}  style={{ borderBottom: '2px solid #CCDDE0' }}>
						<span style={{ lineHeight: '40px', paddingLeft: '10px', fontWeight: 'bold' }}>PRODUCT NAME</span>
					</Col>
					<Col md={3} xs={2}  style={{ padding: '10px', textAlign: 'center', borderBottom: '2px solid #CCDDE0', backgroundColor: '#DEE7E9'}}>
						Odoo Online
					</Col>
					<Col md={3} xs={2} style={{ padding: '10px', textAlign: 'center', borderBottom: '2px solid #CCDDE0'}}>
						Sap Business One
					</Col>
					<Col md={3} xs={2}  style={{ padding: '10px', textAlign: 'center', borderBottom: '2px solid #CCDDE0', backgroundColor: '#DEE7E9'}}>
						NetSuite ERP
					</Col>

					{
						configuration.map( (singleConfig) => {
							return (<ComparisonText config={singleConfig}/>);
						})
					}
				</Row>
			</div>
		);
	}
}

export default Comparison;