const validators = {
  primary_name: {
    rules: [
      {
        test: (value) => {
          if(!value || !value.trim()) {
            return false;
          }
          return true;
        },
        message: 'Please enter contact name.'
      }
    ],
    valid: false,
    errors: [],
    state: ''
  },
  primary_email: {
    rules: [
      {
        test: (value) => {
          if(!value || !value.trim()) {
            return false;
          }
          return true;
        },
        message: 'Please enter your email.'
      },
      {
        test: /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/,
        message: 'Please enter a valid email.'
      }
    ],
    valid: false,
    errors: [],
    state: '' 
  },
  primary_phone: {
    rules: [
      {
        test: (value) => {
          if(!value || !value.trim()) {
            return false;
          }
          return true;
        },
        message: 'Please enter your contact number.'
      }
    ],
    valid: false,
    errors: [],
    state: '' 
  },
  business_name: {
    rules: [
      {
        test: (value) => {
          if(!value || !value.trim()) {
            return false;
          }
          return true;
        },
        message: 'Please enter your business name.'
      }
    ],
    valid: false,
    errors: [],
    state: '' 
  },
  business_address: {
    rules: [
      {
        test: (value) => {
          if(!value || !value.trim()) {
            return false;
          }
          return true;
        },
        message: 'Please enter your business address.'
      }
    ],
    valid: false,
    errors: [],
    state: '' 
  },
  business_website: {
    rules: [
      {
        test: (value) => {
          if(!value || !value.trim()) {
            return false;
          }
          return true;
        },
        message: 'Please enter your business website.'
      }
    ],
    valid: false,
    errors: [],
    state: '' 
  },
  experience: {
    rules: [
      {
        test: (value) => {
          if(!value || !value.trim()) {
            return false;
          }
          return true;
        },
        message: 'Please select your experience.'
      }
    ],
    valid: false,
    errors: [],
    state: '' 
  },
  consult_checkbox: {
    rules: [
      {
        test: (value) => {
          if(!value || !value.length) {
            return false;
          }
          return true;
        },
        message: 'Please select a value.'
      }
    ],
    valid: false,
    errors: [],
    state: '' 
  },
  solutions_interested: {
    rules: [
      {
        test: (value) => {
          if(!value || !value.length) {
            return false;
          }
          return true;
        },
        message: 'Please select a value.'
      }
    ],
    valid: false,
    errors: [],
    state: '' 
  },
  free_appointment: {
    rules: [
      {
        test: (value) => {
          if(!value || !value.trim()) {
            return false;
          }
          return true;
        },
        message: 'Please select whether you want a free appointment.'
      }
    ],
    valid: false,
    errors: [],
    state: '' 
  },
  custom_application: {
    rules: [
      {
        test: (value) => {
          if(!value || !value.trim()) {
            return false;
          }
          return true;
        },
        message: 'Please select whether you want a custom application.'
      }
    ],
    valid: false,
    errors: [],
    state: '' 
  },
  contact_person: {
    rules: [
      {
        test: (value) => {
          if(!value || !value.trim()) {
            return false;
          }
          return true;
        },
        message: 'Please select a value.'
      }
    ],
    valid: false,
    errors: [],
    state: '' 
  }
};

export default validators;