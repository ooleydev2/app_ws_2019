import React from 'react';
import './Signup.css';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/FormControl';
import Carousel from 'react-bootstrap/Carousel';
import Jumbotron from 'react-bootstrap/Jumbotron';
import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Accordion from 'react-bootstrap/Accordion';
import Badge from 'react-bootstrap/Badge';
import Modal from 'react-bootstrap/Modal';
import config from '../config';
import { NavLink } from 'react-router-dom';
import validators from './common/signup_validators';
import Spinner from 'react-bootstrap/Spinner';
import Alert from 'react-bootstrap/Alert';

class Login extends React.Component {
	state = {
		show: false,
		submitted: false,
		email: '',
		email_subscribed: false,
		errors: {},
		form_submitted: false,
		user_info: {
			name: '',
			email: '',
			password: ''
		},
		processing: false,
		successfull: false,
		failed: false
	};

	renderSuccess() {
		if(this.state.success) {
			return (
				<Alert variant={'success'}>
				    You have successfully signed up. Please verify your email in order to continue!
				</Alert>
			);
		}
	}

	renderFailed() {
		if(this.state.failed) {
			return (
				<Alert variant={'danger'}>
				   {this.state.failed}
				</Alert>
			);
		}
	}

	validators = validators;
	handleSubmit(e) {
		if(this.isFormValid()) {
			this.setState({
				processing: true
			});
			const formData = {
				'email': this.state.user_info.email,
				'password': this.state.user_info.password,
				'name': this.state.user_info.name
			};
			fetch(config.api_url+'api/users', {
				method: 'POST',
  				headers: {
		            'Content-Type': 'application/json'
		        },
  				body: JSON.stringify(formData)
			})
			.then( response => {
				let status = response.status;

				response.json()
				.then( success => {
					if(status == 200) {
						this.setState({
							failed: false,
							success: true,
							processing: false
						});
						document.getElementsByTagName('form')[0].reset();
					} else {
						this.setState({
							success: false,
							failed: success.description ? success.description : 'Something went wrong while processing your request. Please try again later.!',
							processing: false
						});
					}
				});
			})
			.catch( error => {
				this.setState({
					processing: false,
					success: false,
					failed: 'Something went wrong while processing your request. Please try again later.!'
				});
			});
		}
		e.preventDefault();
		e.stopPropagation();
	}

	handleChange(name, event) {
		let fieldName = name;
		let fieldValue = event.target.value;
		this.updateValidators(fieldName, fieldValue);
		let values = this.state.user_info;
		values[name] = event.target.value;
		this.setState({
			user_info: values
		});
	}

	isFormValid() {
	    let status = true;
	    Object.keys(this.validators).forEach((field) => {
	      this.updateValidators(field, this.state.user_info[field]);
	      if (!this.validators[field].valid) {
	        status = false;
	      }
	    });
	    this.setState({ form_submitted: true });
	    return status;
	}

	updateValidators(fieldName, value) {
	    this.validators[fieldName].errors = [];
	    this.validators[fieldName].state = value;
	    this.validators[fieldName].valid = true;
	    this.validators[fieldName].rules.forEach((rule) => {
	    	if(!this.validators[fieldName].errors.length) {
	    		if (rule.test instanceof RegExp) {
			        if (!rule.test.test(value)) {
			          this.validators[fieldName].errors.push(rule.message);
			          this.validators[fieldName].valid = false;
			        }
			    } else if (typeof rule.test === 'function') {
			        if (!rule.test(value)) {
			          this.validators[fieldName].errors.push(rule.message);
			          this.validators[fieldName].valid = false;
			        }
			    }
	    	}	
	    });
	}

	displayValidationErrors(fieldName) {
	    const validator = this.validators[fieldName];
	    const result = '';
	    if (validator && !validator.valid) {
	      const errors = validator.errors.map((info, index) => {
	        return <span className="error" key={index}>* {info}</span>;
	      });
	      return (
	        <Row>
	        	<Col md={12} sm={12} className="text text-danger">
	        		{errors}
	        	</Col>
	        </Row>
	      );
	    }
	    return result;
	}

	renderSubmitButton() {
		if(this.state.processing) {
			return (
				<Button disabled variant="primary" className="margin-center" type="submit">
			    	<Spinner
				      as="span"
				      animation="border"
				      size="sm"
				      role="status"
				      aria-hidden="true"
				    />	Signup
				</Button>
			);
		} else {
			return (
				<Button variant="primary" className="margin-center" type="submit">
			    	Signup
				</Button>
			);
		}
	}
	
	render() {
		return (
			<div>
				<Form noValidate onSubmit={e => this.handleSubmit(e)}>
					<Row className="top-class">
						<Col md={4} className="form-wrapper">
							<Row>
								<Col>
								{this.renderSuccess()}
								{this.renderFailed()}
								</Col>
							</Row>
							<Form.Row>
							    <Form.Group as={Col} controlId="formGridEmail">
							      <Form.Label>Name</Form.Label>
							      <Form.Control type="text" onChange={this.handleChange.bind(this, 'name')} name="name" required placeholder="John doe" />
							      {this.displayValidationErrors('name')}
							    </Form.Group>
							</Form.Row>

							<Form.Row>
							    <Form.Group as={Col} controlId="formGridEmail">
							      <Form.Label>Email</Form.Label>
							      <Form.Control type="email" onChange={this.handleChange.bind(this, 'email')} name="email" required placeholder="example@gmail.com" />
							      {this.displayValidationErrors('email')}
							    </Form.Group>
							</Form.Row>

							<Form.Group controlId="formGridAddress1">
							    <Form.Label>Password</Form.Label>
							    <Form.Control type="password" onChange={this.handleChange.bind(this, 'password')} required  name="address" placeholder="Enter Password" />
							    {this.displayValidationErrors('password')}
							</Form.Group>

							<Row className="checkout-button">
								<Col md={12} xs={12}>
									{this.renderSubmitButton()}
								</Col>
								<Col md={12} xs={12}>
									<NavLink exact to="/forgot" className="pull-left">Forgot Password?</NavLink>
									<NavLink exact to="/login" className="pull-right">Login here</NavLink>
								</Col>
							</Row>
						
						</Col>
					</Row>
				</Form>
			</div>
		);
	}
}

export default Login;