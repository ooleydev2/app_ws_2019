import React from 'react';
import './Dashboard.css';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/FormControl';
import Carousel from 'react-bootstrap/Carousel';
import Jumbotron from 'react-bootstrap/Jumbotron';
import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Accordion from 'react-bootstrap/Accordion';
import Badge from 'react-bootstrap/Badge';
import Spinner from 'react-bootstrap/Spinner';
import Alert from 'react-bootstrap/Alert';
import Modal from 'react-bootstrap/Modal';
import Breadcrumb from 'react-bootstrap/Breadcrumb';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Tooltip from 'react-bootstrap/Tooltip';
import moment from 'moment';

import config from '../config';
import LeftSideBar from './common/LeftSideBar';
import $ from 'jquery';

class Dashboard extends React.Component {
	state = {
		orders: [],
		loading: true,
		failed: false,
		show: false,
		current_subscription: '',
		current_subscription_item: null,
		cancelling_subscription: false,
		subscription_cancelled: false,
		error_cancelling: false
	};

	getCookie(cname) {
	  var name = cname + "=";
	  var decodedCookie = decodeURIComponent(document.cookie);
	  var ca = decodedCookie.split(';');
	  for(var i = 0; i <ca.length; i++) {
	    var c = ca[i];
	    while (c.charAt(0) == ' ') {
	      c = c.substring(1);
	    }
	    if (c.indexOf(name) == 0) {
	      return c.substring(name.length, c.length);
	    }
	  }
	  return "";
	}

	componentDidMount() {
		const that = this;
		fetch(config.api_url+'api/orders', {
			headers: {
				'Authorization': 'Bearer '+this.getCookie('session_app')
			}
		})
		.then( (response) => {
			response.json()
			.then( success => {
				that.setState({ orders: success.data, loading: false });
				console.log(this.state.orders);
			})
			.catch( error => {
				that.setState({ failed: true });
			});
		})
		.catch( (error) => {

		});
	}

	renderEmpty() {
		if(!this.state.orders.length) {
			return (
				<Alert variant={'warning'}>
				   You have no active subscriptions!
				</Alert>
			);
		}

		if(this.state.error) {
			return (
				<Alert variant={'danger'}>
				   Error while processing your request. Please try again later!
				</Alert>
			);
		}
	}

	cancelSubscriptionButton(item, event) {
		this.setState({ current_subscription: item.id });
		this.setState({ current_subscription_item: item });
		this.setState({ show: true });
	}

	renderSubscriptionButton(item) {
		if(item.subscription_state == 'active') {
			return (
				<Button variant="danger" className="pull-right" onClick={this.cancelSubscriptionButton.bind(this, item)}>Cancel</Button>
			);
		} else {
			return (
				<Button variant="danger" disabled className="pull-right">Cancelled</Button>
			);
		}
	}

	handleClosePopup() {
		this.setState({ show: false });
	}

	renderCancellingMessage() {
		if(this.state.cancelling_subscription) {
			return (
				<span>
					<Spinner
				      as="span"
				      animation="border"
				      size="sm"
				      role="status"
				      aria-hidden="true"
				    /> Cancelling Subscription
				</span>
			);
		} else {
			return (
				<span>Are you sure you want to cancel this subscription #{this.state.current_subscription}?</span>
			);
		}
	}

	subscription_cancelled() {
		if(this.state.subscription_cancelled) {
			return (
				<Alert variant={'success'}>
				   Subscription #{this.state.current_subscription} cancelled successfuly!
				</Alert>
			);
		}

		if(this.state.error_cancelling) {
			return (
				<Alert variant={'danger'}>
				   {this.state.error_cancelling}
				</Alert>
			);
		}
	}

	cancelSubscription() {
		console.log("Cancle ", this.state.current_subscription_item);
		const that = this;
		console.log(this.state.current_subscription_item);
		this.setState({ cancelling_subscription: true });
		fetch(config.api_url+'api/orders/cancel_subscription', {
			method: 'POST',
			headers: {
	            'Content-Type': 'application/json',
	        	'Authorization': 'Bearer '+this.getCookie('session_app')
	        },
			body: JSON.stringify({ subscription_id: this.state.current_subscription_item.stripe_id })
		})
		.then( (response) => {
			console.log(response.status);
			response.json()
			.then( (success) => {
				if(response.status == 200) {
					that.setState({
						error_cancelling: false,
						cancelling_subscription: false,
						subscription_cancelled: true,
						show: false
					});
					let subscriptions = this.state.orders;
					subscriptions.forEach((subscription, index) => {
						if (that.state.current_subscription == subscription.id) {
							subscriptions[index]['subscription_state'] = 'cancelled';
						}
					});
					this.setState({ orders: subscriptions });
				} else {
					that.setState({
						error_cancelling: success.description ? success.description : 'Something went wrong while cancelling subscription. Please try again later!',
						cancelling_subscription: false,
						show: false,
						subscription_cancelled: false
					});
				}
			})
			.catch( (error) => {
				that.setState({
					error_cancelling: 'Something went wrong while cancelling subscription. Please try again later!',
					cancelling_subscription: false,
					show: false,
					subscription_cancelled: false
				});
			});
		})
		.catch( (error) => {
			that.setState({
				error_cancelling: 'Something went wrong while cancelling subscription. Please try again later!',
				cancelling_subscription: false,
				show: false,
				subscription_cancelled: false
			});
		});
	}

	render() {
		setTimeout( () => {
			$('.footer').css('display', 'none');
		} ,200);
		return (
			<div className="orders-container container" fluid={true}>
				<Modal show={this.state.show}>
		          <Modal.Header closeButton  onClick={this.handleClosePopup.bind(this)}>
		            <Modal.Title>Cancel <strong>Subscription?</strong></Modal.Title>
		          </Modal.Header>
		          <Modal.Body>
		          	{this.renderCancellingMessage()}
		          </Modal.Body>
		          <Modal.Footer>
		            <Button variant="primary" onClick={this.handleClosePopup.bind(this)}>
		              Close
		            </Button>
		            <Button variant="danger" onClick={this.cancelSubscription.bind(this)}>
		              Cancel Subscription
		            </Button>
		          </Modal.Footer>
		        </Modal>
				<Breadcrumb>
				  <Breadcrumb.Item>Your Subscriptions</Breadcrumb.Item>
				</Breadcrumb>
				{this.subscription_cancelled()}
				{this.renderEmpty()}
				{
					this.state.orders.map( (order) => {
						return (
							<Card className="plans-card">
							  <Card.Header as="h5">Plan #{order.id} {this.renderSubscriptionButton(order)} Created on {moment(order.created_at).format('LLLL')}</Card.Header>
							  <Card.Body>
							    <Card.Title>
							    	{order.total} USD / {order.duration == 'annual' ? 'Month' : 'Year'}
							    </Card.Title>
							    <Card.Text>
							    	<Row>
							    	{
							    		order.order_items.map( (order_item) => {
							    			return (
							    				<Col key={order_item.id} md={4}>
									      			<Row className="product-column">
									      				<Col md="3" xs="3" className="padding-left-0">
									      					<img src={order_item.plan.image} />
									      				</Col>
									      				<Col md="9" xs="9"  className="padding-left-0 padding-right-0">
									      					<Row>
									      						<Col><strong>{order_item.plan.name}</strong> 
									      						</Col>
									      					</Row>
									      					<Row>
									      						<Col>
									      							{Number(order_item.plan.price+((order_item.plan.price * order_item.plan.fee)/100)).toFixed(2)} USD / month
									      							<OverlayTrigger overlay={<Tooltip id="tooltip-disabled">Tooltip!</Tooltip>}>
																	  <span className="pull-right info-pricing">
																	  </span>
																	</OverlayTrigger>
									      						</Col>
									      					</Row>
									      				</Col>
									      			</Row>
									      		</Col>
							    			);
							    		})
							    	}
							    	</Row>
							    </Card.Text>
							  </Card.Body>
							</Card>
						);
					})
				}
			</div>
		);
	}
}

export default Dashboard;