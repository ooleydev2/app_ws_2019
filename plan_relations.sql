-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jul 18, 2019 at 06:27 PM
-- Server version: 5.7.26
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `appmosphere_new`
--

-- --------------------------------------------------------

--
-- Table structure for table `plan_relations`
--

DROP TABLE IF EXISTS `plan_relations`;
CREATE TABLE IF NOT EXISTS `plan_relations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plan_id` int(11) DEFAULT NULL,
  `required_plan_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `plan_relations`
--

INSERT INTO `plan_relations` (`id`, `plan_id`, `required_plan_id`, `created_at`, `updated_at`) VALUES
(1, 3, 2, '2019-07-16 23:14:56', NULL),
(2, 5, 4, '2019-07-16 23:14:56', NULL),
(3, 5, 2, '2019-07-16 23:14:56', NULL),
(4, 6, 9, '2019-07-16 23:14:56', NULL),
(5, 6, 2, '2019-07-16 23:14:56', NULL),
(6, 7, 2, '2019-07-16 23:14:56', NULL),
(7, 10, 9, '2019-07-16 23:14:56', NULL),
(8, 11, 2, '2019-07-16 23:14:56', NULL),
(9, 12, 8, '2019-07-16 23:14:56', NULL),
(10, 14, 2, '2019-07-16 23:14:56', NULL),
(11, 15, 4, '2019-07-16 23:14:56', NULL),
(12, 19, 2, '2019-07-16 23:14:56', NULL),
(13, 22, 9, '2019-07-16 23:14:56', NULL),
(14, 25, 9, '2019-07-16 23:14:56', NULL),
(15, 26, 4, '2019-07-16 23:14:56', NULL),
(16, 27, 13, '2019-07-16 23:14:56', NULL),
(17, 30, 9, '2019-07-16 23:14:56', NULL),
(18, 30, 2, '2019-07-16 23:14:56', NULL),
(19, 30, 3, '2019-07-16 23:14:56', NULL),
(20, 31, 9, '2019-07-16 23:14:56', NULL),
(21, 31, 2, '2019-07-16 23:14:56', NULL),
(22, 31, 3, '2019-07-16 23:14:56', NULL),
(23, 32, 9, '2019-07-16 23:14:56', NULL),
(24, 32, 2, '2019-07-16 23:14:56', NULL),
(25, 32, 3, '2019-07-16 23:14:56', NULL),
(26, 33, 9, '2019-07-16 23:14:56', NULL),
(27, 33, 2, '2019-07-16 23:14:56', NULL),
(28, 33, 3, '2019-07-16 23:14:56', NULL),
(29, 34, 9, '2019-07-16 23:14:56', NULL),
(30, 34, 2, '2019-07-16 23:14:56', NULL),
(31, 34, 3, '2019-07-16 23:14:56', NULL),
(32, 36, 9, '2019-07-16 23:14:56', NULL),
(33, 36, 2, '2019-07-16 23:14:56', NULL),
(34, 36, 3, '2019-07-16 23:14:56', NULL),
(35, 37, 9, '2019-07-16 23:14:56', NULL),
(36, 37, 2, '2019-07-16 23:14:56', NULL),
(37, 37, 3, '2019-07-16 23:14:56', NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
