-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jul 14, 2019 at 06:53 PM
-- Server version: 5.7.26
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `appmosphere_new`
--

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

DROP TABLE IF EXISTS `blogs`;
CREATE TABLE IF NOT EXISTS `blogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `thumbnail` varchar(250) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `short_text` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `title`, `content`, `thumbnail`, `created_at`, `updated_at`, `user_id`, `short_text`) VALUES
(16, 'How a Dedicated Team of Odoo Consultants Helped Florida Iron Doors\r\n', '<p>Hello from CKEditor 4!</p>', 'uploads/blog/file-1562873517607.png', '2019-07-11 19:31:57', '2019-07-11 19:31:57', 30, 'Find out how the consultants helped'),
(17, 'How a Dedicated Team of Odoo Consultants Helped Florida Iron Doors\r\n', '<p><img alt=\"\" src=\"http://localhost:3001/uploads/blog/upload-1562873549105.png\" style=\"height:968px; width:1604px\" /></p>\n', 'uploads/blog/file-1562873565367.png', '2019-07-11 19:32:45', '2019-07-11 19:32:45', 30, 'Find out how the consultants helped'),
(18, 'How a Dedicated Team of Odoo Consultants Helped Florida Iron Doors\r\n', '<p>Hello from CKEditor 4!</p>', 'uploads/blog/file-1562873517607.png', '2019-07-11 19:31:57', '2019-07-11 19:31:57', 30, 'Find out how the consultants helped'),
(19, 'How a Dedicated Team of Odoo Consultants Helped Florida Iron Doors\r\n', '<p>Hello from CKEditor 4!</p>', 'uploads/blog/file-1562873517607.png', '2019-07-11 19:31:57', '2019-07-11 19:31:57', 30, 'Find out how the consultants helped'),
(20, 'How a Dedicated Team of Odoo Consultants Helped Florida Iron Doors\r\n', '<p>Hello from CKEditor 4!</p>', 'uploads/blog/file-1562873517607.png', '2019-07-11 19:31:57', '2019-07-11 19:31:57', 30, 'Find out how the consultants helped'),
(21, 'How a Dedicated Team of Odoo Consultants Helped Florida Iron Doors\r\n', '<p>Hello from CKEditor 4!</p>', 'uploads/blog/file-1562873517607.png', '2019-07-11 19:31:57', '2019-07-11 19:31:57', 30, 'Find out how the consultants helped'),
(22, 'How a Dedicated Team of Odoo Consultants Helped Florida Iron Doors\r\n', '<p>Hello from CKEditor 4!</p>', 'uploads/blog/file-1562873517607.png', '2019-07-11 19:31:57', '2019-07-11 19:31:57', 30, 'Find out how the consultants helped'),
(23, 'How a Dedicated Team of Odoo Consultants Helped Florida Iron Doors\r\n', '<p>Hello from CKEditor 4!</p>', 'uploads/blog/file-1562873517607.png', '2019-07-11 19:31:57', '2019-07-11 19:31:57', 30, 'Find out how the consultants helped'),
(24, 'How a Dedicated Team of Odoo Consultants Helped Florida Iron Doors\r\n', '<p>Hello from CKEditor 4!</p>', 'uploads/blog/file-1562873517607.png', '2019-07-11 19:31:57', '2019-07-11 19:31:57', 30, 'Find out how the consultants helped'),
(25, 'How a Dedicated Team of Odoo Consultants Helped Florida Iron Doors\r\n', '<p>Hello from CKEditor 4!</p>', 'uploads/blog/file-1562873517607.png', '2019-07-11 19:31:57', '2019-07-11 19:31:57', 30, 'Find out how the consultants helped'),
(26, 'How a Dedicated Team of Odoo Consultants Helped Florida Iron Doors\r\n', '<p>Hello from CKEditor 4!</p>', 'uploads/blog/file-1562873517607.png', '2019-07-11 19:31:57', '2019-07-11 19:31:57', 30, 'Find out how the consultants helped'),
(27, 'How a Dedicated Team of Odoo Consultants Helped Florida Iron Doors\r\n', '<p>Hello from CKEditor 4!</p>', 'uploads/blog/file-1562873517607.png', '2019-07-11 19:31:57', '2019-07-11 19:31:57', 30, 'Find out how the consultants helped'),
(28, 'How a Dedicated Team of Odoo Consultants Helped Florida Iron Doors\r\n', '<p>Hello from CKEditor 4!</p>', 'uploads/blog/file-1562873517607.png', '2019-07-11 19:31:57', '2019-07-11 19:31:57', 30, 'Find out how the consultants helped'),
(29, 'How a Dedicated Team of Odoo Consultants Helped Florida Iron Doors\r\n', '<p>Hello from CKEditor 4!</p>', 'uploads/blog/file-1562873517607.png', '2019-07-11 19:31:57', '2019-07-11 19:31:57', 30, 'Find out how the consultants helped'),
(30, 'How a Dedicated Team of Odoo Consultants Helped Florida Iron Doors\r\n', '<p>Hello from CKEditor 4!</p>', 'uploads/blog/file-1562873517607.png', '2019-07-11 19:31:57', '2019-07-11 19:31:57', 30, 'Find out how the consultants helped'),
(31, 'How a Dedicated Team of Odoo Consultants Helped Florida Iron Doors\r\n', '<p>Hello from CKEditor 4!</p>', 'uploads/blog/file-1562873517607.png', '2019-07-11 19:31:57', '2019-07-11 19:31:57', 30, 'Find out how the consultants helped'),
(32, 'How a Dedicated Team of Odoo Consultants Helped Florida Iron Doors\r\n', '<p>Hello from CKEditor 4!</p>', 'uploads/blog/file-1562873517607.png', '2019-07-11 19:31:57', '2019-07-11 19:31:57', 30, 'Find out how the consultants helped'),
(33, 'How a Dedicated Team of Odoo Consultants Helped Florida Iron Doors\r\n', '<p>Hello from CKEditor 4!</p>', 'uploads/blog/file-1562873517607.png', '2019-07-11 19:31:57', '2019-07-11 19:31:57', 30, 'Find out how the consultants helped'),
(34, 'How a Dedicated Team of Odoo Consultants Helped Florida Iron Doors\r\n', '<p>Hello from CKEditor 4!</p>', 'uploads/blog/file-1562873517607.png', '2019-07-11 19:31:57', '2019-07-11 19:31:57', 30, 'Find out how the consultants helped'),
(35, 'How a Dedicated Team of Odoo Consultants Helped Florida Iron Doors\r\n', '<p>Hello from CKEditor 4!</p>', 'uploads/blog/file-1562873517607.png', '2019-07-11 19:31:57', '2019-07-11 19:31:57', 30, 'Find out how the consultants helped'),
(36, 'How a Dedicated Team of Odoo Consultants Helped Florida Iron Doors\r\n', '<p>Hello from CKEditor 4!</p>', 'uploads/blog/file-1562873517607.png', '2019-07-11 19:31:57', '2019-07-11 19:31:57', 30, 'Find out how the consultants helped'),
(37, 'How a Dedicated Team of Odoo Consultants Helped Florida Iron Doors\r\n', '<p>Hello from CKEditor 4!</p>', 'uploads/blog/file-1562873517607.png', '2019-07-11 19:31:57', '2019-07-11 19:31:57', 30, 'Find out how the consultants helped'),
(38, 'How a Dedicated Team of Odoo Consultants Helped Florida Iron Doors\r\n', '<p>Hello from CKEditor 4!</p>', 'uploads/blog/file-1562873517607.png', '2019-07-11 19:31:57', '2019-07-11 19:31:57', 30, 'Find out how the consultants helped'),
(39, 'How a Dedicated Team of Odoo Consultants Helped Florida Iron Doors\r\n', '<p>Hello from CKEditor 4!</p>', 'uploads/blog/file-1562873517607.png', '2019-07-11 19:31:57', '2019-07-11 19:31:57', 30, 'Find out how the consultants helped'),
(40, 'How a Dedicated Team of Odoo Consultants Helped Florida Iron Doors\r\n', '<p>Hello from CKEditor 4!</p>', 'uploads/blog/file-1562873517607.png', '2019-07-11 19:31:57', '2019-07-11 19:31:57', 30, 'Find out how the consultants helped'),
(41, 'How a Dedicated Team of Odoo', '<p>Hello from CKEditor 4! nqwerqwerqwerwqe</p>\n', 'uploads/blog/file-1563088188153.undefined', '2019-07-11 19:31:57', '2019-07-14 07:09:48', 30, 'Find out how the consultants helped'),
(45, 'Wn', '<p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p><p>Hello from CKEditor 4!</p>', 'uploads/blog/file-1563034493844.png', '2019-07-13 16:14:53', '2019-07-13 16:14:53', 30, NULL),
(46, 'Wn', '<p>cxzczxcvccx</p>\n', 'uploads/blog/file-1563034590275.png', '2019-07-13 16:16:30', '2019-07-13 16:16:30', 30, 'Description'),
(47, 'This is the title of the blog post', '<p><img alt=\"\" src=\"http://localhost:3001/uploads/blog/upload-1563035995556.png\" style=\"height:500px; width:407px\" /></p>\n\n<p>&nbsp;</p>\n\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n', 'uploads/blog/file-1563036047190.png', '2019-07-13 16:40:47', '2019-07-14 12:34:10', 30, 'Short description of the blog'),
(48, 'How a Dedicated Team of Odoo Consultants Helped Florida Iron Doors', '<p>Source source&nbsp;Source sourceSource sourceSource sourceSource sourceSource sourceSource sourceSource sourceSource sourceSource sourceSource sourceSource sourceSource sourceSource source</p>\n', 'uploads/blog/file-1563112541533.png', '2019-07-14 13:55:41', '2019-07-14 13:55:41', 30, 'Find out how the consultants helped'),
(49, 'Wn', '<p>Body</p>\r\n', 'uploads/blog/file-1563124484035.jpg', '2019-07-14 13:57:34', '2019-07-14 17:14:44', 30, 'Find out how the consultants helped'),
(50, 'How a Dedicated Team of Odoo Consultants Helped Florida Iron Doors', '<p>Find out how the consultants helpedFind out how the consultants helpedFind out how the consultants helpedFind out how the consultants helpedFind out how the consultants helpedFind out how the consultants helped</p>\n', 'uploads/blog/file-1563125012309.png', '2019-07-14 17:23:32', '2019-07-14 17:23:32', 30, 'Find out how the consultants helped'),
(51, 'How a Dedicated Team of Odoo Consultants Helped Florida Iron Doors', '<p>Find out how the consultants helped</p>\n', 'uploads/blog/file-1563129723320.png', '2019-07-14 18:42:03', '2019-07-14 18:42:03', 30, 'Find out how the consultants helped');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
