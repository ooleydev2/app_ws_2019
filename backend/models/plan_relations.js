'use strict';
module.exports = (sequelize, DataTypes) => {
  var plan_relations = sequelize.define('plan_relations', {
	plan_id: DataTypes.INTEGER,
	required_plan_id: DataTypes.INTEGER,
	created_at: DataTypes.DATE,
	updated_at: DataTypes.DATE
  }, {
	underscored: true
  });

  plan_relations.associate = function(models) {
  	plan_relations.belongsTo(models.plans);
  	plan_relations.belongsTo(models.plans, { as: 'required_plan', foreignKey: 'required_plan_id', targetKey: 'id' });
  }

  return plan_relations;
};