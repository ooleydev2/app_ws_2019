'use strict';
const config = require('config');
const bcrypt = require('bcrypt');

module.exports = (sequelize, DataTypes) => {
  var users = sequelize.define('users', {
     email: DataTypes.STRING,
     address: DataTypes.STRING,
     address_1: DataTypes.STRING,
     zipcode: DataTypes.STRING,
     country: DataTypes.STRING,
     state: DataTypes.STRING,
     city: DataTypes.STRING,
     created_at: DataTypes.DATE,
     company_name: DataTypes.STRING,
     tin: DataTypes.STRING,
     phone: DataTypes.STRING,
     password: DataTypes.STRING,
     verify_token: DataTypes.STRING,
     verified: DataTypes.STRING,
     reset_token: DataTypes.STRING,
     name: DataTypes.STRING,
     type: DataTypes.STRING
  }, {
	underscored: true,
    freezeTableName: true,
	hooks: {
		beforeCreate: (user) => {
            return bcrypt.hash(user.password, config.salt_rounds)
            .then((hash) => {
                user.password = hash; // eslint-disable-line no-param-reassign
            });
        }
	}
  });

  users.associate = function(models){
    users.belongsTo(models.countries, {as: 'countries', foreignKey:'country', targetKey: 'id'});
    users.belongsTo(models.states, {as: 'states', foreignKey: 'state', targetKey: 'id' });
    users.hasMany(models.orders, {foreignKey: 'user_id', targetKey: 'id'});
  }

  return users;
};