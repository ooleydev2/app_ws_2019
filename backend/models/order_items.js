'use strict';
module.exports = (sequelize, DataTypes) => {
  var order_items = sequelize.define('order_items', {
	plan_id: DataTypes.STRING,
	price: DataTypes.FLOAT,
	order_id: DataTypes.INTEGER 
  }, {
	underscored: true,
	freezeTableName: true
  });

  order_items.associate = function(models){
    order_items.belongsTo(models.orders); 
    order_items.belongsTo(models.plans);
  }

  return order_items;
};