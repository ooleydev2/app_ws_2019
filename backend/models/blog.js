'use strict';

module.exports = (sequelize, DataTypes) => {
  var blogs = sequelize.define('blogs', {
	title: DataTypes.STRING,
	content: DataTypes.TEXT,
	thumbnail: DataTypes.STRING,
	short_text: DataTypes.STRING,
	user_id: DataTypes.INTEGER,
	created_at: DataTypes.DATE,
	updated_at: DataTypes.DATE
  }, {
	underscored: true
  });

  blogs.associate = function(models) {
  	blogs.belongsTo(models.users);
  }

  return blogs;
};