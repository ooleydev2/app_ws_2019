'use strict';

module.exports = (sequelize, DataTypes) => {
  var appointments = sequelize.define('appointments', {
	primary_email: DataTypes.STRING,
	primary_name: DataTypes.STRING,
	primary_phone: DataTypes.STRING,
	updated_at: DataTypes.DATE,
	date: DataTypes.DATE,
	contact_person: DataTypes.STRING,
	custom_application: DataTypes.STRING,
	free_appointment: DataTypes.STRING,
	solutions_interested: DataTypes.STRING,
	consult_checkbox: DataTypes.STRING,
	experience: DataTypes.STRING,
	business_website: DataTypes.STRING,
	business_address: DataTypes.STRING,
	business_name: DataTypes.STRING,
	created_at: DataTypes.DATE,
	ip: DataTypes.STRING,
	date: DataTypes.DATE
  }, {
	underscored: true
  });

  return appointments;
};