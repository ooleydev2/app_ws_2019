'use strict';

module.exports = (sequelize, DataTypes) => {
  var messages = sequelize.define('messages', {
	name: DataTypes.STRING,
	email: DataTypes.STRING,
	message: DataTypes.STRING,
	created_at: DataTypes.DATE,
	ip: DataTypes.STRING
  }, {
	underscored: true
  });

  return messages;
};