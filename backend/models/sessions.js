'use strict';
module.exports = (sequelize, DataTypes) => {
  var sessions = sequelize.define('sessions', {
	token: DataTypes.STRING,
	created_at: DataTypes.DATE,
	deleted_at: DataTypes.DATE
  }, {
	paranoid: true,
	underscored: true
  });

  return sessions;
};