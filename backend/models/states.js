'use strict';
module.exports = (sequelize, DataTypes) => {
  var states = sequelize.define('states', {
	name: DataTypes.STRING,
	country_id: DataTypes.INTEGER
  }, {
	underscored: true,
	freezeTableName: true
  });

  return states;
};