'use strict';
module.exports = (sequelize, DataTypes) => {
  var countries = sequelize.define('countries', {
	sortname: DataTypes.STRING,
	name: DataTypes.STRING,
	phonecode: DataTypes.STRING
  }, {
	underscored: true,
	freezeTableName: true
  });

  return countries;
};