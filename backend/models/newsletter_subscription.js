'use strict';
module.exports = (sequelize, DataTypes) => {
  var newsletter_subscription = sequelize.define('newsletter_subscription', {
	email: DataTypes.STRING,
	ip: DataTypes.STRING,
	created_at: DataTypes.DATE,
	updated_at: DataTypes.DATE
  }, {
	underscored: true,
	freezeTableName: true
  });

  return newsletter_subscription;
};