'use strict';
module.exports = (sequelize, DataTypes) => {
  var plans = sequelize.define('plans', {
	name: DataTypes.STRING,
	price: DataTypes.FLOAT,
	image: DataTypes.STRING,
	require_plan_id: DataTypes.INTEGER,
	created_at: DataTypes.DATE,
	updated_at: DataTypes.DATE,
	categories_id: DataTypes.INTEGER,
	fee: DataTypes.FLOAT
  }, {
	underscored: true
  });

  plans.associate = function(models) {
  	plans.hasMany(models.plan_relations);
  }

  return plans;
};