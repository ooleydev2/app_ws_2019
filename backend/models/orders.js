'use strict';
module.exports = (sequelize, DataTypes) => {
  var orders = sequelize.define('orders', {
	email: DataTypes.STRING,
	address: DataTypes.STRING,
	address_1: DataTypes.STRING,
	zipcode: DataTypes.STRING,
	country: DataTypes.STRING,
	state: DataTypes.STRING,
	city: DataTypes.STRING,
	company_name: DataTypes.STRING,
	tin: DataTypes.STRING,
	phone: DataTypes.STRING,
	total: DataTypes.STRING,
	discount: DataTypes.STRING,
	stripe_id: DataTypes.STRING,
	created_at: DataTypes.DATE,
	updated_at: DataTypes.DATE,
	subscription_state: DataTypes.STRING,
	user_id: DataTypes.STRING,
	users: DataTypes.STRING
  }, {
	paranoid: true,
	underscored: true,
	freezeTableName: true
  });

  orders.associate = function(models) {
  	orders.hasMany(models.order_items);
  	orders.belongsTo(models.users);
  }

  return orders;
};