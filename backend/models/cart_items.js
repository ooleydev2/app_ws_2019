'use strict';

module.exports = (sequelize, DataTypes) => {
  var sessions = sequelize.define('cart_items', {
	token: DataTypes.STRING,
	created_at: DataTypes.DATE,
	deleted_at: DataTypes.DATE
  }, {
	paranoid: true,
	underscored: true
  });

  return sessions;
};