const passport = require('passport');

const LocalStrategy = require('passport-local').Strategy;

const models = require('../models');

const passportJWT = require('passport-jwt');

const JWTStrategy = passportJWT.Strategy;

const ExtractJWT = passportJWT.ExtractJwt;

const config = require('config');

const bcrypt = require('bcrypt');

passport.use(new JWTStrategy(
    {
        jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
        secretOrKey: config.jwt_secret,
    },
    (jwtPayload, cb) =>
        // find the user in db if needed.
        // This functionality may be omitted if you store everything you'll need in JWT payload.
        models.users.findOne({
            where: {
                id: jwtPayload.id   
            }
        })
            .then(user => cb(null, user))
            .catch(err => cb(err)),

));

module.exports = passport;
