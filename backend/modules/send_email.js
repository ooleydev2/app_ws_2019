const ejs = require('ejs');
const nodemailer = require('nodemailer');
const config = require('config');

const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: config.smtp.user,
        pass: config.smtp.pass
    }
});

module.exports = {
    sendEmail: (template, data, to, from, subject) => {
        return new Promise( (resolve, reject) => {
            ejs.renderFile(template, data, (error, str) => {
                if(error){
                    return reject(error);
                }
                let mailOptions = {
                    from: config.smtp.user,
                    to: to,
                    subject: subject,
                    html: str,
                };
                transporter.sendMail(mailOptions, (err, info) => {
                    console.log(err);
                    console.log(info);
                    if(err){
                        return reject(err);
                    }
                    return resolve(true);
                });
            });
        });
    }
};