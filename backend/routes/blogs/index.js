var express = require('express');
var router = express.Router();
var multer = require('multer');
var storage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, 'public/uploads/blog')
    },
    filename: (req, file, cb) => {
      let fileName = file.originalname;
      let fileNameArray = fileName.split(".");
      cb(null, file.fieldname + '-' + Date.now() + '.' + fileNameArray[1]);
    }
});
var upload = multer({storage: storage});
var models = require('../../models');
const passport = require('../../modules/passport');
const blogFields = ['title', 'short_text', 'thumbnail', 'created_at', 'user_id'];

router.post('/add_blog', upload.single('file'), passport.authenticate('jwt', { session: false }), function(req, res, next) {
  try {
    if(req.file) {
      console.log(req.body);
      console.log({
        title: req.body.title,
        content: req.body.content,
        short_text: req.body.short_text,
        thumbnail: 'uploads/blog/'+req.file.filename,
        user_id: req.user.id
      });
      models.blogs.create({
        title: req.body.title,
        content: req.body.content,
        short_text: req.body.short_text,
        thumbnail: 'uploads/blog/'+req.file.filename,
        user_id: req.user.id
      })
      .then( (created) => {
        return res.status(200).send({ description: 'Blog post saved successfully!', data: created });
      })
      .catch( (error) => {
        return res.status(500).send({ description: 'Error while adding blog. Please try again later!' });
      });
    } else {
      return res.status(400).send({ description: 'Unable to upload file.' });
    }
  } catch(error) {
    return res.status(500).send({ description: 'Error while adding blog. Please try again later!' });
  }
});

router.post('/edit_blog/:id', upload.single('file'), passport.authenticate('jwt', { session: false }), function(req, res, next) {
  try {
    let data = {
      title: req.body.title,
      content: req.body.content,
      short_text: req.body.short_text
    };
    console.log(req.file);
    if(req.file && req.file.filename) {
      data['thumbnail'] = 'uploads/blog/'+req.file.filename;
    }

    console.log(data);

    models.blogs.update(data, {
      where: {
        id: req.params.id
      }
    })
    .then( (updated) => {
      return res.status(200).send({ description: 'Blog post updated successfully!', data: updated });
    })
    .catch( (error) => {
      console.log(error);
      return res.status(500).send({ description: 'Error while adding blog. Please try again later!' });
    });
  } catch(error) {
    console.log(error);
    return res.status(500).send({ description: 'Error while adding blog. Please try again later!' });
  }
});

router.get('/:id', (req, res, next) => {
  return models.blogs.findOne({
    where: {
      id: req.params.id
    },
    include: {
      model: models.users
    }
  })
  .then( (blog) => {
    return res.status(200).send({ data: blog, description: 'Blog details fetched successfully!' });
  })
  .catch( (error) => {
    return res.status(500).send({  description: error.message });
  });
});

router.get('/', (req, res, next) => {
  let offset = req.query.start ? parseInt(req.query.start) : 0;
  let limit = req.query.length ? parseInt(req.query.length) : 12;
  let responseType = 'array';
  if(req.query.responseType == 'json') {
    responseType = req.query.responseType;
  }
  let total_count = 0;
  let query = {
    include: {
      model: models.users
    },
    order:[
        ["id","DESC"]
    ]
  };

  if(req.query.search && req.query.search.value) {
    query["where"] = {
      [models.Sequelize.Op.or]: [{
        title: {
            [models.Sequelize.Op.like]: '%'+req.query.search.value+'%'    
          }
        },{
          short_text: {
            [models.Sequelize.Op.like]: '%'+req.query.search.value+'%'    
          }
        }
      ]
    }
  }

  if(req.query.order && req.query.order[0]) {
    query['order'] = [[blogFields[req.query.order[0].column], req.query.order[0].dir == 'asc' ? 'ASC' : 'DESC']];
  }

  models.blogs.findAndCountAll(query)
  .then( (result) => {
    query["offset"] = offset;
    query["limit"] = limit;
    total_count = result.count;
    return models.blogs.findAll(query);
  })
  .then( (blogs) => {
    if(responseType == 'json') {
      return res.status(200).send({ total: total_count, data: blogs, description: 'Successfully fetched blogs!' });
    } else {
      let response = [];
      if(blogs && blogs.length) {
        blogs.forEach( (blog) => {
          response.push([blog.title, blog.short_text, blog.thumbnail, blog.created_at, blog.user.name, blog.id]);
        });
      }
      return res.status(200).send({ iTotalDisplayRecords: total_count, recordsFiltered: blogs.length, recordsTotal: total_count, data: response });
    }
  })
  .catch( (error) => {
    console.log(error);
    return res.status(500).send({  description: error.message });
  })
});

module.exports = router;
