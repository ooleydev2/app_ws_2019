var express = require('express');
var router = express.Router();
var multer = require('multer');
var storage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, 'public/uploads/blog')
    },
    filename: (req, file, cb) => {
      let fileName = file.originalname;
      let fileNameArray = fileName.split(".");
      cb(null, file.fieldname + '-' + Date.now() + '.' + fileNameArray[1]);
    }
});
var upload = multer({storage: storage});

router.post('/upload', upload.single('upload'), function(req, res, next) {
	if(req.file) {
		return res.send({
    		"uploaded": 1,
		    "fileName": req.file.filename,
		    "url": req.protocol+"://"+req.headers.host+"/uploads/blog/"+req.file.filename
		});
	} else {
    return res.send({
        "uploaded": 0
    });
	}
});

module.exports = router;
