var express = require('express');
var router = express.Router();
var models = require('../../models');
const config = require('config');
const stripe = require("stripe")("sk_test_tSvkTrCCLhP9K60KDWJ62lXl00zJ31NhU5");
const send_email = require('../../modules/send_email');

router.post('/subscription', function(req, res, next) {
	if(!req.body.email) {
		return res.status(400).send({ description: 'Please enter all the fields!'});
	}

	let user_id = '';
	models.users.findOne({
		where: {
			email: req.body.email
		}
	})
	.then( (user) => {
		if(user) {
			return new Promise( (resolve, reject) => { resolve(user) });
			user_id = user.id
		} else {
			return models.users.create(req.body);
		}
	})
	.then( (user) => {
		req.body.user_id = user.id;
		stripe.plans.create({
		  amount: parseInt(req.body.total * 100),
		  interval: "month",
		  product: {
		    name: "Appmosphere subscription "+(new Date())
		  },
		  currency: "usd",
		  interval: req.body.interval == 'month' ? 'month' : 'year'
		}, function(err, plan) {
		  if(err) {
		  	console.log(err);
		  	return res.status(500).json({ 'description': 'Error while creating plan.'});
		  }
		  stripe.customers.create({
			  description: 'Customer for '+req.body.email,
			  source: req.body.stripe_id // obtained with Stripe.js
			}, function(err_cust, customer) {
			    if(err_cust) {
			      console.log(err_cust);
				  return res.status(500).json({ 'description': 'Error while creating customer.'});  	
			    }

			  	stripe.subscriptions.create({
				  customer: customer.id,
				  items: [
				    {
				      plan: plan.id,
				    },
				  ]
				}, function(err_sub, subscription) {
						if(err_sub) {
							console.log(err_sub);
							return res.status(500).json({ 'description': 'Error while creating subscription.'});
						}
				   		models.orders.create({
				   			stripe_id: subscription.id,
				   			email: req.body.email,
							address: req.body.address,
							address_1: req.body.address_1,
							zipcode: req.body.zipcode,
							country: req.body.country,
							state: req.body.state,
							city: req.body.city,
							company_name: req.body.company_name,
							tin: req.body.tin,
							phone: req.body.phone,
							total: req.body.total,
							discount: req.body.discount,
							stripe_id: subscription.id,
							users: req.body.users,
							user_id: req.body.user_id,
							duration: req.body.duration == 'year' ? 'annual' : 'monthly'
				   		})
				   		.then( success => {
							req.body.plan = success;
				   			let plan_items_array = [];
				   			if(req.body.plan_items && req.body.plan_items.length) {
				   				req.body.plan_items.forEach( (plan_item) => {
				   					plan_items_array.push({
				   						order_id: success.id,
				   						plan_id: plan_item.id,
										price: plan_item.price
				   					});
				   				});
				   			}
				   			if(plan_items_array && plan_items_array.length) {
				   				models.order_items.bulkCreate(plan_items_array)
				   				.then( (success) => {
				   					send_email.sendEmail('views/templates/subscription_created.ejs', {
										baseUrl: req.protocol+"://"+req.headers.host,
										user: user,
										logoUrl: '',
										plan: req.body.plan,
										plan_items: plan_items_array
									}, user.email);
									send_email.sendEmail('views/templates/subscription_created_admin.ejs', {
										baseUrl: req.protocol+"://"+req.headers.host,
										user: user,
										logoUrl: '',
										plan: req.body.plan,
										plan_items: plan_items_array
									}, config.admin_email);
				   					return res.status(200).json({ 'description': 'Subscription created.'});
				   				})
				   				.catch( (error) => {
									   console.log(error);
				   					return res.status(500).json({ 'description': 'Error while saving subscription.'});
				   				});
				   			} else {
					   			return res.status(200).json({ 'description': 'Subscription created.'});
				   			}
				   		})
				   		.catch( error => {
				   			console.log(error);
				   			return res.status(500).json({ 'description': 'Error while saving subscription.'});
				   		});
				  }
				);

			});
		});
	})
	.catch( (error) => {
		console.log(error);
		return res.status(500).send({ description: 'Error while creating subscription. Please tryagain later!'});
	});
})

/* GET home page. */
router.get('/:id', function(req, res, next) {
  models.states.findAll(
  {
  	where: {
  		country_id: req.params.id
  	}
  }
  )
  .then(states => {
  	res.status(200).send({ data: states });
  })
  .catch(error => {
  	res.status(500).send({ error: 'Internal Server Error'});
  })
});

/* GET home page. */
router.get('/', function(req, res, next) {
  models.countries.findAll()
  .then(countries => {
  	res.status(200).send({ data: countries });
  })
  .catch(error => {
  	res.status(500).send({ error: 'Internal Server Error'});
  })
});

module.exports = router;
