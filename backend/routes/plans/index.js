var express = require('express');
var router = express.Router();
var models = require('../../models');
const stripe = require("stripe")("sk_test_tSvkTrCCLhP9K60KDWJ62lXl00zJ31NhU5");

/* GET home page. */
router.get('/', function(req, res, next) {
  models.plans.findAll({
  	include: { model: models.plan_relations, include: { model: models.plans, as: 'required_plan' }},
  	order: [['id', 'ASC']]
  })
  .then(plans => {
  	res.status(200).send({ data: plans });
  })
  .catch(error => {
  	console.log(error);
  	res.status(500).send({ error: 'Internal Server Error'});
  });
});

module.exports = router;
