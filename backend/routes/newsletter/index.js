var express = require('express');
var router = express.Router();
const send_email = require('../../modules/send_email');
var models = require('../../models');
var moment = require('moment');
const config = require('config');
const questions  = require('./questions');
const passport = require('../../modules/passport');
const appointmentCols = ['primary_name', 'primary_email', 'created_at', 'business_address', 'business_website'];

router.post('/appointment', function(req, res, next) {
	req.body.ip = req.connection.remoteAddress;
	if(req.body.solutions_interested) {
		req.body.solutions_interested = req.body.solutions_interested.join(",");
	}
	if(req.body.consult_checkbox) {
		req.body.consult_checkbox = req.body.consult_checkbox.join(",");
	}
	models.appointments.create(req.body)
	.then( (success) => {
		let response = [];
		response.push({
			question: 'Primary Name',
			answer: req.body.primary_name
		});
		response.push({
			question: 'Primary Email',
			answer: req.body.primary_email
		});
		response.push({
			question: 'Primary Phone',
			answer: req.body.primary_phone
		});
		response.push({
			question: 'Business Website',
			answer: req.body.business_website
		});
		response.push({
			question: 'Business Address',
			answer: req.body.business_address
		});
		response.push({
			question: 'Business Name',
			answer: req.body.business_name
		});
		
		Object.keys(questions).forEach((question) => {
			if(question == 'solutions_interested') {
				let options = req.body.solutions_interested.split(",");
				let answers = [];
				for(var i=0; i<options.length; i++) {
					answers.push(questions[question].answers[parseInt(i)])
				}
				response.push({
					question: questions[question].question,
					answer: answers.join(" ,")
				});		
			} else if(question == 'consult_checkbox') {
				let options = req.body.consult_checkbox.split(",");
				let answers = [];
				for(var i=0; i<options.length; i++) {
					answers.push(questions[question].answers[parseInt(i)])
				}
				response.push({
					question: questions[question].question,
					answer: answers.join(" ,")
				});
			} else {
				response.push({
					question: questions[question].question,
					answer: questions[question].answers[parseInt(req.body[question])]
				});
			}
		});

		send_email.sendEmail('views/templates/new_appointment.ejs', {
			name: req.body.name,
			email: req.body.email,
			date: moment(req.body.date).format('LL'),
			logoUrl: '',
			response: response,
			primary_name: req.body.primary_name,
			primary_email: req.body.primary_email
		}, req.body.primary_email);
		return res.status(200).send({ description: 'Successfully created appointment!'});
	})
	.catch( (error) => {
		console.log(error);
		return res.status(500).send({ description: 'Error occured while sending message!'});
	});
});

router.get('/appointment/:id', passport.authenticate('jwt', { session: false }), function(req, res, next) {
	models.appointments.findOne({
		where: {
			id: req.params.id
		}
	})
	.then( (appointment) => {
		res.status(200).send({ data: appointment });
	})
	.catch( (error) => {
		res.status(500).send({ description: error.message });
	});
});

router.get('/appointment_list', passport.authenticate('jwt', { session: false }), function(req, res, next) {
	let query = {
		limit: parseInt(req.query.length),
		offset: parseInt(req.query.start)
	};

	if(req.query.search && req.query.search.value) {
		query['where'] = {
			[models.Sequelize.Op.or]: [{
					primary_name: {
						[models.Sequelize.Op.like]: '%'+req.query.search.value+'%' 		
					}
				},{
					primary_email: {
						[models.Sequelize.Op.like]: '%'+req.query.search.value+'%' 		
					}
				}
			]
		}
	}

	if(req.query.order && req.query.order[0]) {
		query['order'] = [[appointmentCols[req.query.order[0].column], req.query.order[0].dir == 'asc' ? 'ASC' : 'DESC']];
	}

	models.appointments.findAndCountAll({ where: query.where })
	.then( (total_count) => {
		models.appointments.findAll(query)
		.then( (users) => {
			let users_array = [];
			users.forEach(function(user, index) {
				users_array.push([user.primary_name, user.primary_email, moment(user.created_at).format('LLLL'), user.business_address, user.business_website, user.id]);
			});
			return res.status(200).send({ iTotalDisplayRecords: total_count.count, recordsFiltered: users.length, recordsTotal: total_count.count, data: users_array });
		})	
	})
	.catch( (error) => {
		console.log(error);
		return res.status(500).send({ description: 'Something went wrong while fetching your request. Plese try again later!' });
	});
});

router.post('/contact_us', function(req, res, next) {
	req.body.ip = req.connection.remoteAddress;
	send_email.sendEmail('views/templates/new_message.ejs', {
		name: req.body.name,
		email: req.body.email,
		date: moment(req.body.date).format('LL'),
		message: req.body.message,
		logoUrl: ''
	}, config.admin_email);
	models.messages.create(req.body)
	.then( (success) => {
		return res.status(200).send({ description: 'Successfully sent message!'});
	})
	.catch( (error) => {
		console.log(error);
		return res.status(500).send({ description: 'Error occured while sending message!'});
	});
});

router.post('/', function(req, res, next) {
	req.body.ip = req.connection.remoteAddress;
	models.newsletter_subscription.create(req.body)
	.then( (success) => {
		return res.status(200).send({ description: 'Successfully signed up!'});
	})
	.catch( (error) => {
		console.log(error);
		return res.status(500).send({ description: 'Error occured while signing up!'});
	});
});

module.exports = router;
