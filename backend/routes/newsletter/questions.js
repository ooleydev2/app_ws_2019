module.exports  = {
	"experience": {
		"question": "Please check the box that best describes your experience levels with Odoo ERM solutions.",
		"answers":[
			'I am a current (or former) subscriber to one or more Odoo software applications',
			'I am not a current or former subscriber, but I am familiar with the software application options',
			'I am not a current or former subscriber, and I\'m not very familiar with software application options'
		]
	},
	"consult_checkbox": {
		"question": "Odoo offers a way of integrating all of your business systems for data, resource, and financial efficiency. Please select all the systems that may be applicable to your business. (This doesn't commit you to an Odoo solution, but will help us personalize recommendations for you during a consultation)",
		"answers": [
			'Invoicing/Bookkeeping/Sales',
			'Point of Sale System', 
			'E-Commerce',
			'Invoicing',
			'Proposals/Quotes',
			'Accounting/Bookkeeping',
			'Marketing',
			'Customer Management System (CRM)',
			'Website',
			'E-Mail Marketing',
			'Events',
			'Appointments/Calendars',
			'Marketing Automation',
			'Project/Supply Chain Management',
			'Inventory Management',
			'Manufacturing Management',
			'Purchasing/Procurement',
			'Project Management',
			'Subscriptions',
			'Document Management',
			'Signatures/Routing',
			'PLM',
			'HR/Employee Management',
			'Timecards',
			'Expenses',
			'Recruitment',
			'Leaves',
			'Help Desk'
		]
	},
	'solutions_interested': {
		"question": "Now, please indicate which Odoo solutions you are interested in for your business.",
		"answers": [
			'Invoicing/Bookkeeping/Sales',
			'Point of Sale System', 
			'E-Commerce',
			'Invoicing',
			'Proposals/Quotes',
			'Accounting/Bookkeeping',
			'Marketing',
			'Customer Management System (CRM)',
			'Website',
			'E-Mail Marketing',
			'Events',
			'Appointments/Calendars',
			'Marketing Automation',
			'Project/Supply Chain Management',
			'Inventory Management',
			'Manufacturing Management',
			'Purchasing/Procurement',
			'Project Management',
			'Subscriptions',
			'Document Management',
			'Signatures/Routing',
			'PLM',
			'HR/Employee Management',
			'Timecards',
			'Expenses',
			'Recruitment',
			'Leaves',
			'Help Desk'
		] 
	},
	"free_appointment": {
		"question": "Are you interested in a free consultation appointment with a member of our team?",
		"answers": [
			'Yes, that would be helpful!',
			'No, no need to chitty-chat. Let\'s just get started!'
		]
	},
	"custom_application": {
		"question": "Are you interested in custom applications for your business? (either stand-alone apps, or custom modifications to existing Odoo apps)?",
		"answers": [
			'Yes, definitely...let\'s talk!',
			'I\'m not sure yet....let\'s discuss',
			'No, just sticking to what\'s available'
		]
	},
	'contact_person': {
		"question": 'Do you have a point-person within your organization that will be the main Point of Contact for managing the ERM/Odoo solutions?',
		'answers': [
			'Yes',
			'Not sure yet',
			'No, there are many people involved'
		]
	}
}