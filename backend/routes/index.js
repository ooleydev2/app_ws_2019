var express = require('express');
var router = express.Router();
var locationRouter = require('./location');
var newsletter = require('./newsletter');
var plans = require('./plans');
var users = require('./users');
var orders = require('./orders');
var images = require('./images');
var blogs = require('./blogs');
const passport = require('../modules/passport');

router.use('/location', locationRouter);
router.use('/newsletter', newsletter);
router.use('/plans', plans);
router.use('/users', users);
router.use('/orders', passport.authenticate('jwt', { session: false }), orders);
router.use('/orders', users);
router.use('/images', images);
router.use('/blogs', blogs);

module.exports = router;
