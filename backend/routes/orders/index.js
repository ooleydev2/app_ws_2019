var express = require('express');
var router = express.Router();
var models = require('../../models');
const stripe = require("stripe")("sk_test_tSvkTrCCLhP9K60KDWJ62lXl00zJ31NhU5");
const send_email = require('../../modules/send_email');
const config = require('config');

router.post('/cancel_subscription', function(req, res, next) {
  if(!req.body.subscription_id) {
    return res.status(200).send({ description: 'No subscription id found. Please try again later!'});
  }

  stripe.subscriptions.del(
    req.body.subscription_id,
    function(err, confirmation) {
      console.log(err);
      if(err) {
        return res.status(500).send({ description: 'Error while cancelling subscription. Please try again later!'});
      }

      models.orders.update({
        subscription_state: 'cancelled'
      }, {
        where: {
          stripe_id: req.body.subscription_id
        }
      })
      .then( (success) => {
        return models.orders.findOne({
          where: {
            stripe_id: req.body.subscription_id
          },
          include: { model: models.users }
        });
      })
      .then( (order) => {
        send_email.sendEmail('views/templates/subscription_cancelled.ejs', {
          baseUrl: req.protocol + "://" + req.headers.host,
          user: order.user,
          logoUrl: '',
          plan: order
        }, order.user.email);
        send_email.sendEmail('views/templates/subscription_cancelled_admin.ejs', {
          baseUrl: req.protocol + "://" + req.headers.host,
          user: order.user,
          logoUrl: '',
          plan: order
        }, config.admin_email);
        return res.status(200).send({ description: 'Subscription cancelled successfuly!'});
      })
      .catch( (error) => {
        return res.status(500).send({ description: 'Error while cancelling subscription. Please try again later!'});
      })
    }
  );
});

/* GET home page. */
router.get('/', function(req, res, next) {
  console.log(req.user);
  let limit = req.query.limit || 10;
  let offset = req.query.offset || 0;

  let query = {
  	include: [{
  		model: models.order_items,
  		include: {
  			model: models.plans
  		}
  	}, { model: models.users }],
  	limit: limit,
  	offset: offset,
    order: models.sequelize.literal('created_at DESC')
  };

  if(req.user.type != 'admin') {
  	query['where'] = {
  		user_id: req.user.id
  	};
  }

  models.orders.findAll(query)
  .then(plans => {
  	res.status(200).send({ data: plans });
  })
  .catch(error => {
  	console.log(error);
  	res.status(500).send({ error: 'Internal Server Error'});
  });
});

module.exports = router;
