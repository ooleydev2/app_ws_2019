var express = require('express');
var router = express.Router();
var models = require('../../models');
const stripe = require("stripe")("sk_test_tSvkTrCCLhP9K60KDWJ62lXl00zJ31NhU5");
const send_email = require('../../modules/send_email');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const config = require('config');
const passport = require('../../modules/passport');
const moment = require('moment');
const userCols = ['name', 'email', 'created_at', 'address', 'country', 'state'];

router.get('/details', passport.authenticate('jwt', { session: false }), function(req, res, next) {
	models.users.findOne({ 
		where: {
			id: req.user.id
		}
	})
	.then( (user) => {
		return res.status(200).send({ data: user });
	})
	.catch( (error) => {
		return res.status(500).send({ description: 'Something went wrong while fetching your request. Plese try again later!' });
	});
});

router.get('/user_details/:id', passport.authenticate('jwt', { session: false }), function(req, res, next) {
	models.users.findOne({ 
		where: {
			id: req.params.id
		},
		include: [
			{ model: models.countries, as: 'countries' },
			{ model: models.states, as: 'states' },
			{ model: models.orders, as: 'orders', include: { model: models.order_items } }
		]
	})
	.then( (user) => {
		return res.status(200).send({ data: user });
	})
	.catch( (error) => {
		return res.status(500).send({ description: 'Something went wrong while fetching your request. Plese try again later!' });
	});
});

router.get('/users_list', passport.authenticate('jwt', { session: false }), function(req, res, next) {
	let query = {
		limit: parseInt(req.query.length),
		offset: parseInt(req.query.start),
		include: [
			{ model: models.countries, as: 'countries' },
			{ model: models.states, as: 'states' },
			{ model: models.orders, as: 'orders' }
		]
	};

	if(req.query.order && req.query.order[0]) {
		query['order'] = [[userCols[req.query.order[0].column], req.query.order[0].dir == 'asc' ? 'ASC' : 'DESC']];
	}

	if(req.query.search.value) {
		query['where'] = {
			[models.Sequelize.Op.or]: [{
				name: {
						[models.Sequelize.Op.like]: '%'+req.query.search.value+'%' 		
					}
				},{
					email: {
						[models.Sequelize.Op.like]: '%'+req.query.search.value+'%' 		
					}
				}
			]
		}
	}

	models.users.findAndCountAll({ where: query.where })
	.then( (total_count) => {
		models.users.findAll(query)
		.then( (users) => {
			let users_array = [];
			users.forEach(function(user, index) {
				users_array.push([user.name, user.email, moment(user.created_at).format('LLLL'), user.address, (user.countries ? user.countries.name : '-'), (user.states ? user.states.name : '-'), (user.orders ? user.orders.length : '-'), user.id]);
			});
			return res.status(200).send({ iTotalDisplayRecords: total_count.count, recordsFiltered: users.length, recordsTotal: total_count.count, iTotalDisplayRecords: total_count.count, data: users_array });
		})	
	})
	.catch( (error) => {
		console.log(error);
		return res.status(500).send({ description: 'Something went wrong while fetching your request. Plese try again later!' });
	});
});


//Verify user
router.post('/verify', (req, res, next) => {
	models.users.findOne({
	  	where: {
	  		verify_token: req.body.token
	  	}
	})
	.then( (user) => {
		if(user) {
			user.updateAttributes({ verify_token: null, verified: 1 })
			.then( success  => {
				console.log(success);
			})
			.catch( error => {
				console.log(error);
			});
		  	return res.status(200).send({ data: {}, description: 'Successfully verified account!' });
		} else {
			return res.status(400).send({ description: 'No such request found!' });
		}
	})
	.catch(error => {
	  	console.log(error);
	  	return res.status(500).send({ description: 'Internal Server Error'});
	});
});

//Send forgot password email
router.post('/forgot', (req, res, next) => {
	models.users.findOne({
	  	where: {
	  		email: req.body.email
	  	}
	})
	.then( (user) => {
		if(!user) {
			return res.status(400).send({ description: 'No such user found!' });
		} else if(!user.verified){
			return res.status(400).send({ description: 'Please verify your account first!' });
		} else if(user) {
			const token = jwt.sign({ id: user.id, email: user.email }, config.jwt_secret);
			user.updateAttributes({ 'reset_token': token });
			send_email.sendEmail('views/templates/reset_password.ejs', {
				token: token,
				baseUrl: req.protocol+"://"+req.headers.host,
				user: user,
				logoUrl: ''
			}, user.email);
		  	return res.status(200).send({ data: { token: token, user_type: user.type } });
		}
	})
	.catch(error => {
	  	console.log(error);
	  	return res.status(500).send({ description: 'Internal Server Error'});
	});
});

//Reset password
router.post('/update_password', (req, res, next) => {
	models.users.findOne({
	  	where: {
	  		reset_token: req.body.token
	  	}
	})
	.then( (user) => {
		if(user) {
			bcrypt.hash(req.body.password, config.salt_rounds)
			.then( (hash) => {
				user.updateAttributes({ reset_token: null, password: hash })
				.then( success => {
					console.log(success);
				})
				.catch( error => {
					console.log(error);
				});
			  	return res.status(200).send({ data: {}, description: 'Successfully reset password!' });	
			})
			.catch( (error) => {
				return res.status(500).send({ description: 'Internal Server Error'});
			});
		} else {
			return res.status(400).send({ description: 'No such request found!' });
		}
	})
	.catch(error => {
	  	return res.status(500).send({ description: 'Internal Server Error'});
	});
});

//Reset password
router.post('/login', (req, res, next) => {
	console.log(req.body);
	models.users.findOne({
	  	where: {
	  		email: req.body.email
	  	}
	})
	.then( (user) => {
		if(!user) {
			return res.status(400).send({ description: 'Invalid credentials!' });
		} else if(!user.verified) {
			return res.status(400).send({ description: 'Please verify your email in order to continue!' });
		} else if(user) {
			bcrypt.compare(req.body.password, user.password)
            .then((compared) => {
                if (compared) {
                	const token = jwt.sign({ id: user.id, email: user.email }, config.jwt_secret);
                    return res.status(200).send({ user_type: user.type, token: token, description: 'Successfully logged in!' });
                } else {
                	return res.status(400).send({ description: 'Invalid credentials!' });
                }
            })
            .catch(() => {
        		return res.status(500).send({ description: 'Internal Server Error'});    	
            });
		}
	})
	.catch(error => {
		console.log(error);
	  	return res.status(500).send({ description: 'Internal Server Error'});
	});
});

router.post('/valid_token', function(req, res, next) {
	if(!req.body.token) {
		return res.status(400).send({ description: 'Please use a valid token!'});
	}

	models.users.findOne({
		where: {
			reset_token: req.body.token
		}
	})
	.then( (user) => {
		if(user) {
			return res.status(200).send({ description: 'Valid token!'});
		} else {
			return res.status(400).send({ description: 'Either token has expired or doesn\'t exist. Please retry sending email!'});
		}
	})
	.catch( (error) => {
		return res.status(500).send({ description: 'Something went wrong while processing your request. Please try again later!'});
	});
});

//Create user
router.post('/', function(req, res, next) {
  if(!req.body.email || !req.body.name || !req.body.password) {
  	return res.status(400).send({ description: 'Please enter all the fields'});
  }

  models.users.findOne({
  	where: {
  		email: req.body.email
  	}
  })
  .then( (user) => {
  	if(user) {
  		return res.status(400).send({ description: 'User already exists'});
  	} else {
  		return models.users.create(req.body);
  	}
  })
  .then(user => {
  		if(user && user.id) {
	  		const token = jwt.sign({ id: user.id, email: user.email }, config.jwt_secret);
			user.updateAttributes({ 'verify_token': token });
			send_email.sendEmail('views/templates/verify_email.ejs', {
				token: token,
				baseUrl: req.protocol+"://"+req.headers.host,
				user: user,
				logoUrl: ''
			}, user.email)
			.then(() => {
				console.log("Succesfully sent email");
			})
			.catch(error => {
				//console.log(error);
			});
		  	res.status(200).send({ data: user });
	    }
  })
  .catch(error => {
  	//console.log(error);
  	res.status(500).send({ description: 'Internal Server Error'});
  });
});

router.post('/update_details', passport.authenticate('jwt', { session: false }), function(req, res, next) {
	models.users.update(req.body, {
		where: {
			id: req.user.id
		}
	})
	.then( (updated) => {
		return res.status(200).send({ description: 'Sucessfuly udpated!'});
	})
	.catch( (error) => {
		return res.status(500).send({ description: 'Error while updating!'});
	});
});

module.exports = router;